package com.refresh.pos.ui.sale;

public class DataSale {
    private String id,saleid,nost,productid,quantity,unit_price,payment,start_time,cashier,outlet,pengasong,sync,discount,
    tunai,voucher,kartu,cashless,foc,noref;

    public DataSale() {
    }

    public DataSale(String id, String saleid, String nost, String productid, String quantity, String unit_price,
                    String payment, String start_time, String cashier, String outlet, String pengasong, String sync, String discount,
                    String tunai, String voucher, String kartu, String cashless, String foc, String noref) {
        this.id = id;
        this.saleid = saleid;
        this.nost = nost;
        this.outlet = outlet;
        this.productid = productid;
        this.quantity = quantity;
        this.unit_price = unit_price;
        this.payment = payment;
        this.start_time = start_time;
        this.cashier = cashier;
        this.outlet = outlet;
        this.pengasong = pengasong;
        this.sync = sync;
        this.discount = discount;
        this.tunai = tunai;
        this.voucher = voucher;
        this.kartu = kartu;
        this.cashless = cashless;
        this.foc = foc;
        this.noref = noref;

    }

    public String getId() {
        return id;
    }
    public void setId(String id) { this.id = id; }

    public String getSaleid() {
        return saleid;
    }
    public void setSaleid(String saleid) { this.saleid = saleid; }

    public String getNost() {
        return nost;
    }

    public void setNost(String nost) {
        this.nost = nost;
    }

    public String getOutlet() { return outlet; }
    public void setOutlet(String outlet) { this.outlet = outlet; }

    public String getProductid() {
        return productid;
    }
    public void setProductid(String productid) { this.productid = productid; }

    public String getQuantity() {
        return quantity;
    }
    public void setQuantity(String quantity) { this.quantity = quantity; }

    public String getUnit_price() {
        return unit_price;
    }
    public void setUnit_price(String unit_price) { this.unit_price = unit_price; }

    public String getPayment() { return payment; }
    public void setPayment(String payment) { this.payment = payment; }

    public String getStart_time() {
        return start_time;
    }
    public void setStart_time(String start_time) { this.start_time = start_time; }

    public String getCashier() {
        return cashier;
    }
    public void setCashier(String cashier) { this.cashier = cashier; }

    public String getSync() {
        return sync;
    }
    public void setSync(String sync) { this.sync = sync; }

    public String getDiscount() { return discount; }
    public void setDiscount(String discount) { this.discount = discount; }

    public String getPengasong() { return pengasong; }
    public void setPengasong(String pengasong) { this.pengasong = pengasong; }

    public String getTunai() { return tunai; }
    public void setTunai(String tunai) { this.tunai = tunai; }

    public String getVoucher() { return voucher; }
    public void setVoucher(String voucher) { this.voucher = voucher; }

    public String getKartu() { return kartu; }
    public void setKartu(String kartu) { this.kartu = kartu; }

    public String getCashless() { return cashless; }
    public void setCashless(String cashless) { this.cashless = cashless; }

    public String getFoc() { return foc; }
    public void setFoc(String foc) { this.foc = foc; }

    public String getNoref() { return noref; }
    public void setNoref(String noref) { this.noref = noref; }
}
