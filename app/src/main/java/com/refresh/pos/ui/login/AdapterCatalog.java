package com.refresh.pos.ui.login;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.refresh.pos.R;

import java.util.List;

public class AdapterCatalog extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<DataCatalog> item;

    public AdapterCatalog(Activity activity, List<DataCatalog> item) {
        this.activity = activity;
        this.item = item;
    }

    @Override
    public int getCount() { return item.size(); }

    @Override
    public Object getItem(int location) { return  item.get(location); }

    @Override
    public long getItemId(int position) { return position; }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_inventory, null);

        TextView outlet = (TextView) convertView.findViewById(R.id.outlet);

        DataCatalog dataCatalog = item.get(position);

        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView barcode = (TextView) convertView.findViewById(R.id.barcode);
        TextView price = (TextView) convertView.findViewById(R.id.price);

        return convertView;
    }
}
