package com.refresh.pos.ui;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.refresh.pos.R;
import com.refresh.pos.domain.DateTimeStrategy;
import com.refresh.pos.domain.LanguageController;
import com.refresh.pos.domain.inventory.Inventory;
import com.refresh.pos.domain.sale.Register;
import com.refresh.pos.domain.sale.SaleLedger;
import com.refresh.pos.techicalservices.AndroidDatabase;
import com.refresh.pos.techicalservices.Database;
import com.refresh.pos.techicalservices.DatabaseExecutor;
import com.refresh.pos.techicalservices.inventory.InventoryDao;
import com.refresh.pos.techicalservices.inventory.InventoryDaoAndroid;
import com.refresh.pos.techicalservices.sale.SaleDao;
import com.refresh.pos.techicalservices.sale.SaleDaoAndroid;

import java.util.Locale;

public class LoginActivity extends Activity {
    private Button btnlogin;

    private void initiateCoreApp() {
        Database database = new AndroidDatabase(this);
        InventoryDao inventoryDao = new InventoryDaoAndroid(database);
        SaleDao saleDao = new SaleDaoAndroid(database);
        DatabaseExecutor.setDatabase(database);
        LanguageController.setDatabase(database);

        Inventory.setInventoryDao(inventoryDao);
        Register.setSaleDao(saleDao);
        SaleLedger.setSaleDao(saleDao);

        DateTimeStrategy.setLocale("th", "TH");
        setLanguage(LanguageController.getInstance().getLanguage());

        Log.d("Core App", "INITIATE");
    }

    /**
     * Set language.
     * @param localeString
     */
    private void setLanguage(String localeString) {
        Locale locale = new Locale(localeString);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
    }

    private void initiateUI(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_login);
        btnlogin = (Button) findViewById(R.id.btn_login);
        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                go();
            }
        });
    }

    private void go() {
        Intent newActivity = new Intent(LoginActivity.this,MainActivity.class);
        startActivity(newActivity);
        LoginActivity.this.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        initiateUI(savedInstanceState);
        initiateCoreApp();
    }

}
