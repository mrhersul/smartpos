package com.refresh.pos.ui.login;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.HashMap;

public class SQLiteHandler extends SQLiteOpenHelper {

    private static final String TAG = SQLiteHandler.class.getSimpleName();

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "com.refresh.db1";

    // Login table name
    private static final String TABLE_CASHIER = "cashier";

    // Login Table Columns names
    private static final String KEY_ID = "_id";
    private static final String KEY_OUTLETID = "outlet_id";
    private static final String KEY_OUTLET = "outlet";
    private static final String KEY_CASHIER = "cashier";
    private static final String KEY_LEVEL = "level";
    private static final String KEY_CREATED_AT = "created_at";

    public SQLiteHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_LOGIN_TABLE = "CREATE TABLE " + TABLE_CASHIER + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_OUTLETID + "INTEGER,"
                + KEY_OUTLET + " TEXT," + KEY_CASHIER + " TEXT,"
                + KEY_LEVEL + " TEXT UNIQUE,"
                + KEY_CREATED_AT + " TEXT" + ")";
        db.execSQL(CREATE_LOGIN_TABLE);

        Log.d(TAG, "Database tables created");
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CASHIER);

        // Create tables again
        onCreate(db);
    }

    /**
     * Storing user details in database
     * */
   // public void addUser(String outlet, String cashier) {
   //     SQLiteDatabase db = this.getWritableDatabase();

//        ContentValues values = new ContentValues();
  //      values.put(KEY_OUTLET, outlet);
   //     values.put(KEY_CASHIER, cashier); // Email
        //values.put(KEY_LEVEL, level); // Created At

        // Inserting Row
   //     long id = db.insert(TABLE_CASHIER, null, values);
    //    db.close(); // Closing database connection

    //    Log.d(TAG, "New user inserted into sqlite: " + id);
    //}

    /**
     * Getting user data from database
     * */
    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        String selectQuery = "SELECT  * FROM " + TABLE_CASHIER;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            user.put("outlet", cursor.getString(1));
            user.put("cashier", cursor.getString(2));
            user.put("level", cursor.getString(3));
        }
        cursor.close();
        db.close();
        // return user
        Log.d(TAG, "Fetching user from Sqlite: " + user.toString());

        return user;
    }

    /**
     * Re crate database Delete all tables and create them again
     * */
    public void deleteUsers() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_CASHIER, null, null);
        db.close();

        Log.d(TAG, "Deleted all user info from sqlite");
    }

}