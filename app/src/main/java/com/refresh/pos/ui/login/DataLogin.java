package com.refresh.pos.ui.login;

public class DataLogin {
    private String outlet_id, cashier_id,outlet, cashier;

    public DataLogin() {
    }

    public DataLogin(String outlet_id, String cashier_id, String outlet, String cashier) {
        this.outlet_id = outlet_id;
        this.cashier_id = cashier_id;
        this.outlet = outlet;
        this.cashier = cashier;
    }

    public String getOutlet_id() { return outlet_id; }
    public void setOutlet_id(String outlet_id) { this.outlet_id = outlet_id; }

    public String getCashier_id() { return cashier_id; }
    public void setCashier_id(String cashier_id) { this.cashier_id = cashier_id; }

    public String getOutlet() { return outlet; }
    public void setOutlet(String outlet) { this.outlet = outlet; }

    public String getCashier() { return cashier; }
    public void setCashier(String cashier) { this.cashier = cashier; }
}
