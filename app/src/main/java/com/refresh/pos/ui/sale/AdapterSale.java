package com.refresh.pos.ui.sale;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.refresh.pos.R;
import com.refresh.pos.ui.login.Adapter;

import org.w3c.dom.Text;

import java.util.List;

public class AdapterSale extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<DataSale> items;

    public AdapterSale(Activity activity, List<DataSale> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount() { return items.size(); }

    @Override
    public Object getItem(int location) {
        return items.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_row_sales, null);

        TextView id = (TextView) convertView.findViewById(R.id.id);
        TextView saleid = (TextView) convertView.findViewById(R.id.saleid);
        TextView nost = (TextView) convertView.findViewById(R.id.nost);
        TextView outlet = (TextView) convertView.findViewById(R.id.outlet);
        TextView productid = (TextView) convertView.findViewById(R.id.productid);
        TextView quantity = (TextView) convertView.findViewById(R.id.quantity);
        TextView unit_price = (TextView) convertView.findViewById(R.id.unit_price);
        TextView payment = (TextView) convertView.findViewById(R.id.payment);
        TextView starttime = (TextView) convertView.findViewById(R.id.startTime);
        TextView cashier = (TextView) convertView.findViewById(R.id.cashier);
        TextView sync = (TextView) convertView.findViewById(R.id.sync);
        TextView pengasong = (TextView) convertView.findViewById(R.id.pengasong);
        TextView tunai = (TextView) convertView.findViewById(R.id.tunai);
        TextView voucher = (TextView) convertView.findViewById(R.id.voucher);
        TextView kartu = (TextView) convertView.findViewById(R.id.kartu);
        TextView cashless = (TextView) convertView.findViewById(R.id.cashless);
        TextView foc = (TextView) convertView.findViewById(R.id.foc);
        TextView noref = (TextView) convertView.findViewById(R.id.noref);


        DataSale data = items.get(position);

        id.setText(data.getId());
        saleid.setText(data.getSaleid());
        nost.setText(data.getNost());
        outlet.setText(data.getOutlet());
        productid.setText(data.getProductid());
        quantity.setText(data.getQuantity());
        unit_price.setText(data.getUnit_price());
        payment.setText(data.getPayment());
        starttime.setText(data.getStart_time());
        cashier.setText(data.getCashier());
        sync.setText(data.getSync());
        pengasong.setText(data.getPengasong());
        tunai.setText(data.getTunai());
        voucher.setText(data.getVoucher());
        kartu.setText(data.getKartu());
        cashless.setText(data.getCashless());
        foc.setText(data.getFoc());
        noref.setText(data.getNoref());

        return convertView;
    }
}
