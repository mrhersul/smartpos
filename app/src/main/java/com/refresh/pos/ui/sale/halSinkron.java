package com.refresh.pos.ui.sale;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.refresh.pos.R;
import com.refresh.pos.domain.DateTimeStrategy;
import com.refresh.pos.techicalservices.AndroidDatabase;
import com.refresh.pos.ui.MainActivity;
import com.refresh.pos.ui.SharedData;
import com.refresh.pos.ui.login.Adapter;
import com.refresh.pos.ui.login.AppConfig;
import com.refresh.pos.ui.login.AppController;
import com.refresh.pos.ui.login.RegisterActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class halSinkron extends Activity {
    private static final String TAG = RegisterActivity.class.getSimpleName();
    public EditText serverside;
    public EditText lokalside;
    private EditText sisa;
    private Button syncButton;
    private Button exitButton;
    private ProgressDialog pDialog;
    private TextView tanggal;

    List<DataSinkronServer> serverList = new ArrayList<DataSinkronServer>();
    List<DataSinkronLokal> lokalList = new ArrayList<DataSinkronLokal>();
    AndroidDatabase SQLite = new AndroidDatabase(this);

    public static final String TAG_SERVER = "server";
    public static final String TAG_TSERVER = "tserver";
    public static final String TAG_LOKAL = "lokal";
    public static final String TAG_TLOKAL = "tlokal";
    public static final String TAG_SISA = "sisa";

    public static final String TAG_ID = "_id";
    private static final String TAG_SALEID = "sale_id";
    private static final String TAG_NOST = "nost";
    public static final String TAG_PRODUCTID = "product_id";
    public static final String TAG_QUANTITY = "quantity";
    public static final String TAG_UNITPRICE = "unit_price";
    public static final String TAG_PAYMENT = "payment";
    public static final String TAG_START = "start_time";
    public static final String TAG_CASHIER = "cashier";
    public static final String TAG_OUTLET = "outlet_id";
    public static final String TAG_PENGASONG = "pengasong";
    public static final String TAG_TUNAI = "tunai";
    public static final String TAG_VOUCHER = "voucher";
    public static final String TAG_KARTU = "kartu";
    public static final String TAG_CASHLESS = "cashless";
    public static final String TAG_FOC = "foc";
    public static final String TAG_NOREF = "noref";
    public static final String TAG_SYNC = "sync";
    private static final String TAG_SUCCESS = "success";

    int success;

    public String M_Server, T_Server, M_Lokal, T_Lokal, M_Sisa, T_Sisa, M_Outlet;
    SharedData sharedData = SharedData.getInstance();

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.halsinkron);
        serverside = (EditText) findViewById(R.id.serverside);
        lokalside = (EditText) findViewById(R.id.localside);
        sisa = (EditText) findViewById(R.id.sisa);
        exitButton = (Button) findViewById(R.id.exitButton);
        syncButton = (Button) findViewById(R.id.confirmButton);
        tanggal = (TextView) findViewById(R.id.tanggal);
        //Call Lokal
        CallLocal();

        //Call Server
        CallServer();
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(halSinkron.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        syncButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newActivity = new Intent(halSinkron.this, TransactionSinkron.class);
                startActivity(newActivity);
                finish();
            }
        });

        tanggal.setText("Data pada : [ " + DateTimeStrategy.getCurrentTime() +  " ] ");
    }

    public void CallLocal() {
        ArrayList<HashMap<String, String>> rows = SQLite.getSumSales();
        M_Lokal = rows.get(0).get(TAG_LOKAL);
        T_Lokal = rows.get(0).get(TAG_TLOKAL);
        double tot = Double.parseDouble(T_Lokal);
        lokalside.setText(M_Lokal + " | Rp." + tot);
    }
    public void CallServer() {
        serverList.clear();
        ArrayList<HashMap<String, String>> rows = SQLite.getLogin();
        M_Outlet = rows.get(0).get(TAG_OUTLET);
        String url = "http://pos.sule-soft.com/service/sumsales.php?id=" + M_Outlet;
        //String url = "https://data.jasabogaraya.com/service/sumsales.php?id=" + M_Outlet;
        JsonArrayRequest jArr = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.e(TAG, response.toString());

                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);
                                M_Server = obj.getString(TAG_SERVER);
                                T_Server = obj.getString(TAG_TSERVER);
                                serverside.setText(M_Server + " | Rp." + T_Server);
                                int a = Integer.parseInt(M_Lokal);
                                int b = Integer.parseInt(M_Server);
                                double ta = Double.parseDouble(T_Lokal);
                                double tb = Double.parseDouble(T_Server);

                                sisa.setText(String.valueOf(a-b) + " | Rp." + String.valueOf(ta - tb));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        //adapter.notifyDataSetChanged();
                        hideDialog();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //VolleyLog.e(TAG, "Error: " + error.getMessage());
                VolleyLog.e(TAG, "Jaringan Internet Terputus.... Klik Tombol Refresh!!");
                Toast.makeText(halSinkron.this, error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        });

        AppController.getInstance().addToRequestQueue(jArr);
    }
    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}
