package com.refresh.pos.ui.login;

public class Data {
    private String id, outlet;

    public Data() {

    }

    public Data(String id, String Outlet) {
        this.id = id;
        this.outlet = outlet;
    }

    public String getId() { return id; }

    public void setId(String id) { this.id = id;}

    public String getOutlet() { return outlet; }

    public void setOutlet(String outlet) { this.outlet = outlet; }
}
