package com.refresh.pos.ui;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.io.FileNotFoundException;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.FragmentTransaction;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import java.io.InputStream;
import java.io.IOException;

import com.refresh.pos.R;
import com.refresh.pos.domain.DateTimeStrategy;
import com.refresh.pos.domain.LanguageController;
import com.refresh.pos.domain.inventory.Inventory;
import com.refresh.pos.domain.inventory.Product;
import com.refresh.pos.domain.inventory.ProductCatalog;
import com.refresh.pos.domain.sale.Sale;
import com.refresh.pos.domain.sale.SaleLedger;
import com.refresh.pos.techicalservices.AndroidDatabase;
import com.refresh.pos.techicalservices.NoDaoSetException;
import com.refresh.pos.ui.component.UpdatableFragment;
import com.refresh.pos.ui.inventory.InventoryFragment;
import com.refresh.pos.ui.inventory.ProductDetailActivity;
import com.refresh.pos.ui.login.LoginActivity;
import com.refresh.pos.ui.login.SQLiteHandler;
import com.refresh.pos.ui.sale.ClosingActivity;
import com.refresh.pos.ui.sale.ReportFragment;
import com.refresh.pos.ui.sale.SaleFragment;

import com.refresh.pos.ui.login.SessionManager;
import com.refresh.pos.ui.sale.SalesActivity;
import com.refresh.pos.ui.sale.SalesSync;
import com.refresh.pos.ui.sale.halSinkron;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import android.widget.Toast;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


import android.net.Uri;


/**
 * This UI loads 3 main pages (Inventory, Sale, Report)
 * Makes the UI flow by slide through pages using ViewPager.
 * 
 * @author Refresh Team
 *
 */
@SuppressLint("NewApi")
public class MainActivity extends FragmentActivity {

	private SessionManager session;
	private ViewPager viewPager;
	private ProductCatalog productCatalog;
	private String productId;
	private Product product;
	private static boolean SDK_SUPPORTED;
	private PagerAdapter pagerAdapter;
	private Resources res;

	private SaleLedger saleLedger;
	List<Map<String, String>> saleList;
	private Calendar currentTime;

	// Database Name
	private static String DB_PATH = "/data/data/com.refresh.pos/databases/";
	private static final String DATABASE_NAME = "com.refresh.db1";

	BluetoothAdapter bluetoothAdapter;
	BluetoothSocket bluetoothSocket;
	BluetoothDevice bluetoothDevice;

	java.io.OutputStream outputStream;
	InputStream inputStream;
	Thread thread;

	byte[] readBuffer;
	int readBufferPosition;
	volatile boolean stopWorker;

	AndroidDatabase SQLite = new AndroidDatabase(this);
	DownloadManager downloadManager;

	@SuppressLint("NewApi")
	/**
	 * Initiate this UI.
	 */
	private void initiateActionBar() {
		if (SDK_SUPPORTED) {
			ActionBar actionBar = getActionBar();
			
			actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

			ActionBar.TabListener tabListener = new ActionBar.TabListener() {
				@Override
				public void onTabReselected(Tab tab, FragmentTransaction ft) {
				}

				@Override
				public void onTabSelected(Tab tab, FragmentTransaction ft) {
					viewPager.setCurrentItem(tab.getPosition());
				}

				@Override
				public void onTabUnselected(Tab tab, FragmentTransaction ft) {
				}
			};
			actionBar.addTab(actionBar.newTab().setText(res.getString(R.string.inventory))
					.setTabListener(tabListener), 0, false);
			actionBar.addTab(actionBar.newTab().setText(res.getString(R.string.sale))
					.setTabListener(tabListener), 1, true);
			actionBar.addTab(actionBar.newTab().setText(res.getString(R.string.report))
					.setTabListener(tabListener), 2, false);
	
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
				actionBar.setStackedBackgroundDrawable(new ColorDrawable(Color
						.parseColor("#73bde5")));
			}

		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		//tambahan logout
		SQLite = new AndroidDatabase(getApplicationContext());
		// session manager
		session = new SessionManager(getApplicationContext());

		if (!session.isLoggedIn()) {
			//logoutUser();
			openQuitDialog();
		}

		res = getResources();
		setContentView(R.layout.layout_main);
		viewPager = (ViewPager) findViewById(R.id.pager);
		super.onCreate(savedInstanceState);
		SDK_SUPPORTED = Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
		initiateActionBar();
		FragmentManager fragmentManager = getSupportFragmentManager();
		pagerAdapter = new PagerAdapter(fragmentManager, res);
		viewPager.setAdapter(pagerAdapter);
		viewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						if (SDK_SUPPORTED)
							getActionBar().setSelectedNavigationItem(position);
					}
				});
		viewPager.setCurrentItem(1);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			openQuitDialog();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	/**
	 * Open quit dialog.
	 */
	private void openQuitDialog() {
		AlertDialog.Builder quitDialog = new AlertDialog.Builder(
				MainActivity.this);
		quitDialog.setTitle(res.getString(R.string.dialog_quit));
		quitDialog.setPositiveButton(res.getString(R.string.quit), new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				logoutUser();
				finish();
			}
		});

		quitDialog.setNegativeButton(res.getString(R.string.no), new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				
			}
		});
		quitDialog.show();
	}

	/**
	 * Option on-click handler.
	 * @param view
	 */
	public void optionOnClickHandler(View view) {
		viewPager.setCurrentItem(0);
		String id = view.getTag().toString();
		productId = id;
		try {
			productCatalog = Inventory.getInstance().getProductCatalog();
		} catch (NoDaoSetException e) {
			e.printStackTrace();
		}
		product = productCatalog.getProductById(Integer.parseInt(productId));
		openDetailDialog();

	}

	/**
	 * Open detail dialog.
	 */
	private void openDetailDialog() {
		AlertDialog.Builder quitDialog = new AlertDialog.Builder(MainActivity.this);
		quitDialog.setTitle(product.getName());
		quitDialog.setPositiveButton(res.getString(R.string.remove), new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				openRemoveDialog();
			}
		});

		quitDialog.setNegativeButton(res.getString(R.string.product_detail), new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent newActivity = new Intent(MainActivity.this,
						ProductDetailActivity.class);
				newActivity.putExtra("id", productId);
				startActivity(newActivity);
			}
		});

		quitDialog.show();
	}

	/**
	 * Open remove dialog.
	 */
	private void openRemoveDialog() {
		AlertDialog.Builder quitDialog = new AlertDialog.Builder(
				MainActivity.this);
		quitDialog.setTitle(res.getString(R.string.dialog_remove_product));
		quitDialog.setPositiveButton(res.getString(R.string.no), new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {

			}
		});

		quitDialog.setNegativeButton(res.getString(R.string.remove), new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				productCatalog.suspendProduct(product);
				pagerAdapter.update(0);
			}
		});

		quitDialog.show();
	}

	/**
	 * Get view-pager
	 * @return
	 */
	public ViewPager getViewPager() {
		return viewPager;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu){
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return true;
	}
	
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
			//case R.id.testprint:
			//	FindBluetoothDevice();
			//	openBluetoothPrinter();
			//	return true;
            case R.id.logoff:
				logoutUser();
				finish();
                return true;
			case R.id.sinkron:
			//	goSinkron();
				hSinkron();
				return true;
			//case R.id.sinkronall:
			//	goSinkronAll();
			//	return true;
			case R.id.closing:
				goClosing();
				return true;
			case R.id.update:
				goUpdate();
				return true;
			case R.id.export:
				//exportDB();
				cobaExportDB();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

	void FindBluetoothDevice(){
		try{
			bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
			if(bluetoothAdapter==null){

			}
			if(bluetoothAdapter.isEnabled()){
				android.content.Intent enableBT = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
				startActivityForResult(enableBT, 0);
			}

			Set<BluetoothDevice> pairedDevice = bluetoothAdapter.getBondedDevices();
			if(pairedDevice.size()>0){
				for(BluetoothDevice pairedDev:pairedDevice){
					if(pairedDev.getName().equals("58Printer")){
						bluetoothDevice=pairedDev;
						break;
					}
				}
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	/* Open Bluetooth Printer */
	void openBluetoothPrinter() {
		try{
			UUID uuidSting = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
			bluetoothSocket=bluetoothDevice.createRfcommSocketToServiceRecord(uuidSting);
			bluetoothSocket.connect();
			outputStream=bluetoothSocket.getOutputStream();
			inputStream=bluetoothSocket.getInputStream();
			beginListenData();
		}catch (Exception ex){
			ex.printStackTrace();
		}
	}

	/*Modul Printer Data*/
	void beginListenData(){
		try{
			final Handler handler=new Handler();
			final byte delimeter=10;
			stopWorker = false;
			readBufferPosition = 0;
			readBuffer = new byte[1024];


			thread=new Thread(new Runnable() {
				@Override
				public void run() {
					while (!Thread.currentThread().isInterrupted() && !stopWorker){
						try{
							int byteAvailable = inputStream.available();
							if(byteAvailable>0){
								byte[] packetByte = new byte[byteAvailable];
								inputStream.read(packetByte);
								for(int i=0; i<byteAvailable; i++){
									byte b = packetByte[i];
									if(b==delimeter) {
										byte[] encodedByte = new byte[readBufferPosition];
										System.arraycopy(
												readBuffer, 0,
												encodedByte, 0,
												encodedByte.length
										);

										final String data = new String(encodedByte, "US-ASCII");
										readBufferPosition = 0;
										handler.post(new Runnable() {
											@Override
											public void run() {
											}
										});
									}else{
										readBuffer[readBufferPosition++]=b;
									}
								}
							}
						}catch(Exception ex){
							stopWorker=true;
							ex.printStackTrace();
						}
					}
				}
			});

			thread.start();
		}catch (Exception ex){
			ex.printStackTrace();
		}
	}

    private void goSinkron() {
		//Intent newActivity = new Intent(MainActivity.this, SalesActivity.class);
		Intent newActivity = new Intent (MainActivity.this, SalesSync.class);
		startActivity(newActivity);
		MainActivity.this.finish();
	}

	private void cobaExportDB() {
		try {
			File file = new File(DB_PATH + DATABASE_NAME);
			FileInputStream myInput;
			myInput = new FileInputStream(file);

			String outFileName = "/sdcard/Download/com.refresh.db1";
			OutputStream myOutput = new FileOutputStream(outFileName);

			byte[] buffer = new byte[1024];
			int length;
			while ((length = myInput.read(buffer)) > 0) {
				myOutput.write(buffer, 0 , length);
			}

			myOutput.flush();
			myOutput.close();
			myInput.close();
			Toast.makeText(getApplicationContext(),"Berhasil", Toast.LENGTH_LONG).show();
		} catch (Exception e) {
			Toast.makeText(getBaseContext(), e.toString(), Toast.LENGTH_LONG)
					.show();
		}
	}


	private void goUpdate() {
		downloadManager = (DownloadManager)getSystemService(Context.DOWNLOAD_SERVICE);
		Uri uri = Uri.parse("http://braya2.sule-soft.com/appupdate/POS.apk");
		DownloadManager.Request request = new DownloadManager.Request(uri);
		request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
		Long reference = downloadManager.enqueue(request);

		//Intent intent = new Intent(Intent.ACTION_VIEW);
		//intent.setDataAndType(Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/download/" + "POS.apk")), "application/vnd.android.package-archive");
		//intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		//startActivity(intent);
		//MainActivity.this.finish();
	}

	private void InstallApp() {
		//File file = new File(dir, "POS.apk");
		//Intent intent = new Intent(Intent.ACTION_VIEW);
		//intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
		//startActivity(intent);
	}

	private void hSinkron() {
		Intent newActivity = new Intent(MainActivity.this, halSinkron.class);
		startActivity(newActivity);
		MainActivity.this.finish();
	}

	private void goClosing() {
		Intent newActivity = new Intent(MainActivity.this,
				ClosingActivity.class);
		startActivity(newActivity);
		MainActivity.this.finish();
	}

	private void showList(List<Sale> list) {
		saleList = new ArrayList<Map<String, String>>();
		for (Sale sale : list) {
			saleList.add(sale.toMap());
		}
	}
	
	/**
	 * Set language
	 * @param localeString
	 */

	private void logoutUser() {
		session.setLogin(false);

		//db.deleteUsers();
		SQLite.deleteCashier();

		// Launching the login activity
		Intent intent = new Intent(MainActivity.this, LoginActivity.class);
		startActivity(intent);
		MainActivity.this.finish();
		//finish();
	}
	private void setLanguage(String localeString) {
		Locale locale = new Locale(localeString);
		Locale.setDefault(locale);
		Configuration config = new Configuration();
		config.locale = locale;
		LanguageController.getInstance().setLanguage(localeString);
		getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
		Intent intent = getIntent();
		finish();
		startActivity(intent);
	}

}

/**
 * 
 * @author Refresh team
 *
 */
class PagerAdapter extends FragmentStatePagerAdapter {

	private UpdatableFragment[] fragments;
	private String[] fragmentNames;

	/**
	 * Construct a new PagerAdapter.
	 * @param fragmentManager
	 * @param res
	 */
	public PagerAdapter(FragmentManager fragmentManager, Resources res) {
		
		super(fragmentManager);
		
		UpdatableFragment reportFragment = new ReportFragment();
		UpdatableFragment saleFragment = new SaleFragment(reportFragment);
		UpdatableFragment inventoryFragment = new InventoryFragment(
				saleFragment);

		fragments = new UpdatableFragment[] { inventoryFragment, saleFragment,
				reportFragment };
		fragmentNames = new String[] { res.getString(R.string.inventory),
				res.getString(R.string.sale),
				res.getString(R.string.report) };

	}

	@Override
	public Fragment getItem(int i) {
		return fragments[i];
	}

	@Override
	public int getCount() {
		return fragments.length;
	}

	@Override
	public CharSequence getPageTitle(int i) {
		return fragmentNames[i];
	}

	/**
	 * Update
	 * @param index
	 */
	public void update(int index) {
		fragments[index].update();
	}

}