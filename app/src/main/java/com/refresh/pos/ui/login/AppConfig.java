package com.refresh.pos.ui.login;

import com.refresh.pos.techicalservices.AndroidDatabase;

import java.util.ArrayList;
import java.util.HashMap;

public class AppConfig {
    // Server Jasabogaraya
    public static String URL_LOGIN = "https://pos.sule-soft.com/service/ceklogin.php";
    public static String URL_CATALOG = "https://pos.sule-soft.com/service/catalog.php";
    public static String URL_SALE_DET = "https://pos.sule-soft.com/service/sinkronsalesdet.php";
    public static String URL_SALE = "https://pos.sule-soft.com/service/sinkronsales.php";
    public static String URL_REGISTER = "https://pos.sule-soft.com/service/register.php";
    public static String URL_OUTLET = "https://pos.sule-soft.com/service/outlet.php";
    public static String URL_SUMSALES = "https://pos.sule-soft.com/service/sumsales.php";
    public static String URL_UPDATESALE = "https://pos.sule-soft.com/service/updatesales.php";

    //Server Braya2
    //public static String URL_LOGIN = "http://braya2.sule-soft.com/service/ceklogin.php";
    //public static String URL_CATALOG = "http://braya2.sule-soft.com/service/catalog.php";
    //public static String URL_SALE_DET = "http://braya2.sule-soft.com/service/sinkronsalesdet.php";
    //public static String URL_SALE = "http://braya2.sule-soft.com/service/sinkronsales.php";
    //public static String URL_REGISTER = "http://braya2.sule-soft.com/service/register.php";
    //public static String URL_OUTLET = "http://braya2.sule-soft.com/service/outlet.php";
    //public static String URL_SUMSALES = "http://braya2.sule-soft.com/service/sumsales.php";
    //public static String URL_UPDATESALE = "http://braya2.sule-soft.com/service/updatesales.php";
}
