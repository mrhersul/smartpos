package com.refresh.pos.ui.login;

public class DataCounter {
    private Integer total;

    public DataCounter() {

    }

    public DataCounter(Integer total) {
        this.total = total;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }
}
