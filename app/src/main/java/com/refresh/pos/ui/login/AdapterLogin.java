package com.refresh.pos.ui.login;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.refresh.pos.R;

import org.w3c.dom.Text;

import java.util.List;

public class AdapterLogin extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<DataLogin> items;

    public AdapterLogin(Activity activity, List<DataLogin> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount() { return items.size(); }

    @Override
    public Object getItem(int location) {
        return items.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_login, null);

        TextView outlet_id = (TextView) convertView.findViewById(R.id.outlet_id);
        TextView cashier_id = (TextView) convertView.findViewById(R.id.cashier_id);
        TextView outlet = (TextView) convertView.findViewById(R.id.outlet);
        TextView cashier = (TextView) convertView.findViewById(R.id.cashier);
        DataLogin dataLogin = items.get(position);

        outlet_id.setText(dataLogin.getOutlet_id());
        cashier_id.setText(dataLogin.getCashier_id());
        outlet.setText(dataLogin.getOutlet());
        cashier.setText(dataLogin.getCashier());

        return convertView;
    }
}
