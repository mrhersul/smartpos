package com.refresh.pos.ui.sale;

public class DataHelper {
    private String id, name, price, qty;

    public DataHelper() {

    }

    public DataHelper (String id, String name, String price, String qty) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.qty = qty;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) { this.id = id; }

    public String getName() {
        return name;
    }
    public void setName(String name) { this.name = name; }

    public String getPrice() {
        return price;
    }
    public void setPrice(String price) { this.price = price; }

    public String getQty() {
        return qty;
    }
    public void setQty(String qty) { this.qty = qty; }
}
