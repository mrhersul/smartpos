package com.refresh.pos.ui.sale;

import com.refresh.pos.R;
import com.refresh.pos.domain.DateTimeStrategy;
import com.refresh.pos.domain.outlet.Outlet;
import com.refresh.pos.techicalservices.AndroidDatabase;
import com.refresh.pos.techicalservices.sale.SaleDao;
import com.refresh.pos.ui.MainActivity;
import com.refresh.pos.ui.SharedData;
import com.refresh.pos.ui.component.UpdatableFragment;
import com.refresh.pos.ui.login.AdapterCounter;
import com.refresh.pos.ui.login.AdapterLogin;
import com.refresh.pos.ui.login.DataCounter;
import com.refresh.pos.ui.login.DataLogin;
import com.refresh.pos.ui.login.SQLiteHandler;
import com.refresh.pos.ui.login.SessionManager;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.util.Printer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.UUID;

/**
 * A dialog for input a money for sale.
 * @author Refresh Team
 *
 */
@SuppressLint("ValidFragment")
public class PaymentFragmentDialog extends DialogFragment {
	BluetoothAdapter bluetoothAdapter;
	BluetoothSocket bluetoothSocket;
	BluetoothDevice bluetoothDevice;

	java.io.OutputStream outputStream;
	InputStream inputStream;
	Thread thread;

	byte[] readBuffer;
	int readBufferPosition;
	volatile boolean stopWorker;

	private TextView totalPrice;
	private EditText input;
	private EditText voucher;
	private EditText kartu;
	private EditText cashless;
	private EditText foc;
	private EditText noref;
	private EditText pengasong;
	private EditText statusPrint;
	private Button clearButton;
	private Button confirmButton;
	private Button connectButton;
	private String strtext;
	private String strname;
	private String strqty;
	private String strprice;
	private UpdatableFragment saleFragment;
	private UpdatableFragment reportFragment;
	private Spinner spTipeBayar;
	private String PrinterName = "58Printer";
	private String o_id, c_id, o, c, tot;
	private String HOutlet, HCashier, HCounter;

	ListView listView;
	AlertDialog.Builder dialog;
	List<DataLogin> itemList = new ArrayList<DataLogin>();
	AdapterLogin adapter;
	List<DataCounter> itemcounter = new ArrayList<DataCounter>();
	AdapterCounter adapterCounter;
	AndroidDatabase SQLite = new AndroidDatabase(getActivity());

	public static final String TAG_OUTLETID = "outlet_id";
	public static final String TAG_CASHIERID = "cashier_id";
	public static final String TAG_OUTLET = "outlet";
	public static final String TAG_CASHIER = "cashier";
	public static final String TAG_TOTAL = "total";

	SharedData sharedData = SharedData.getInstance();
	/**
	 * Construct a new PaymentFragmentDialog.
	 * @param saleFragment
	 * @param reportFragment
	 */
	public PaymentFragmentDialog(UpdatableFragment saleFragment, UpdatableFragment reportFragment) {
		super();
		this.saleFragment = saleFragment;
		this.reportFragment = reportFragment;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.dialog_payment_cashless, container,false);
		strtext=getArguments().getString("edttext");
		Double tax = Math.floor(Double.parseDouble(strtext.toString()) * 1.1);
		input = (EditText) v.findViewById(R.id.dialog_saleInput);
		voucher = (EditText) v.findViewById(R.id.voucher);
		kartu = (EditText) v.findViewById(R.id.kartu);
		cashless = (EditText) v.findViewById(R.id.cashless);
		foc = (EditText) v.findViewById(R.id.foc);
		noref = (EditText) v.findViewById(R.id.noref);
		pengasong = (EditText) v.findViewById(R.id.pengasong);
		totalPrice = (TextView) v.findViewById(R.id.payment_total);
		totalPrice.setText(strtext);
		//spTipeBayar = (Spinner) v.findViewById(R.id.spinner_tipe);
		statusPrint = (EditText) v.findViewById(R.id.statusPrint);

		SQLite = new AndroidDatabase(getActivity());

		strname = getArguments().getSerializable("list").toString();
		clearButton = (Button) v.findViewById(R.id.clearButton);
		clearButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				end();
			}
		});

		connectButton = (Button) v.findViewById(R.id.connectButton);
		connectButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
					if (pengasong.length() > 0) {
						sharedData.setPengasong(pengasong.getText().toString());
					} else {
						sharedData.setPengasong("");
					}
					//sharedData.setPayment(spTipeBayar.getSelectedItem().toString());
					sharedData.setTunai(input.getText().toString());
					sharedData.setVoucher(voucher.getText().toString());
					sharedData.setKartu(kartu.getText().toString());
					sharedData.setCashless(cashless.getText().toString());
					sharedData.setFoc(foc.getText().toString());
					sharedData.setNoref(noref.getText().toString());

					/*Cari Printer Bluetooth */

					String inputString = input.getText().toString();
					String vouString = voucher.getText().toString();
					String kartuString = kartu.getText().toString();
					String clsString = cashless.getText().toString();
					String focString = foc.getText().toString();


					ArrayList<HashMap<String, String>> row = SQLite.getLogin();
					o_id = row.get(0).get(TAG_OUTLETID);
					c_id = row.get(0).get(TAG_CASHIERID);
					o = row.get(0).get(TAG_OUTLET);
					c = row.get(0).get(TAG_CASHIER);

					sharedData.setCashier(c);
					sharedData.setOutlet(o_id);

					ArrayList<HashMap<String, String>> cnt = SQLite.getCounter();
					tot = cnt.get(0).get(TAG_TOTAL);
					int Cou = tot.length();
					if (Cou == 1) {
						HCounter = "00";
					} else if (Cou == 2) {
						HCounter = "0";
					} else {
						HCounter = "";
					}
					sharedData.setPart2("#" + HCounter + tot.toString());

					// Len Outlet
					String Otl = String.valueOf(o_id).toString();
					int cOtl = Otl.length();
					if (cOtl > 1) {
						HOutlet = Otl.toString();
					} else {
						HOutlet = "0" + Otl.toString();
					}

					// Len Cashier
					String Csh = String.valueOf(c_id).toString();
					int cCsh = Csh.length();
					if (cCsh > 1) {
						HCashier = Csh.toString();
					} else {
						HCashier = "0" + Csh.toString();
					}

					sharedData.setNost(sharedData.getPart1() + HOutlet + HCashier + sharedData.getPart2());

					if (inputString.equals("")) {
						Toast.makeText(getActivity().getBaseContext(), getResources().getString(R.string.please_input_all), Toast.LENGTH_SHORT).show();
						return;
					}
					//double a = Double.parseDouble(strtext);
					double a = Double.parseDouble(strtext);
					double b = Double.parseDouble(inputString);
					double g = Double.parseDouble(vouString);
					double d = Double.parseDouble(kartuString);
					double e = Double.parseDouble(clsString);
					double f = Double.parseDouble(focString);
					if ((b+g+d+e+f) < a) {
						Toast.makeText(getActivity().getBaseContext(), getResources().getString(R.string.need_money) + " " + ((b+g+d+e+f) - a), Toast.LENGTH_SHORT).show();
					} else {
						//print Sruk
						try{
							FindBluetoothDevice();
							openBluetoothPrinter();
							openCashDrawer();

							/*Mulai Print */
							String msg = "www.jasabogaraya.com";
							msg+="\n";
							msg+=DateTimeStrategy.getCurrentTime().toString();
							msg+="\n";
							msg+="Struk " + "\u0009" + ": " + sharedData.getNost();
							//msg+="Struk " + "\u0009" + ": " + sharedData.getPart1() + HOutlet + HCashier + sharedData.getPart2();
							msg+="\n";
							msg+="Outlet " + "\u0009" + ": " + o.toString() ;
							msg+="\n";
							msg+="Cashier " + ": " + c.toString();
							msg+="\n";
							msg+="--------------------------------";
							msg+="\n";
							/*hapus tanda [ */
							String hapus1 = strname.replace("[","");
							String hapus2 = hapus1.replace("]","");
							//msg+=hapus2.toString();
							//msg+="\n";
							String rubah = hapus2.replace("},"," :");
							String hapus3 = rubah.replace("}","");
							//msg+=hapus3.toString();
							//msg+="\n";
							String[] items = hapus3.trim().split(":");
							for (int x=0; x < items.length; x++) {
								StringTokenizer prn = new StringTokenizer(items[x], ",");
								String qty = prn.nextToken().trim().replace("quantity=","");
								String nm = prn.nextToken().trim().replace("name=","");
								String pri = prn.nextToken().trim().replace("price=","");
								String fpri = pri.trim().replace(".0", "");

								//msg+=nm.toString();
								msg+=nm.replace("name=", "").toString();
								msg+="\n";
								msg+=" x" + qty.substring(1,qty.length()).toString();
								msg+="\u0009";
								msg+=fpri.toString();
								msg+="\n";
							}
							//Double Tot = Math.floor(Double.parseDouble(strtext.toString()) * 1.1);
							Double Ppn = Math.floor(Double.parseDouble(strtext.toString()) * (0.0909));
							String nPpn = Ppn.toString().replace(".0","");
							String nTot = strtext.toString().replace(".0","");
							msg+="--------------------------------";
							msg+="\n";
							msg+="TOTAL ";
							msg+="\u0009";
							//msg+=": " + strtext.toString();
							msg+=": " + nTot.toString();
							msg+="\n";
							msg+="Pb1 10 % ";
							msg+="\u0009";
							msg+=": " + nPpn.toString();
							msg+="\n \n";
							msg+="Tunai Rp.";
							msg+="\u0009 \u0009";
							msg+=": " + input.getText().toString();
							msg+="\n";
							msg+="Voucher ";
							msg+="\u0009";
							msg+=": " + voucher.getText().toString();
							msg+="\n";
							msg+="Kartu ";
							msg+="\u0009 \u0009";
							msg+=": " + kartu.getText().toString();
							msg+="\n";
							msg+="Cashless ";
							msg+="\u0009";
							msg+=": " + cashless.getText().toString();
							msg+="\n";
							msg+="FOC ";
							msg+="\u0009 \u0009";
							msg+=": " + foc.getText().toString();
							msg+="\n \n";
							msg+="Pengasong ";
							msg+="\u0009";
							msg+=": " + pengasong.getText().toString();
							msg+="\n \n \n \n";
							outputStream.write(msg.getBytes());
							statusPrint.setText("Print Send");
						} catch (Exception ex){
							statusPrint.setText("Print Disconnect");
							ex.printStackTrace();
						}

						/*
						Bundle bundle = new Bundle();
						bundle.putString("edttext", b - a + "");
						EndPaymentFragmentDialog newFragment = new EndPaymentFragmentDialog(
								saleFragment, reportFragment);
						newFragment.setArguments(bundle);
						newFragment.show(getFragmentManager(), "");
						end();
						*/
					}

				}

		});

		confirmButton = (Button) v.findViewById(R.id.confirmButton);
		confirmButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (pengasong.length() > 0) {
					sharedData.setPengasong(pengasong.getText().toString());
				} else {
					sharedData.setPengasong("NONE");
				}
				String stprint = statusPrint.getText().toString();
				if (stprint.length() < 1) {
					Toast.makeText(getActivity().getBaseContext(),"Struk Belum Tercetak..", Toast.LENGTH_LONG).show();
					return;
				}
				//sharedData.setPayment(spTipeBayar.getSelectedItem().toString());
				sharedData.setTunai(input.getText().toString());
				sharedData.setVoucher(voucher.getText().toString());
				sharedData.setKartu(kartu.getText().toString());
				sharedData.setCashless(cashless.getText().toString());
				sharedData.setFoc(foc.getText().toString());
				sharedData.setNoref(noref.getText().toString());

				String inputString = input.getText().toString();
				String vouString = voucher.getText().toString();
				String cardString = kartu.getText().toString();
				String clsString = cashless.getText().toString();
				String focString = foc.getText().toString();

				ArrayList<HashMap<String, String>> row = SQLite.getLogin();
				o_id = row.get(0).get(TAG_OUTLETID);
				c_id = row.get(0).get(TAG_CASHIERID);
				o = row.get(0).get(TAG_OUTLET);
				c = row.get(0).get(TAG_CASHIER);

				sharedData.setCashier(c);
				sharedData.setOutlet(o_id);

				ArrayList<HashMap<String, String>> cnt = SQLite.getCounter();
				tot = cnt.get(0).get(TAG_TOTAL);
				int Cou = tot.length();
				if (Cou == 1) {
					HCounter = "00";
				} else if (Cou == 2) {
					HCounter = "0";
				} else {
					HCounter = "";
				}
				sharedData.setPart2("#" + HCounter + tot.toString());

				// Len Outlet
				String Otl = String.valueOf(o_id).toString();
				int cOtl = Otl.length();
				if (cOtl > 1) {
					HOutlet = Otl.toString();
				} else {
					HOutlet = "0" + Otl.toString();
				}

				// Len Cashier
				String Csh = String.valueOf(c_id).toString();
				int cCsh = Csh.length();
				if (cCsh > 1) {
					HCashier = Csh.toString();
				} else {
					HCashier = "0" + Csh.toString();
				}

				sharedData.setNost(sharedData.getPart1() + HOutlet + HCashier + sharedData.getPart2());

				//if (inputString.equals("") || vouString.equals("") || kartuString.equals("") || clsString.equals("") || focString.equals("")) {
				//	Toast.makeText(getActivity().getBaseContext(), getResources().getString(R.string.please_input_all), Toast.LENGTH_SHORT).show();
				//	return;
				//}
				double a = Double.parseDouble(strtext);
				double b = Double.parseDouble(inputString);
				double c = Double.parseDouble(vouString);
				double d = Double.parseDouble(cardString);
				double e = Double.parseDouble(clsString);
				double f = Double.parseDouble(focString);
				if ((b+c+d+e+f) < a) {
					Toast.makeText(getActivity().getBaseContext(), getResources().getString(R.string.need_money) + " " + ((b+c+d+e+f) - a), Toast.LENGTH_SHORT).show();
				} else {
					//Toast.makeText(getActivity().getBaseContext(), " Tunai " + b + " , Voucher " + c + " , Kartu " + d + " , " +
					//		"cashless " + e + " , foc " + f,  Toast.LENGTH_SHORT).show();
					Bundle bundle = new Bundle();
					bundle.putString("edttext", (b+c+d+e+f) - a + "");
					bundle.putString("tunaitext", b + "");
					bundle.putString("voutext", c + "");
					bundle.putString("kartutext", d + "");
					bundle.putString("cashtext", e + "");
					bundle.putString("foctext", f + "");
					bundle.putString("noreftext", noref.getText().toString());
					EndPaymentFragmentDialog newFragment = new EndPaymentFragmentDialog(
							saleFragment, reportFragment);
					newFragment.setArguments(bundle);
					newFragment.show(getFragmentManager(), "");
					end();
				}

			}
		});
		return v;
	}

	void FindBluetoothDevice(){
		try{
			bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
			if(bluetoothAdapter==null){
				statusPrint.setText("Bluetooth OFF");
			}
			if(bluetoothAdapter.isEnabled()){
				android.content.Intent enableBT = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
				startActivityForResult(enableBT, 0);
			}

			Set<BluetoothDevice> pairedDevice = bluetoothAdapter.getBondedDevices();
			if(pairedDevice.size()>0){
				for(BluetoothDevice pairedDev:pairedDevice){
					if(pairedDev.getName().equals("58Printer") || pairedDev.getName().equals("MPT-II") || pairedDev.getName().equals("BlueTooth Printer")){
						bluetoothDevice=pairedDev;
						break;
					}
				}
			}
			statusPrint.setText("Connected");
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	/* Open Bluetooth Printer */
	void openBluetoothPrinter() throws IOException{
		try{
			UUID uuidSting = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
			bluetoothSocket=bluetoothDevice.createRfcommSocketToServiceRecord(uuidSting);
			bluetoothSocket.connect();
			outputStream=bluetoothSocket.getOutputStream();
			inputStream=bluetoothSocket.getInputStream();
			beginListenData();
			statusPrint.setText("Bluetooth Open");
		}catch (Exception ex){
			ex.printStackTrace();
		}
	}

	/*Modul Printer Data*/
	void beginListenData(){
		try{
			final Handler handler=new Handler();
			final byte delimeter=10;
			stopWorker = false;
			readBufferPosition = 0;
			readBuffer = new byte[1024];


			thread=new Thread(new Runnable() {
				@Override
				public void run() {
					while (!Thread.currentThread().isInterrupted() && !stopWorker){
						try{
							int byteAvailable = inputStream.available();
							if(byteAvailable>0){
								byte[] packetByte = new byte[byteAvailable];
								inputStream.read(packetByte);
								for(int i=0; i<byteAvailable; i++){
									byte b = packetByte[i];
									if(b==delimeter) {
										byte[] encodedByte = new byte[readBufferPosition];
										System.arraycopy(
												readBuffer, 0,
												encodedByte, 0,
												encodedByte.length
										);

										final String data = new String(encodedByte, "US-ASCII");
										readBufferPosition = 0;
										handler.post(new Runnable() {
											@Override
											public void run() {
											}
										});
									}else{
										readBuffer[readBufferPosition++]=b;
									}
								}
							}
						}catch(Exception ex){
							stopWorker=true;
							ex.printStackTrace();
						}
					}
				}
			});

			thread.start();
		}catch (Exception ex){
			ex.printStackTrace();
		}
	}

	/**
	 * Fungsi untuk membuka cash drawer dengan ESC/POS command
	 * Lihat: http://keyhut.com/popopen.htm
	 *
	 * @return open status
	 */
	public boolean openCashDrawer() {
		try {
			byte[] bytes = intArrayToByteArray(new int[]{27, 112, 0, 50, 250});
			outputStream.write(bytes);
			return true;
		} catch (IOException e) {
			//Log.e(TAG, "Open drawer error", e);
			return false;
		}
	}

	private byte[] intArrayToByteArray(int[] Iarr) {
		byte[] bytes = new byte[Iarr.length];
		for (int i = 0; i < Iarr.length; i++) {
			bytes[i] = (byte) (Iarr[i] & 0xFF);
		}
		return bytes;
	}
	/**
	 * End.
	 */
	private void end() {
		this.dismiss();
	}
	

}
