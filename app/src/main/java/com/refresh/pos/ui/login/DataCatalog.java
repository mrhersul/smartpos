package com.refresh.pos.ui.login;

import com.android.volley.toolbox.StringRequest;

public class DataCatalog {
    private String name, barcode;
    private Integer outlet;
    private Double price;

    public DataCatalog() {
    }

    public DataCatalog (Integer outlet, String name, String barcode, Double price) {
        this.outlet = outlet;
        this.name = name;
        this.barcode = barcode;
        this.price = price;
    }

    public Integer getOutlet() {
        return outlet;
    }
    public void setOutlet(Integer outlet) { this.outlet = outlet; }

    public String getName() {
        return name;
    }
    public void setName(String name) { this.name = name; }

    public String getBarcode() {
        return barcode;
    }
    public void setBarcode(String barcode) { this.barcode = barcode; }

    public Double getPrice() {
        return price;
    }
    public void setPrice(double price) { this.price = price; }
}
