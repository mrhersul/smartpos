package com.refresh.pos.ui.sale;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.refresh.pos.R;
import com.refresh.pos.techicalservices.AndroidDatabase;
import com.refresh.pos.ui.MainActivity;
import com.refresh.pos.ui.SharedData;
import com.refresh.pos.ui.login.AppConfig;
import com.refresh.pos.ui.login.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SalesSync extends Activity {
    ListView listView;
    AlertDialog.Builder dialog;
    List<DataSale> itemList = new ArrayList<DataSale>();
    AdapterSale adapter;
    AndroidDatabase SQLite = new AndroidDatabase(this);
    int success;

    RequestQueue requestQueue;
    ProgressDialog progressDialog;

    public static final String TAG_ID = "_id";
    private static final String TAG_SALEID = "sale_id";
    private static final String TAG_NOST = "nost";
    public static final String TAG_PRODUCTID = "product_id";
    public static final String TAG_QUANTITY = "quantity";
    public static final String TAG_UNITPRICE = "unit_price";
    public static final String TAG_PAYMENT = "payment";
    public static final String TAG_START = "start_time";
    public static final String TAG_CASHIER = "cashier";
    public static final String TAG_OUTLET = "outlet_id";
    public static final String TAG_PENGASONG = "pengasong";
    public static final String TAG_SYNC = "sync";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_TUNAI = "tunai";
    private static final String TAG_VOUCHER = "voucher";
    private static final String TAG_KARTU = "kartu";
    private static final String TAG_CASHLESS = "cashless";
    private static final String TAG_FOC = "foc";
    private static final String TAG_NOREF = "noref";

    String M_id, M_SaleID, M_Nost, M_ProductID, M_Quantity, M_UnitPrice, M_Payment, M_Start, M_Cashier, M_Outlet, M_Sync, M_Pengasong,
    M_Tunai, M_Voucher, M_Kartu, M_Cashless, M_Foc, M_Noref;

    SharedData sharedData = SharedData.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceSaved) {
        super.onCreate(savedInstanceSaved);
        setContentView(R.layout.activity_sales);
        SQLite = new AndroidDatabase(getApplicationContext());
        listView = (ListView) findViewById(R.id.list_view);
        adapter = new AdapterSale(SalesSync.this, itemList);
        listView.setAdapter(adapter);

        getAllData();

        KeluarMain();
    }

    private void KeluarMain(){
        Intent intent = new Intent(SalesSync.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void getAllData() {
        ArrayList<HashMap<String, String>> rows = SQLite.getLogin();
        M_Outlet = rows.get(0).get(TAG_OUTLET);
        M_Cashier = rows.get(0).get(TAG_CASHIER);

        ArrayList<HashMap<String, String>> row = SQLite.getAllSalesDetail();
        for (int i = 0; i < row.size(); i++) {
            M_id = row.get(i).get(TAG_ID);
            M_SaleID = row.get(i).get(TAG_SALEID);
            M_Nost = row.get(i).get(TAG_NOST);
            M_ProductID = row.get(i).get(TAG_PRODUCTID);
            M_Quantity = row.get(i).get(TAG_QUANTITY);
            M_UnitPrice = row.get(i).get(TAG_UNITPRICE);
            M_Payment = row.get(i).get(TAG_PAYMENT);
            M_Start = row.get(i).get(TAG_START);
            //M_Cashier = row.get(i).get(TAG_CASHIER);
            M_Sync = row.get(i).get(TAG_SYNC);
            if (row.get(i).get(TAG_PENGASONG) != null) {
                M_Pengasong = row.get(i).get(TAG_PENGASONG);
            } else {
                M_Pengasong = "NONE";
            }
            M_Tunai = row.get(i).get(TAG_TUNAI);
            M_Voucher = row.get(i).get(TAG_VOUCHER);
            M_Kartu = row.get(i).get(TAG_KARTU);
            M_Cashless = row.get(i).get(TAG_CASHLESS);
            M_Foc = row.get(i).get(TAG_FOC);
            M_Noref = row.get(i).get(TAG_NOREF);

            //simpan server
            //simpan_server(M_id, M_SaleID, M_Nost, M_ProductID, M_Quantity, M_UnitPrice, M_Payment, M_Start, M_Outlet, M_Cashier);
            simpan_server(M_id, M_SaleID, M_Nost, M_ProductID, M_Quantity, M_UnitPrice, M_Payment, M_Start, M_Outlet, M_Cashier, M_Pengasong,
                    M_Tunai, M_Voucher, M_Kartu, M_Cashless, M_Foc, M_Noref);

            DataSale data = new DataSale();

            data.setId(String.valueOf(M_id));
            data.setSaleid(M_SaleID);
            data.setNost(M_Nost);
            data.setProductid(M_ProductID);
            data.setQuantity(M_Quantity);
            data.setUnit_price(M_UnitPrice);
            data.setPayment(M_Payment);
            data.setStart_time(M_Start);
            data.setOutlet(M_Outlet);
            data.setCashier(M_Cashier);
            data.setSync(M_Sync);
            data.setPengasong(M_Pengasong);
            data.setTunai(M_Tunai);
            data.setVoucher(M_Voucher);
            data.setKartu(M_Kartu);
            data.setCashless(M_Cashless);
            data.setFoc(M_Foc);
            data.setNoref(M_Noref);

            itemList.add(data);

            //SQLite.updateSyncSales(M_id);
        }
        //Toast.makeText(SalesActivity.this,M_Cashier,Toast.LENGTH_LONG).show();
        adapter.notifyDataSetChanged();
    }

    public void AmbilData() {

    }
    private void simpan_server(final String id, final String SaleID, final String ost, final String ProductID,
                               final String Quantity, final String UnitPrice, final String Payment,
                               final String Start, final String Outlet, final String Cashier, final String Pengasong,
                               final String Tunai, final String Voucher, final String Kartu, final String Cashless,
                               final String Foc, final String Noref) {
        StringRequest strReq = new StringRequest(Request.Method.POST, AppConfig.URL_SALE_DET, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    success = jObj.getInt(TAG_SUCCESS);
                    if (success == 1) {
                        // Toast.makeText(getBaseContext(), "Success...", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getBaseContext(), "Gagal.." + ost.toString(), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters ke post url
                Map<String, String> params = new HashMap<String, String>();
                // jika id kosong maka simpan, jika id ada nilainya maka update
                params.put("_id", id);
                params.put("sale_id", SaleID);
                params.put("nost", ost);
                params.put("product_id", ProductID);
                params.put("quantity", Quantity);
                params.put("unit_price", UnitPrice);
                params.put("payment", Payment);
                params.put("start_time", Start);
                params.put("outlet_id", Outlet);
                params.put("cashier", Cashier);
                params.put("pengasong", Pengasong);
                params.put("tunai", Tunai);
                params.put("voucher", Voucher);
                params.put("kartu", Kartu);
                params.put("cashless", Cashless);
                params.put("foc", Foc);
                params.put("noref",Noref);
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(strReq);
    }
}
