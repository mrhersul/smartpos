package com.refresh.pos.ui.sale;

public class DataSinkronLokal {
    private String lokalSide;

    public DataSinkronLokal(String lokalSide) {
        this.lokalSide = lokalSide;
    }

    public String getLokalSide() {
        return lokalSide;
    }

    public void setLokalSide(String lokalSide) {
        this.lokalSide = lokalSide;
    }
}
