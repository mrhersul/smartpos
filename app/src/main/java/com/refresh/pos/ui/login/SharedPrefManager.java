package com.refresh.pos.ui.login;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefManager {
    public static final String SP_Outlet = "spOutlet";
    public static final String SP_Cashier = "spCashier";

    public static final String SP_SUDAH_LOGIN = "spSudahLogin";

    SharedPreferences sp;
    SharedPreferences.Editor spEditor;

    //public SharedPreferences(Context context) {
     //   sp = context.getSharedPreferences(SP_Outlet, context.MODE_PRIVATE);
     //   spEditor = sp.edit();
    //}

    public void saveSPString(String keySP, String value){
        spEditor.putString(keySP, value);
        spEditor.commit();
    }

    public void saveSPInt(String keySP, int value){
        spEditor.putInt(keySP, value);
        spEditor.commit();
    }

    public void saveSPBoolean(String keySP, boolean value){
        spEditor.putBoolean(keySP, value);
        spEditor.commit();
    }

    public String getSP_Cashier(){
        return sp.getString(SP_Cashier, "");
    }

    public Boolean getSPSudahLogin() {
        return sp.getBoolean(SP_SUDAH_LOGIN, false);
    }
}
