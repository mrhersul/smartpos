package com.refresh.pos.ui.login;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.refresh.pos.R;
import com.refresh.pos.domain.inventory.Inventory;
import com.refresh.pos.domain.inventory.Product;
import com.refresh.pos.domain.sale.Register;
import com.refresh.pos.techicalservices.AndroidDatabase;
import com.refresh.pos.techicalservices.Database;
import com.refresh.pos.techicalservices.DatabaseContents;
import com.refresh.pos.techicalservices.inventory.InventoryDaoAndroid;
import com.refresh.pos.ui.MainActivity;
import com.refresh.pos.domain.inventory.ProductCatalog;
import com.refresh.pos.techicalservices.NoDaoSetException;

import com.refresh.pos.ui.SharedData;
import com.refresh.pos.ui.component.UpdatableFragment;
import com.refresh.pos.ui.inventory.AddProductDialogFragment;
import com.refresh.pos.ui.inventory.InventoryFragment;

import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.net.URLConnection;

public class LoginActivity extends Activity {
    private static final String TAG = RegisterActivity.class.getSimpleName();
    private Button btnLogin;
    private Button btnRefresh;
    private TextView inputOutlet;
    private EditText inputCashier;
    private EditText inputPassword;
    private ProgressDialog pDialog;
    private SessionManager session;
    //private SQLiteHandler db;
    private UpdatableFragment fragment;

    private ProductCatalog productCatalog;
    private String outlet_id, nama_outlet;

    Spinner spinner_outlet;
    Adapter adapter;
    List<Data> listOutlet = new ArrayList<Data>();
    SharedData sharedData = SharedData.getInstance();

    AndroidDatabase SQLite = new AndroidDatabase(this);

    public static final String TAG_ID = "_id";
    public static final String TAG_OUTLET = "outlet";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        spinner_outlet = (Spinner) findViewById(R.id.spinner_outlet);
        inputOutlet = (TextView) findViewById(R.id.outlet);
        inputCashier = (EditText) findViewById(R.id.cashier);
        inputPassword = (EditText) findViewById(R.id.password);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnRefresh = (Button) findViewById(R.id.btnRefresh);

        spinner_outlet.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                inputOutlet.setText(listOutlet.get(position).getId());
                nama_outlet = listOutlet.get(position).getOutlet();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        adapter = new Adapter(LoginActivity.this, listOutlet) ;
        spinner_outlet.setAdapter(adapter);
        SQLite = new AndroidDatabase(getApplicationContext());

        CallData();
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        // Session manager
        session = new SessionManager(getApplicationContext());

        // Check if user is already logged in or not
        if (session.isLoggedIn()) {
            // User is already logged in. Take him to main activity
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }

        // Refresh Button Click Event
        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CallData();
            }
        });

        // Login button Click Event
        btnLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                String cashier = inputCashier.getText().toString().trim();
                String password = inputPassword.getText().toString().trim();
                String outlet_id = inputOutlet.getText().toString();
                sharedData.setOutlet(nama_outlet.toString());
                sharedData.setOutlet_id(Integer.parseInt(outlet_id));
                //SQLite.insertCashier(nama_outlet.toString(), cashier.toString());

                // Check for empty data in the fo
                if (!"".equals(cashier) && !"".equals(password)) {
                    checkLogin(cashier, password, outlet_id);
                } else {
                    // Prompt user to enter credentials
                    Toast.makeText(getApplicationContext(),
                            "Please enter the credentials!", Toast.LENGTH_LONG)
                            .show();
                }
            }

        });

    }

    private void CallData() {
        listOutlet.clear();
        JsonArrayRequest jArr = new JsonArrayRequest(AppConfig.URL_OUTLET,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.e(TAG, response.toString());

                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);

                                Data item = new Data();

                                item.setId(obj.getString(TAG_ID));
                                item.setOutlet(obj.getString(TAG_OUTLET));

                                listOutlet.add(item);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        adapter.notifyDataSetChanged();
                        hideDialog();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //VolleyLog.e(TAG, "Error: " + error.getMessage());
                VolleyLog.e(TAG, "Jaringan Internet Terputus.... Klik Tombol Refresh!!");
                Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        });

        AppController.getInstance().addToRequestQueue(jArr);
    }

    /**
     * function to verify login details in mysql db
     * */
    private void checkLogin(final String cashier, final String password, final String outlet) {
        // Tag used to cancel the request
        String tag_string_req = "req_login";

        pDialog.setMessage("Logging in ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_LOGIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Login Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        // user successfully logged in
                        // Create login session
                        session.setLogin(true);

                        // Now store the user in SQLite
                        //String uid = jObj.getString("uid");

                        JSONObject user = jObj.getJSONObject("user");
                        String cashier_id = user.getString("_id");
                        String outlet = user.getString("outlet");
                        String cashier = user.getString("cashier");

                        // Inserting row in users table
                        //db.addUser(outlet, cashier);
                        SQLite.insertCashier(Integer.parseInt(inputOutlet.getText().toString()),Integer.parseInt(cashier_id),
                                nama_outlet.toString(), cashier.toString());

                        // Simpan Outlet Global
                        sharedData.setCashier(cashier.toString());
                        sharedData.setCashier_id(Integer.parseInt(cashier_id.toString()));

                        //Sync Product
                        syncProduct();
                        //fragment.update();

                        // Launch main activity
                        Intent intent = new Intent(LoginActivity.this,
                                MainActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("cashier", cashier);
                params.put("password", password);
                params.put("outlet", outlet);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void syncProduct() {
        try {
            productCatalog = Inventory.getInstance().getProductCatalog();
        } catch (NoDaoSetException e) {
            e.printStackTrace();
        }

        productCatalog.clearProductCatalog();
        productCatalog.clearStock();
        outlet_id = sharedData.getOutlet_id().toString();

        String url_catalog = "https://data.jasabogaraya.com/service/catalog.php?outlet_id="+outlet_id;
        //ambil data array
        JsonArrayRequest jArr = new JsonArrayRequest(url_catalog,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        Toast.makeText(LoginActivity.this,"Mulai List Barang..",
                                Toast.LENGTH_LONG).show();
                        if (response.length() > 0) {

                            for (int i = 0; i < response.length(); i++) {
                                try {
                                    JSONObject jObj = response.getJSONObject(i);
                                    String id = jObj.getString("_id");
                                    String name = jObj.getString("name");
                                    String barcode = jObj.getString("barcode");
                                    String price = jObj.getString("unit_price");

                                    boolean success = productCatalog.addProduct(Integer.parseInt(id),name
                                            .toString(), barcode.toString(), Double.parseDouble(price
                                            .toString()));
                                    if (success) {
                                        //fragment.update();

                                    } else {
                                        Toast.makeText(LoginActivity.this,
                                                "Gagal...",
                                                Toast.LENGTH_SHORT).show();
                                        //fragment.update();
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            Toast.makeText(LoginActivity.this,
                                    "List Catalog Selesai..",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(LoginActivity.this,
                                    "Tidak Ada Data", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(LoginActivity.this,
                        "Koneksi Internet Terputus..", Toast.LENGTH_SHORT).show();
            }
        });
        AppController.getInstance().addToRequestQueue(jArr);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
