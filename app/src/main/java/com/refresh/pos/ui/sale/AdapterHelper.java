package com.refresh.pos.ui.sale;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.refresh.pos.R;

import java.util.List;

public class AdapterHelper extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<DataHelper> items;

    public AdapterHelper(Activity activity, List<DataHelper> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount() { return items.size(); }

    @Override
    public Object getItem(int location) { return items.get(location); }

    @Override
    public long getItemId(int position) { return position; }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.layout_closing, null);

        //Button clearButton = (Button) convertView.findViewById(R.id.clearButton);
        //Button confirmButton = (Button) convertView.findViewById(R.id.confirmButton);
        TextView id = (TextView) convertView.findViewById(R.id.id);
        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView qty = (TextView) convertView.findViewById(R.id.qty);
        TextView price = (TextView) convertView.findViewById(R.id.price);

        DataHelper data = items.get(position);

        id.setText(data.getId());
        name.setText(data.getName());
        qty.setText(data.getQty());
        price.setText(data.getPrice());
        return convertView;

    }
}
