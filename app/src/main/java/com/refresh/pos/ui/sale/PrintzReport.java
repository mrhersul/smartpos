package com.refresh.pos.ui.sale;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ListView;

import com.refresh.pos.R;
import com.refresh.pos.domain.DateTimeStrategy;
import com.refresh.pos.techicalservices.AndroidDatabase;
import com.refresh.pos.ui.MainActivity;
import com.refresh.pos.ui.SharedData;
import com.refresh.pos.ui.login.AdapterLogin;
import com.refresh.pos.ui.login.DataLogin;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.text.ParseException;

public class PrintzReport extends Activity {
    BluetoothAdapter bluetoothAdapter;
    BluetoothSocket bluetoothSocket;
    BluetoothDevice bluetoothDevice;

    java.io.OutputStream outputStream;
    InputStream inputStream;
    Thread thread;

    byte[] readBuffer;
    int readBufferPosition;
    volatile boolean stopWorker;

    ListView listView;
    AlertDialog.Builder dialog;
    List<DataHelper> itemList = new ArrayList<DataHelper>();
    AdapterHelper adapter;
    List<DataLogin> itemlogin = new ArrayList<DataLogin>();
    AdapterLogin adapterLogin;
    AndroidDatabase SQLite = new AndroidDatabase(this);

    public static final String TAG_ID = "id";
    public static final String TAG_NAME = "name";
    public static final String TAG_PRICE = "unit_price";
    public static final String TAG_QTY = "quantity";
    public static final String TAG_PAYMENT = "payment";
    public static final String TAG_TOTAL = "total";
    public static final String TAG_ASONG = "pengasong";
    public static final String TAG_OUTLET = "outlet";

    SharedData sharedData = SharedData.getInstance();
    private String Awal;

    String cash = "0";
    String card = "0";
    String voucher = "0";

    @Override
    protected void onCreate(Bundle savedInstanceSaved) {
        super.onCreate(savedInstanceSaved);
        setContentView(R.layout.layout_closing_main);

        SQLite = new AndroidDatabase(getApplicationContext());

        listView = (ListView) findViewById(R.id.list_view);

        adapter = new AdapterHelper(PrintzReport.this, itemList);
        listView.setAdapter(adapter);

        //saleId = Integer.parseInt(getIntent().getStringExtra("id"));
        Awal = String.valueOf(getIntent().getStringExtra("Awal"));

        //cetakClosing();
        printClosing();
        logoutUser();
        //getAllData();
    }
    private void logoutUser() {
        //session.setLogin(false);
        //dbf.deleteUsers();
        Intent intent = new Intent(PrintzReport.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void printClosing() {
        ArrayList<HashMap<String, String>> rows = SQLite.getLogin();
        String o = rows.get(0).get(TAG_OUTLET);
        try {
            FindBluetoothDevice();
            openBluetoothPrinter();
            openCashDrawer();

            String msg = "www.jasabogaraya.com";
            msg+="\n \n";
            msg+="CLOSING SALES";
            msg+="\n";
            msg+= DateTimeStrategy.getCurrentTime().toString();
            msg+="\n";
            msg+="Outlet \u0009 : " + o.toString();
            msg+="\n";
            msg+="--------------------------------";
            //ArrayList<HashMap<String, String>> row = SQLite.getAllData();
            ArrayList<HashMap<String, String>> row = SQLite.ClosingByTgl(Awal);
            int s_total = 0;
            int s_qty = 0;
            for (int i = 0; i < row.size(); i++) {
                String id = row.get(i).get(TAG_ID);
                String name = row.get(i).get(TAG_NAME);
                String price = row.get(i).get(TAG_PRICE);
                String qty = row.get(i).get(TAG_QTY);
                s_qty = s_qty + Integer.parseInt(qty.toString());

                Integer st = Integer.parseInt(qty.toString()) * Integer.parseInt(price.toString());
                s_total = s_total + st;

                msg+="\n" + name.toString() + "\n";
                msg+="x" + qty.toString() + "   " + price.toString() + "\u0009" + st.toString();
                msg+="\n \n";
            }
            msg+= "\n";
            msg+= "--------------------------------";
            Double Ppn = Math.floor(s_total * 0.0909);
            Double Ns = Math.floor(s_total / 1.1);
            String Netsales = String.valueOf(Ns);
            String xNs = Netsales.replace(".0","");
            Double Pb = Math.floor(Ns * 0.1);
            String Pb1 = String.valueOf(Pb);
            String xPb1 = Pb1.replace(".0", "");
            msg+="\n";
            msg+="Total Item \u0009 : " + String.valueOf(s_qty);
            msg+="\n";
            msg+="Total Sales \u0009 : Rp." + String.valueOf(s_total);
            msg+="\n";
            msg+="Sales Net \u0009 : Rp." + xNs;
            msg+="\n";
            msg+="Pb1 \u0009 \u0009 : Rp." + xPb1;
            msg+="\n";
            msg+= "================================";
            msg+="\n";
            msg+="Summary Pengasong";
            msg+="\n";
            msg+= "================================";
            msg+="\n";
            ArrayList<HashMap<String, String>> asong = SQLite.getPengasong();
            double nTotal = 0;
            double nKomisi = 0;
            for (int p = 0; p < asong.size(); p++ ) {
                String asongan = asong.get(p).get(TAG_ASONG);
                String aTotal = asong.get(p).get(TAG_TOTAL);
                //dibagi 1.1
                Double pTotal = Math.floor(Integer.parseInt(asong.get(p).get(TAG_TOTAL)) / 1.1);
                nTotal = nTotal + pTotal;
                Double aHasil = Math.floor((Integer.parseInt(asong.get(p).get(TAG_TOTAL)) / 1.1) * 0.1);
                nKomisi = nKomisi + aHasil;
                String rHasil = aHasil.toString().replace(".0","");
                String rTotal = pTotal.toString().replace(".0","");
                msg+="Sales " + asongan.toString() + "\u0009 : Rp." + rTotal.toString();
                msg+="\n";
                msg+="Komisi " + asongan.toString() + "\u0009 : Rp." + rHasil.toString();
                msg+="\n \n";
            }
            double SlsJbr = Ns - nTotal;
            double TotCsh = Double.parseDouble(String.valueOf(s_total)) - nKomisi;
            String rKomisi = String.valueOf(nKomisi).replace(".0", "");
            Double JLA = Math.floor((TotCsh) * 0.1);
            Double JBR = Math.floor((TotCsh) * 0.9);
            Double Seh = Math.floor(TotCsh);
            String rJLA = JLA.toString().replace(".0","");
            String rJBR = JBR.toString().replace(".0","");
            String rSeh = Seh.toString().replace(".0","");
            String rSls = String.valueOf(SlsJbr).replace(".0","");
            msg+="Sales JBR \u0009 : Rp." + rSls.toString();
            msg+="\n \n";
            msg+="Total Komisi \u0009 : Rp." + rKomisi.toString();
            msg+="\n \n";
            msg+= "--------------------------------";
            msg+="\n";
            msg+="Summary Payment";
            msg+="\n";
            msg+= "--------------------------------";
            msg+="\n";
            ArrayList<HashMap<String, String>> crow = SQLite.getPaymentCash();
            for (int n = 0; n < crow.size(); n++) {
                String payment = crow.get(n).get(TAG_PAYMENT);
                String Total = crow.get(n).get(TAG_TOTAL);
                msg+=payment.toString() + " : Rp." + Total.toString();
                msg+="\n";
            }
            msg+="\n";
            msg+= "================================";
            msg+="\n";
            msg+="REPORT PEMBAGIAN";
            msg+="\n";
            msg+= "================================";
            msg+="\n";
            msg+="JLA (A * 10%) \u0009 = Rp." + rJLA.toString();
            msg+="\n";
            msg+="JBR (A * 90%) \u0009 = Rp." + rJBR.toString();
            msg+="\n";
            msg+="(B + C + D) \u0009 = Rp." + rSeh.toString();
            msg+="\n";
            msg+="Pembayaran EDC \u0009 = Rp." + card;
            msg+="\n";
            msg+="Total Cash \u0009 = Rp." + rSeh.toString();
            msg+="\n";
            msg+="\n";
            msg+= "--------------------------------";
            msg+="JBR-POS ver 2.7";
            msg+="\n \n \n \n";
            outputStream.write(msg.getBytes());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    private void cetakClosing() {
        //String o = sharedData.getOutlet();
        ArrayList<HashMap<String, String>> rows = SQLite.getLogin();
        String o = rows.get(0).get(TAG_OUTLET);
        try {
            FindBluetoothDevice();
            openBluetoothPrinter();
            openCashDrawer();

            String msg = "www.jasabogaraya.com";
            msg+="\n \n";
            msg+="CLOSING SALES";
            msg+="\n";
            msg+=DateTimeStrategy.getCurrentTime().toString();
            msg+="\n";
            msg+="Outlet \u0009 : " + o.toString();
            msg+="\n";
            msg+="--------------------------------";
            //ArrayList<HashMap<String, String>> row = SQLite.getAllData();
            ArrayList<HashMap<String, String>> row = SQLite.getClosing();
            int s_total = 0;
            int s_qty = 0;
            for (int i = 0; i < row.size(); i++) {
                String id = row.get(i).get(TAG_ID);
                String name = row.get(i).get(TAG_NAME);
                String price = row.get(i).get(TAG_PRICE);
                String qty = row.get(i).get(TAG_QTY);
                s_qty = s_qty + Integer.parseInt(qty.toString());

                Integer st = Integer.parseInt(qty.toString()) * Integer.parseInt(price.toString());
                s_total = s_total + st;

                msg+="\n" + name.toString() + "\n";
                msg+="x" + qty.toString() + "   " + price.toString() + "\u0009" + st.toString();
                msg+="\n \n";
            }
            msg+= "\n";
            msg+= "--------------------------------";
            Double Ppn = Math.floor(s_total * 0.0909);
            String Netsales = String.valueOf(s_total - Ppn);
            String xNs = Netsales.replace(".0","");
            String Pb1 = String.valueOf(Ppn);
            String xPb1 = Pb1.replace(".0", "");
            Double JLA = Math.floor((s_total - Ppn) * 0.1);
            Double JBR = Math.floor((s_total - Ppn) * 0.9);
            Double Seh = Math.floor(s_total);
            String rJLA = JLA.toString().replace(".0","");
            String rJBR = JBR.toString().replace(".0","");
            String rSeh = Seh.toString().replace(".0","");
            msg+="\n \n";
            msg+="Total Item : " + String.valueOf(s_qty);
            msg+="\n";
            //msg+="Net Sales : " + String.valueOf(s_total - Ppn);
            msg+="Net Sales : Rp." + xNs;
            msg+="\n";
            msg+="Pb1 : Rp." + xPb1;
            msg+="\n";
            msg+="Total Sales : Rp." + String.valueOf(s_total);
            msg+="\n \n";
            msg+= "--------------------------------";
            msg+="\n";
            msg+="Summary Payment";
            msg+="\n";
            msg+= "--------------------------------";
            msg+="\n";
            ArrayList<HashMap<String, String>> crow = SQLite.getPaymentCash();
            for (int n = 0; n < crow.size(); n++) {
                String payment = crow.get(n).get(TAG_PAYMENT);
                String Total = crow.get(n).get(TAG_TOTAL);
                msg+=payment.toString() + " : Rp." + Total.toString();
                msg+="\n";
            }

            msg+="\n";
            msg+= "================================";
            msg+="\n";
            msg+="REPORT PEMBAGIAN";
            msg+="\n";
            msg+= "================================";
            msg+="\n";
            msg+="JLA (A * 10%) = Rp." + rJLA.toString();
            msg+="\n";
            msg+="JBR (A * 90%) = Rp." + rJBR.toString();
            msg+="\n";
            msg+="(B + C + D) = Rp." + rSeh.toString();
            msg+="\n";
            msg+="Pembayaran EDC = Rp." + card;
            msg+="\n";
            msg+="Total JBR = Rp." + rSeh.toString();
            msg+="\n";
            msg+="\n";
            msg+= "================================";
            msg+="\n";
            msg+="Summary Pengasong";
            msg+="\n";
            msg+= "================================";
            msg+="\n";
            ArrayList<HashMap<String, String>> asong = SQLite.getPengasong();
            for (int p = 0; p < asong.size(); p++ ) {
                String asongan = asong.get(p).get(TAG_ASONG);
                String aTotal = asong.get(p).get(TAG_TOTAL);
                Double aPb1 = Math.floor(Integer.parseInt(asong.get(p).get(TAG_TOTAL)) / 1.1);
                Double aHasil = Math.floor(aPb1 * 0.1);
                String rPb1 = aPb1.toString().replace(".0","");
                String rHasil = aHasil.toString().replace(".0","");
                //msg+=asongan.toString() + " : Rp." + aTotal.toString();
                //msg+="\n";
                msg+=asongan.toString();
                msg+="\n";
                msg+= "------------";
                msg+="\n";
                msg+="Total Sales : Rp." + aTotal.toString();
                msg+="\n";
                msg+="Pb1         : Rp." + rPb1.toString();
                msg+="\n";
                msg+="Hasil (10%) : Rp." + rHasil.toString();
                msg+="\n \n";
            }
            msg+= "--------------------------------";
            msg+="\n \n \n \n";
            outputStream.write(msg.getBytes());
            //adapter.notifyDataSetChanged();
            //InventoryFragment.this.update();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    void FindBluetoothDevice(){
        try{
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if(bluetoothAdapter==null){

            }
            if(bluetoothAdapter.isEnabled()){
                android.content.Intent enableBT = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBT, 0);
            }

            Set<BluetoothDevice> pairedDevice = bluetoothAdapter.getBondedDevices();
            if(pairedDevice.size()>0){
                for(BluetoothDevice pairedDev:pairedDevice){
                    if(pairedDev.getName().equals("58Printer")){
                        //if(pairedDev.getName().equals("BlueTooth Printer")) {
                        bluetoothDevice=pairedDev;
                        break;
                    }
                }
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    /* Open Bluetooth Printer */
    void openBluetoothPrinter() throws IOException {
        try{
            UUID uuidSting = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
            bluetoothSocket=bluetoothDevice.createRfcommSocketToServiceRecord(uuidSting);
            bluetoothSocket.connect();
            outputStream=bluetoothSocket.getOutputStream();
            inputStream=bluetoothSocket.getInputStream();

            beginListenData();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    /*Modul Printer Data*/
    void beginListenData(){
        try{
            final Handler handler=new Handler();
            final byte delimeter=10;
            stopWorker = false;
            readBufferPosition = 0;
            readBuffer = new byte[1024];

            thread=new Thread(new Runnable() {
                @Override
                public void run() {
                    while (!Thread.currentThread().isInterrupted() && !stopWorker){
                        try{
                            int byteAvailable = inputStream.available();
                            if(byteAvailable>0){
                                byte[] packetByte = new byte[byteAvailable];
                                inputStream.read(packetByte);

                                for(int i=0; i<byteAvailable; i++){
                                    byte b = packetByte[i];
                                    if(b==delimeter) {
                                        byte[] encodedByte = new byte[readBufferPosition];
                                        System.arraycopy(
                                                readBuffer, 0,
                                                encodedByte, 0,
                                                encodedByte.length
                                        );
                                        final String data = new String(encodedByte, "US-ASCII");
                                        readBufferPosition = 0;
                                        handler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                            }
                                        });
                                    }else{
                                        readBuffer[readBufferPosition++]=b;
                                    }
                                }
                            }
                        }catch(Exception ex){
                            stopWorker=true;
                            ex.printStackTrace();
                        }
                    }
                }
            });

            thread.start();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private void getAllData() {
        //ArrayList<HashMap<String, String>> row = SQLite.getAllData();
        ArrayList<HashMap<String, String>> row = SQLite.getClosing();
        for (int i = 0; i < row.size(); i++) {
            String id = row.get(i).get(TAG_ID);
            String name = row.get(i).get(TAG_NAME);
            String price = row.get(i).get(TAG_PRICE);
            String qty = row.get(i).get(TAG_QTY);

            DataHelper data = new DataHelper();

            data.setId(id);
            data.setName(name);
            data.setPrice(price);
            data.setQty(qty);

            itemList.add(data);
        }
        adapter.notifyDataSetChanged();
    }

    public boolean openCashDrawer() {
        try {
            byte[] bytes = intArrayToByteArray(new int[]{27, 112, 0, 50, 250});
            outputStream.write(bytes);
            return true;
        } catch (IOException e) {
            //Log.e(TAG, "Open drawer error", e);
            return false;
        }
    }

    private byte[] intArrayToByteArray(int[] Iarr) {
        byte[] bytes = new byte[Iarr.length];
        for (int i = 0; i < Iarr.length; i++) {
            bytes[i] = (byte) (Iarr[i] & 0xFF);
        }
        return bytes;
    }

    @Override
    protected void onResume() {
        super.onResume();
        itemList.clear();
        getAllData();
    }
}
