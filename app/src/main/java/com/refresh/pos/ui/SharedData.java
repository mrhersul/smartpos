package com.refresh.pos.ui;

import android.provider.Settings;

public class SharedData {
    private static SharedData instance = new SharedData();

    public static SharedData getInstance() {
        return instance;
    }

    public static void setInstance(SharedData instance) {
        SharedData.instance = instance;
    }

    private Integer outlet_id;
    private String outlet;
    private Integer cashier_id;
    private String cashier;
    private Integer id;
    private String nost;
    private String payment;
    private Double discount;
    private String part1;
    private String part2;
    private String pengasong;
    private String tunai;
    private String voucher;
    private String kartu;
    private String cashless;
    private String foc;
    private String noref;
    private String lokal;
    private String server;

    private SharedData(){

    }

    public Integer getId() { return id; }
    public void setId(Integer id) { this.id = id; }

    public Integer getOutlet_id() {
        return outlet_id;
    }
    public void setOutlet_id(Integer outlet_id) {
        this.outlet_id = outlet_id;
    }

    public String getCashier() {
        return cashier;
    }
    public void setCashier(String cashier) {
        this.cashier = cashier;
    }

    public Integer getCashier_id() { return cashier_id; }
    public void setCashier_id(Integer cashier_id) {
        this.cashier_id = cashier_id;
    }

    public String getOutlet() {
        return outlet;
    }
    public void setOutlet(String outlet) {
        this.outlet = outlet;
    }

    public String getNost() { return nost; }
    public void setNost(String nost) {
        this.nost = nost;
    }

    public String getPayment() {
        return payment;
    }
    public void setPayment(String payment) {
        this.payment = payment;
    }

    public Double getDiscount() { return discount; }
    public void setDiscount(Double discount) { this.discount = discount; }

    public String getPart1() { return part1; }
    public void setPart1(String part1) { this.part1 = part1; }

    public String getPart2() { return part2; }
    public void setPart2(String part2) { this.part2 = part2; }

    public String getPengasong() { return pengasong; }
    public void setPengasong(String pengasong) { this.pengasong = pengasong; }

    public String getTunai() { return tunai; }
    public void setTunai(String tunai) { this.tunai = tunai; }

    public String getVoucher() { return voucher; }
    public void setVoucher(String voucher) { this.voucher = voucher; }

    public String getKartu() { return kartu; }
    public void setKartu(String kartu) { this.kartu = kartu; }

    public String getCashless() { return cashless; }
    public void setCashless(String cashless) { this.cashless = cashless; }

    public String getFoc() { return foc; }
    public void setFoc(String foc) { this.foc = foc; }

    public String getNoref() { return noref; }
    public void setNoref(String noref) { this.noref = noref; }

    public String getLokal() { return lokal; }
    public void setLokal(String lokal) { this.lokal = lokal; }

    public String getServer() { return server; }
    public void setServer(String server) { this.server = server; }
}
