package com.refresh.pos.ui.login;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.refresh.pos.R;

import java.util.List;

public class AdapterCounter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<DataCounter> item;

    public AdapterCounter(Activity activity, List<DataCounter> item) {
        this.activity = activity;
        this.item = item;
    }

    @Override
    public int getCount() { return item.size(); }

    @Override
    public Object getItem(int location) { return  item.get(location); }

    @Override
    public long getItemId(int position) { return position; }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_counter, null);

        TextView total = (TextView) convertView.findViewById(R.id.total);

        DataCounter dataCounter = item.get(position);

        total.setText(dataCounter.getTotal());

        return convertView;
    }
}
