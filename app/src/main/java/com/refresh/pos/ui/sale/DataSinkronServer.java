package com.refresh.pos.ui.sale;

public class DataSinkronServer {
    private String serverSide;

    public DataSinkronServer(String serverSide) {
        this.serverSide = serverSide;
    }
    public String getServerSide() { return serverSide; }

    public void setServerSide(String serverSide) {
        this.serverSide = serverSide;
    }
}
