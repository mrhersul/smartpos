package com.refresh.pos.techicalservices;

public class DataCatalog {
    private String name, barcode;
    private String outlet;
    private String price;

    public DataCatalog() {

    }

    public DataCatalog(String outlet, String name, String barcode, String price){
        this.outlet = outlet;
        this.name = name;
        this.barcode = barcode;
        this.price = price;
    }

    public String getOutlet() { return outlet; }

    public void setOutlet(String outlet) { this.outlet = outlet; }

    public String getName() {
        return name;
    }

    public void setName(String name) { this.name = name; }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) { this.barcode = barcode; }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) { this.price = price; }
}
