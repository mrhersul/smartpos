package com.refresh.pos.techicalservices.sale;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.ContentValues;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.refresh.pos.domain.DateTimeStrategy;
import com.refresh.pos.domain.inventory.LineItem;
import com.refresh.pos.domain.inventory.Product;
import com.refresh.pos.domain.sale.QuickLoadSale;
import com.refresh.pos.domain.sale.Sale;
import com.refresh.pos.techicalservices.Database;
import com.refresh.pos.techicalservices.DatabaseContents;
import com.refresh.pos.ui.SharedData;
import com.refresh.pos.ui.login.AppConfig;
import com.refresh.pos.ui.login.AppController;
import com.refresh.pos.ui.sale.PaymentFragmentDialog;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * DAO used by android for Sale process.
 * 
 * @author Refresh Team
 *
 */
public class SaleDaoAndroid implements SaleDao {

	Database database;
	int success;
	private static final String TAG_SUCCESS = "success";
	public SaleDaoAndroid(Database database) {
		this.database = database;
	}
	SharedData sharedData = SharedData.getInstance();

	@Override
	public Sale initiateSale(final String startTime) {
		ContentValues content = new ContentValues();
        content.put("start_time", startTime.toString());
        content.put("status", "ON PROCESS");
        content.put("payment", "n/a");
        content.put("total", "0.0");
        content.put("orders", "0");
        content.put("end_time", startTime.toString());
        content.put("sync", "T");

		final int id = database.insert(DatabaseContents.TABLE_SALE.toString(), content);
		//Sinkron ke server
		StringRequest strReq = new StringRequest(Request.Method.POST, AppConfig.URL_SALE_DET,
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						try {
							JSONObject jObj = new JSONObject(response);
							success = jObj.getInt(TAG_SUCCESS);

							if (success == 1) {
								//callVolley();
								//Toast.makeText(SaleDaoAndroid.this, "Sinkron Sukses", Toast.LENGTH_SHORT).show();
							} else {
								//Toast.makeText(SaleDaoAndroid.this, "Sinkron Gagal", Toast.LENGTH_SHORT).show();
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				//Toast.makeText(SaleDaoAndroid.this, "Error", Toast.LENGTH_SHORT).show();
			}
		}) {
			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = new HashMap<String, String>();
				params.put("_id", String.valueOf(id));
				params.put("outlet", sharedData.getOutlet());
				params.put("status","ENDED");
				params.put("payment","CASH");
				params.put("total","0.0");
				params.put("start_time", startTime.toString());
				params.put("end_time", startTime.toString());
				params.put("order", "0");
				params.put("cashier", sharedData.getCashier());
				return params;
			}
		};
		AppController.getInstance().addToRequestQueue(strReq);

		return new Sale(id,startTime);
	}

	@Override
	public void endSale(final Sale sale, final String endTime) {
		ContentValues content = new ContentValues();
        content.put("_id", sale.getId());
        //content.put("nost", sale.getnost());
        content.put("status", "ENDED");
        content.put("payment", "n/a");
        content.put("total", sale.getTotal());
        content.put("orders", sale.getOrders());
        content.put("start_time", sale.getStartTime());
        content.put("end_time", endTime);
        content.put("sync", "T");
		database.update(DatabaseContents.TABLE_SALE.toString(), content);
	}

	@Override
	public void updateSale(Integer outlet, String noSt, String pengasong, String tunai, String voucher, String kartu, String cashless, String foc, String noref){
		//database.execute("UPDATE " + DatabaseContents.TABLE_SALE_LINEITEM + " SET payment = '" +
		//		payment + "', pengasong = '" + sharedData.getPengasong() + "', nost = '" + noSt +
		//		"', outlet = " + sharedData.getOutlet() + ", cashier = '" + sharedData.getCashier() + "' WHERE nost = 'BARU' ");

		database.execute("UPDATE " + DatabaseContents.TABLE_SALE_LINEITEM + " SET pengasong = '" +
				pengasong + "', outlet = " + outlet + ", cashier = '" + sharedData.getCashier() + "'," +
				" tunai = " + tunai + ", voucher = " + voucher + "," +
				" kartu = " + kartu + ", cashless = " + cashless + "," +
				" foc = " + foc + ", noref = '" + noref + "' WHERE nost = 'BARU' ");

		//database.execute("UPDATE " + DatabaseContents.TABLE_SALE_LINEITEM + " SET tunai = '" +
		//		tunai + "', voucher = '" + voucher + "', kartu = '" + kartu + "', cashless = '" +
		//		cashless + "', foc = '" + foc + "', noref = '" + noref + "', pengasong = '" +
        //                sharedData.getPengasong() + "', nost = '" + noSt + "' WHERE nost = 'BARU' ");

		database.execute("UPDATE " + DatabaseContents.TABLE_SALE_LINEITEM + " SET nost = '" +
				noSt + "' WHERE nost = 'BARU' ");
	}
	
	@Override
	public int addLineItem(int saleId, LineItem lineItem) {
		ContentValues content = new ContentValues();
        content.put("sale_id", saleId);
		content.put("nost", "BARU");
        content.put("product_id", lineItem.getProduct().getId());
        content.put("quantity", lineItem.getQuantity());
        content.put("unit_price", lineItem.getPriceAtSale());
        content.put("payment"," ");
        content.put("start_time", DateTimeStrategy.getCurrentTime());
        content.put("cashier", sharedData.getCashier());
        content.put("outlet", sharedData.getOutlet_id());
        content.put("sync", "T");
        int id = database.insert(DatabaseContents.TABLE_SALE_LINEITEM.toString(), content);
        return id;
	}

	@Override
	public void updateLineItem(int saleId, LineItem lineItem) {
		ContentValues content = new ContentValues();		
		content.put("_id", lineItem.getId());
		content.put("sale_id", saleId);
		content.put("nost", "BARU");
		content.put("product_id", lineItem.getProduct().getId());
		content.put("quantity", lineItem.getQuantity());
		content.put("unit_price", lineItem.getPriceAtSale());
		content.put("start_time", DateTimeStrategy.getCurrentTime());
		content.put("outlet", sharedData.getOutlet_id());
		content.put("cashier", sharedData.getCashier());
		//content.put("sync", "T");
		//content.put("discount", sharedData.getDiscount());
		database.update(DatabaseContents.TABLE_SALE_LINEITEM.toString(), content);
	}

	@Override
	public List<Sale> getAllSale() {
		return getAllSale(" WHERE status = 'ENDED'");
	}
	
	@Override
	public List<Sale> getAllSaleDuring(Calendar start, Calendar end) {
		String startBound = DateTimeStrategy.getSQLDateFormat(start);
		String endBound = DateTimeStrategy.getSQLDateFormat(end);
		List<Sale> list = getAllSale(" WHERE end_time BETWEEN '" + startBound + " 00:00:00' AND '" + endBound + " 23:59:59' AND status = 'ENDED'"); 
		return list;
	}
	
	/**
	 * This method get all Sale *BUT* no LineItem will be loaded.
	 * @param condition
	 * @return
	 */
	public List<Sale> getAllSale(String condition) {
		String queryString = "SELECT * FROM " + DatabaseContents.TABLE_SALE + condition;
        List<Object> objectList = database.select(queryString);
        List<Sale> list = new ArrayList<Sale>();
        for (Object object: objectList) {
        	ContentValues content = (ContentValues) object;
        	list.add(new QuickLoadSale(
        			content.getAsInteger("_id"),
        			content.getAsString("start_time"),
        			content.getAsString("end_time"),
        			content.getAsString("status"),
        			content.getAsDouble("total"),
        			content.getAsInteger("orders")      
        			)
        	);
        }
        return list;
	}
	
	/**
	 * This load complete data of Sale.
	 * @param id Sale ID.
	 * @return Sale of specific ID.
	 */
	@Override
	public Sale getSaleById(int id) {
		String queryString = "SELECT * FROM " + DatabaseContents.TABLE_SALE + " WHERE _id = " + id;
        List<Object> objectList = database.select(queryString);
        List<Sale> list = new ArrayList<Sale>();
        for (Object object: objectList) {
        	ContentValues content = (ContentValues) object;
        	list.add(new Sale(
        			content.getAsInteger("_id"),
        			content.getAsString("start_time"),
        			content.getAsString("end_time"),
        			content.getAsString("status"),
        			getLineItem(content.getAsInteger("_id")))
        			);
        }
        return list.get(0);
	}

	@Override
	public List<LineItem> getLineItem(int saleId) {
		String queryString = "SELECT * FROM " + DatabaseContents.TABLE_SALE_LINEITEM + " WHERE sale_id = " + saleId;
		List<Object> objectList = database.select(queryString);
		List<LineItem> list = new ArrayList<LineItem>();
		for (Object object: objectList) {
			ContentValues content = (ContentValues) object;
			int productId = content.getAsInteger("product_id");
			String queryString2 = "SELECT * FROM " + DatabaseContents.TABLE_PRODUCT_CATALOG + " WHERE _id = " + productId;
			List<Object> objectList2 = database.select(queryString2);
			
			List<Product> productList = new ArrayList<Product>();
			for (Object object2: objectList2) {
				ContentValues content2 = (ContentValues) object2;
				productList.add(new Product(productId, content2.getAsString("name"), content2.getAsString("barcode"), content2.getAsDouble("unit_price")));
			}
			list.add(new LineItem(content.getAsInteger("_id") , productList.get(0), content.getAsInteger("quantity"), content.getAsDouble("unit_price")));
		}
		return list;
	}

	@Override
	public void clearSaleLedger() {
		database.execute("DELETE FROM " + DatabaseContents.TABLE_SALE);
		database.execute("DELETE FROM " + DatabaseContents.TABLE_SALE_LINEITEM);
	}

	@Override
	public void cancelSale(Sale sale,String endTime) {
		ContentValues content = new ContentValues();
        content.put("_id", sale.getId());
        content.put("status", "CANCELED");
        content.put("payment", "n/a");
        content.put("total", sale.getTotal());
        content.put("orders", sale.getOrders());
        content.put("start_time", sale.getStartTime());
        content.put("end_time", endTime);
		database.update(DatabaseContents.TABLE_SALE.toString(), content);
		database.execute("DELETE FROM " + DatabaseContents.TABLE_SALE_LINEITEM +
		" WHERE nost = 'BARU'");
	}

	@Override
	public void removeLineItem(int id) {
		database.delete(DatabaseContents.TABLE_SALE_LINEITEM.toString(), id);
	}


}
