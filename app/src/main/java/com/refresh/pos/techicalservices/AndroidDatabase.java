package com.refresh.pos.techicalservices;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.refresh.pos.domain.DateTimeStrategy;
import com.refresh.pos.domain.inventory.LineItem;
import com.refresh.pos.domain.inventory.Product;
import com.refresh.pos.ui.SharedData;
import com.refresh.pos.ui.login.SQLiteHandler;

/**
 * Real database connector, provides all CRUD operation.
 * database tables are created here.
 * 
 * @author Refresh Team
 *
 */
public class AndroidDatabase extends SQLiteOpenHelper implements Database {

	private static final int DATABASE_VERSION = 2;
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_QTY = "quantity";
	public static final String COLUMN_PRICE = "unit_price";

	public static final String COLUMN_STATUS = "status";
	public static final String COLUMN_PAYMENT = "payment";
	public static final String COLUMN_TOTAL = "total";
	public static final String COLUMN_START = "start_time";
	public static final String COLUMN_END = "end_time";
	public static final String COLUMN_ORDER = "order";
	public static final String COLUMN_SYNC = "sync";
	public static final String COLUMN_OUTLET = "outlet";
	public static final String COLUMN_CASHIER = "cashier";
	public static final String COLUMN_PENGASONG = "pengasong";

	public static final String COLUMN_SALEID = "sale_id";
	public static final String COLUMN_NOST = "nost";
	public static final String COLUMN_PRODUCTID = "product_id";
	public static final String COLUMN_QUANTITY = "quantity";
	public static final String COLUMN_UNITPRICE = "unit_price";

	public static final String COLUMN_TUNAI = "tunai";
	public static final String COLUMN_KARTU = "kartu";
	public static final String COLUMN_VOUCHER = "voucher";
	public static final String COLUMN_CASHLESS = "cashless";
	public static final String COLUMN_FOC = "foc";
	public static final String COLUMN_NOREF = "noref";
	public static final String COLUMN_LOKAL = "lokal";
	public static final String COLUMN_TLOKAL = "tlokal";

	private static final String KEY_OUTLETID = "outlet_id";
	private static final String KEY_CASHIERID = "cashier_id";
	private static final String KEY_OUTLET = "outlet";
	private static final String KEY_CASHIER = "cashier";

	//Tambah Table Payment di sales detail
    //private static final String ALTER_PAYMENT = "ALTER TABLE "
    //        + DatabaseContents.TABLE_SALE_LINEITEM + " ADD COLUMN payment TEXT(30);";

	SharedData sharedData = SharedData.getInstance();
	/**
	 * Constructs a new AndroidDatabase.
	 * @param context The current stage of the application.
	 */
	public AndroidDatabase(Context context) {
		super(context, DatabaseContents.DATABASE.toString(), null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		onUpgrade(database,0,DATABASE_VERSION);
	}


	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		if(oldVersion < 1){
			db.execSQL("CREATE TABLE " + DatabaseContents.TABLE_PRODUCT_CATALOG + "("

					+ "_id INTEGER PRIMARY KEY,"
					+ "name TEXT(100),"
					+ "barcode TEXT(100),"
					+ "unit_price DOUBLE,"
					+ "status TEXT(10)"

					+ ");");
			Log.d("CREATE DATABASE", "Create " + DatabaseContents.TABLE_PRODUCT_CATALOG + " Successfully.");

			db.execSQL("CREATE TABLE "+ DatabaseContents.TABLE_STOCK + "("

					+ "_id INTEGER PRIMARY KEY,"
					+ "product_id INTEGER,"
					+ "quantity INTEGER,"
					+ "cost DOUBLE,"
					+ "date_added DATETIME"

					+ ");");
			Log.d("CREATE DATABASE", "Create " + DatabaseContents.TABLE_STOCK + " Successfully.");

			db.execSQL("CREATE TABLE "+ DatabaseContents.TABLE_SALE + "("

					+ "_id INTEGER PRIMARY KEY,"
					+ "status TEXT(40),"
					+ "payment TEXT(50),"
					+ "total DOUBLE,"
					+ "start_time DATETIME,"
					+ "end_time DATETIME,"
					+ "orders INTEGER,"
					+ "sync TEXT(10)"

					+ ");");
			Log.d("CREATE DATABASE", "Create " + DatabaseContents.TABLE_SALE + " Successfully.");

			db.execSQL("CREATE TABLE "+ DatabaseContents.TABLE_SALE_LINEITEM + "("

					+ "_id INTEGER PRIMARY KEY,"
					+ "sale_id INTEGER,"
					+ "nost TEXT(20),"
					+ "product_id INTEGER,"
					+ "quantity INTEGER,"
					+ "unit_price DOUBLE,"
					+ "payment TEXT(30),"
					+ "start_time DATETIME,"
					+ "cashier TEXT(50),"
					+ "outlet INTEGER,"
					+ "sync TEXT(10),"
					+ "pengasong TEXT(50)"

					+ ");");
			Log.d("CREATE DATABASE", "Create " + DatabaseContents.TABLE_SALE_LINEITEM + " Successfully.");


			// this _id is product_id but for update method, it is easier to use name _id
			db.execSQL("CREATE TABLE " + DatabaseContents.TABLE_STOCK_SUM + "("

					+ "_id INTEGER PRIMARY KEY,"
					+ "name TEXT(100),"
					+ "unit_price DOUBLE,"
					+ "quantity INTEGER"

					+ ");");
			Log.d("CREATE DATABASE", "Create " + DatabaseContents.TABLE_STOCK_SUM + " Successfully.");

			db.execSQL("CREATE TABLE " + DatabaseContents.LANGUAGE + "("

					+ "_id INTEGER PRIMARY KEY,"
					+ "language TEXT(5)"

					+ ");");
			Log.d("CREATE DATABASE", "Create " + DatabaseContents.LANGUAGE + " Successfully.");

			db.execSQL("CREATE TABLE " + DatabaseContents.TABLE_TIPE_BAYAR + "("
					+ "_id INTEGER PRIMARY KEY,"
					+ "tipe_bayar TEXT(50)"

					+ ");");
			Log.d("CREATE DATABASE", "Create " + DatabaseContents.TABLE_TIPE_BAYAR + " Successfully.");

			db.execSQL("CREATE TABLE " + DatabaseContents.TABLE_CASHIER + "("
					+ "outlet_id INTEGER,"
					+ "cashier_id INTEGER,"
					+ "outlet TEXT(50),"
					+ "cashier TEXT(100)"
					+ ");");
			Log.d("CREATE DATABASE", "Create " + DatabaseContents.TABLE_CASHIER + " Successfully.");

			db.execSQL("CREATE TABLE " + DatabaseContents.TABLE_PENGASONG + "("
					+ "_id INTEGER PRIMARY KEY,"
					+ "nama TEXT(100)"

					+ ");");
			Log.d("CREATE DATABASE", "Create " + DatabaseContents.TABLE_PENGASONG + " Successfully.");

			db.execSQL("CREATE TABLE " + DatabaseContents.TABLE_CRM + "("
					+ "nost TEXT(20),"
					+ "nama TEXT(100),"
					+ "keterangan TEXT(100)"

					+ ");");
			Log.d("CREATE DATABASE", "Create " + DatabaseContents.TABLE_CRM + " Successfully.");

			Log.d("CREATE DATABASE", "Create Database Successfully.");
		}
		if(oldVersion < 2){
			db.execSQL("ALTER TABLE " + DatabaseContents.TABLE_SALE_LINEITEM + " ADD COLUMN tunai DOUBLE");
			db.execSQL("ALTER TABLE " + DatabaseContents.TABLE_SALE_LINEITEM + " ADD COLUMN voucher DOUBLE");
			db.execSQL("ALTER TABLE " + DatabaseContents.TABLE_SALE_LINEITEM + " ADD COLUMN kartu DOUBLE");
			db.execSQL("ALTER TABLE " + DatabaseContents.TABLE_SALE_LINEITEM + " ADD COLUMN cashless DOUBLE");
			db.execSQL("ALTER TABLE " + DatabaseContents.TABLE_SALE_LINEITEM + " ADD COLUMN foc DOUBLE");
			db.execSQL("ALTER TABLE " + DatabaseContents.TABLE_SALE_LINEITEM + " ADD COLUMN noref TEXT(100)");
			Log.d("ALTER DATABASE", "Alter " + DatabaseContents.TABLE_SALE_LINEITEM + " Successfully.");
		}

	}

	@Override
	public List<Object> select(String queryString) {
		try {
			SQLiteDatabase database = this.getWritableDatabase();
			List<Object> list = new ArrayList<Object>();
			Cursor cursor = database.rawQuery(queryString, null);

			if (cursor != null) {
				if (cursor.moveToFirst()) {
					do {
						ContentValues content = new ContentValues();
						String[] columnNames = cursor.getColumnNames();
						for (String columnName : columnNames) {
							content.put(columnName, cursor.getString(cursor
									.getColumnIndex(columnName)));
						}
						list.add(content);
					} while (cursor.moveToNext());
				}
			}
			cursor.close();
			database.close();
			return list;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public int insert(String tableName, Object content) {
		try {
			SQLiteDatabase database = this.getWritableDatabase();
			int id = (int) database.insert(tableName, null,
					(ContentValues) content);
			database.close();
			return id;
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}

	}

	@Override
	public boolean update(String tableName, Object content) {
		try {
			SQLiteDatabase database = this.getWritableDatabase();
			ContentValues cont = (ContentValues) content;
			// this array will always contains only one element. 
			String[] array = new String[]{cont.get("_id")+""};
			database.update(tableName, cont, " _id = ?", array);
			return true;
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

    @Override
    public boolean delete(String tableName, int id) {
            try {
                    SQLiteDatabase database = this.getWritableDatabase();
                    database.delete(tableName, " _id = ?", new String[]{id+""});
                    return true;
                    
            } catch (Exception e) {
                    e.printStackTrace();
                    return false;
            }
    }

	@Override
	public boolean execute(String query) {
		try{
			SQLiteDatabase database = this.getWritableDatabase();
			database.execSQL(query);
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}

	public void insCatalog(String name, String barcode, Double price) {
		try {
			SQLiteDatabase database = this.getWritableDatabase();
			String query = "INSERT INTO " +  DatabaseContents.TABLE_PRODUCT_CATALOG +
					" (id, name, barcode, unit_price) " +
					"VALUES ('" + name + "', '" + barcode + "', " + price + ")";
			Log.e("insert product catalog ", "" + query);
			database.execSQL(query);
			database.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void updateSyncSales(String id) {
		try {
			SQLiteDatabase database = this.getWritableDatabase();
			String query = "UPDATE " + DatabaseContents.TABLE_SALE_LINEITEM +
					" SET sync = 'Y' WHERE _id = " + id + "";
			Log.e("update sales detail ", "" + query);
			database.execSQL(query);
			database.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void insertCashier(Integer outlet_id, Integer cashier_id, String outlet, String cashier) {
		try {
			SQLiteDatabase database = this.getWritableDatabase();
			String query = "INSERT INTO " + DatabaseContents.TABLE_CASHIER +
					" (outlet_id, cashier_id,outlet, cashier) VALUES (" + outlet_id + "," + cashier_id + ",'" + outlet + "','" + cashier + "')";
			Log.e("Insert data cashier ", "" + query);
			database.execSQL(query);
			database.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deleteCashier() {
		try {
			SQLiteDatabase database = this.getWritableDatabase();
			String query = "DELETE FROM " + DatabaseContents.TABLE_CASHIER + " ";
			Log.e("hapus cashier ", "" + query);
			database.execSQL(query);
			database.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void updateSales(String Nost, String ProductID) {
		try {
			SQLiteDatabase database = this.getWritableDatabase();
			String query = "UPDATE " + DatabaseContents.TABLE_SALE_LINEITEM + " SET sync = 'Y' WHERE " +
					" nost = '" + Nost + "' AND product_id = '" + ProductID + "' ";
			Log.e("Update Sales", "" + query);
			database.execSQL(query);
			database.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	public ArrayList<HashMap<String, String>> getLogin() {
		ArrayList<HashMap<String, String>> wordList;
		wordList = new ArrayList<HashMap<String, String>>();
		String selectQuery = "SELECT * FROM " + DatabaseContents.TABLE_CASHIER;
		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor.moveToNext()) {
			do {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put(KEY_OUTLETID, cursor.getString(0));
				map.put(KEY_CASHIERID, cursor.getString(1));
				map.put(KEY_OUTLET, cursor.getString(2));
				map.put(KEY_CASHIER, cursor.getString(3));
				wordList.add(map);
			} while (cursor.moveToNext());
		}
		Log.e("cashier ", "" + wordList);
		database.close();
		return wordList;
	}

	public ArrayList<HashMap<String, String>> getCounter() {
		ArrayList<HashMap<String, String>> wordList;
		wordList = new ArrayList<HashMap<String, String>>();
		String selectQuery = "select count(*) as total from (" +
				"SELECT DISTINCT sale_id FROM " + DatabaseContents.TABLE_SALE_LINEITEM +
				" where date(start_time) = '" + DateTimeStrategy.getCurrentDate() + "' GROUP BY sale_id) as x";
		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor.moveToNext()) {
			do {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put(COLUMN_TOTAL, cursor.getString(0));
				wordList.add(map);
			} while (cursor.moveToNext());
		}
		Log.e("counter ","" + wordList);
		database.close();
		return wordList;
	}

	public ArrayList<HashMap<String, String>> getSalesDetail() {
		ArrayList<HashMap<String, String>> wordList;
		wordList = new ArrayList<HashMap<String, String>>();
		String selectQuery = "SELECT * FROM " + DatabaseContents.TABLE_SALE_LINEITEM +
				" WHERE " + DatabaseContents.TABLE_SALE_LINEITEM + ".nost = '" + sharedData.getNost() + "'";
		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor.moveToNext()) {
			do {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put(COLUMN_ID, cursor.getString(0));
				map.put(COLUMN_SALEID, cursor.getString(1));
				map.put(COLUMN_NOST, cursor.getString(2));
				map.put(COLUMN_PRODUCTID, cursor.getString(3));
				map.put(COLUMN_QUANTITY, cursor.getString(4));
				map.put(COLUMN_UNITPRICE, cursor.getString(5));
				map.put(COLUMN_PAYMENT, cursor.getString(6));
				map.put(COLUMN_START, cursor.getString(7));
				map.put(COLUMN_CASHIER, cursor.getString(8));
				map.put(COLUMN_OUTLET, cursor.getString(9));
				map.put(COLUMN_SYNC, cursor.getString(10));
				map.put(COLUMN_PENGASONG, cursor.getString(11));
				map.put(COLUMN_TUNAI, cursor.getString(12));
				map.put(COLUMN_VOUCHER, cursor.getString(13));
				map.put(COLUMN_KARTU, cursor.getString(14));
				map.put(COLUMN_CASHLESS, cursor.getString(15));
				map.put(COLUMN_FOC, cursor.getString(16));
				map.put(COLUMN_NOREF, cursor.getString(17));
				wordList.add(map);
			} while (cursor.moveToNext());
		}
		Log.e("Detail Sales ", "" + wordList);
		database.close();
		return wordList;
	}

	public ArrayList<HashMap<String, String>> getSumSales() {
		ArrayList<HashMap<String, String>> wordList;
		wordList = new ArrayList<HashMap<String, String>>();
		String selectQuery = "SELECT COUNT(*) AS lokal, ifnull(SUM(quantity * unit_price),0) AS tlokal FROM " + DatabaseContents.TABLE_SALE_LINEITEM +
				" INNER JOIN " + DatabaseContents.TABLE_SALE + " ON " + DatabaseContents.TABLE_SALE_LINEITEM  +
				".sale_id = " + DatabaseContents.TABLE_SALE + "._id " +
				" WHERE " + DatabaseContents.TABLE_SALE + ".status = 'ENDED' AND " +
				" date(" + DatabaseContents.TABLE_SALE_LINEITEM + ".start_time) = '" + DateTimeStrategy.getCurrentDate() + "'";
		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor.moveToNext()) {
			do {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put(COLUMN_LOKAL, cursor.getString(0));
				map.put(COLUMN_TLOKAL, cursor.getString(1));
				wordList.add(map);
			} while (cursor.moveToNext());
		}
		Log.e("Detail Sales ", "" + wordList);
		database.close();
		return wordList;
	}

	public ArrayList<HashMap<String, String>> getAllSalesDetail() {
		ArrayList<HashMap<String, String>> wordList;
		wordList = new ArrayList<HashMap<String, String>>();
		String selectQuery = "SELECT * FROM " + DatabaseContents.TABLE_SALE_LINEITEM +
                " INNER JOIN " + DatabaseContents.TABLE_SALE + " ON " + DatabaseContents.TABLE_SALE_LINEITEM  +
                ".sale_id = " + DatabaseContents.TABLE_SALE + "._id " +
                " WHERE " + DatabaseContents.TABLE_SALE_LINEITEM + ".sync <> 'Y' AND " + DatabaseContents.TABLE_SALE + ".status = 'ENDED' AND " +
                " date(" + DatabaseContents.TABLE_SALE_LINEITEM + ".start_time) = '" + DateTimeStrategy.getCurrentDate() + "'";
		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor.moveToNext()) {
			do {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put(COLUMN_ID, cursor.getString(0));
				map.put(COLUMN_SALEID, cursor.getString(1));
				map.put(COLUMN_NOST, cursor.getString(2));
				map.put(COLUMN_PRODUCTID, cursor.getString(3));
				map.put(COLUMN_QUANTITY, cursor.getString(4));
				map.put(COLUMN_UNITPRICE, cursor.getString(5));
				map.put(COLUMN_PAYMENT, cursor.getString(6));
				map.put(COLUMN_START, cursor.getString(7));
				map.put(COLUMN_CASHIER, cursor.getString(8));
				map.put(COLUMN_OUTLET, cursor.getString(9));
				map.put(COLUMN_SYNC, cursor.getString(10));
				map.put(COLUMN_PENGASONG, cursor.getString(11));
				map.put(COLUMN_TUNAI, cursor.getString(12));
				map.put(COLUMN_VOUCHER, cursor.getString(13));
				map.put(COLUMN_KARTU, cursor.getString(14));
				map.put(COLUMN_CASHLESS, cursor.getString(15));
				map.put(COLUMN_FOC, cursor.getString(16));
				map.put(COLUMN_NOREF, cursor.getString(17));
				wordList.add(map);
			} while (cursor.moveToNext());
		}
		Log.e("Detail Sales ", "" + wordList);
		database.close();
		return wordList;
	}

	public ArrayList<HashMap<String, String>> ClosingByTgl(String Start) {
		ArrayList<HashMap<String, String>> wordList;
		wordList = new ArrayList<HashMap<String, String>>();
		String selectQuery = "SELECT DISTINCT " + DatabaseContents.TABLE_SALE_LINEITEM + ".product_id as id, " +
				DatabaseContents.TABLE_PRODUCT_CATALOG + ".name, " + DatabaseContents.TABLE_SALE_LINEITEM +
				".unit_price, SUM(" + DatabaseContents.TABLE_SALE_LINEITEM + ".quantity) AS quantity  FROM " +
				DatabaseContents.TABLE_SALE_LINEITEM + " INNER JOIN " + DatabaseContents.TABLE_PRODUCT_CATALOG +
				" ON " + DatabaseContents.TABLE_SALE_LINEITEM + ".product_id = " + DatabaseContents.TABLE_PRODUCT_CATALOG +
				"._id INNER JOIN " + DatabaseContents.TABLE_SALE + " ON " + DatabaseContents.TABLE_SALE_LINEITEM +
				".sale_id = " + DatabaseContents.TABLE_SALE + "._id WHERE " + DatabaseContents.TABLE_SALE +
				".status = 'ENDED' AND date(" + DatabaseContents.TABLE_SALE_LINEITEM + ".start_time) = '" + Start + "' " +
				" GROUP BY id, " + DatabaseContents.TABLE_PRODUCT_CATALOG + ".name, " +
				DatabaseContents.TABLE_SALE_LINEITEM + ".unit_price ";
		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor.moveToNext()) {
			do {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put(COLUMN_ID, cursor.getString(0));
				map.put(COLUMN_NAME, cursor.getString(1));
				map.put(COLUMN_UNITPRICE, cursor.getString(2));
				map.put(COLUMN_QUANTITY, cursor.getString(3));
				wordList.add(map);
			} while (cursor.moveToNext());
		}
		Log.e("Closing Sales ", "" + wordList);
		database.close();
		return wordList;
	}

	public ArrayList<HashMap<String, String>> getClosing() {
	    ArrayList<HashMap<String, String>> wordList;
	    wordList = new ArrayList<HashMap<String, String>>();
	    String selectQuery = "SELECT DISTINCT " + DatabaseContents.TABLE_SALE_LINEITEM + ".product_id as id, " +
				DatabaseContents.TABLE_PRODUCT_CATALOG + ".name, " + DatabaseContents.TABLE_SALE_LINEITEM +
				".unit_price, SUM(" + DatabaseContents.TABLE_SALE_LINEITEM + ".quantity) AS quantity  FROM " +
                DatabaseContents.TABLE_SALE_LINEITEM + " INNER JOIN " + DatabaseContents.TABLE_PRODUCT_CATALOG +
                " ON " + DatabaseContents.TABLE_SALE_LINEITEM + ".product_id = " + DatabaseContents.TABLE_PRODUCT_CATALOG +
				"._id INNER JOIN " + DatabaseContents.TABLE_SALE + " ON " + DatabaseContents.TABLE_SALE_LINEITEM +
                ".sale_id = " + DatabaseContents.TABLE_SALE + "._id WHERE " + DatabaseContents.TABLE_SALE +
                ".status = 'ENDED' AND date(" + DatabaseContents.TABLE_SALE_LINEITEM + ".start_time) = '" + DateTimeStrategy.getCurrentDate() +
				"' GROUP BY id, " + DatabaseContents.TABLE_PRODUCT_CATALOG + ".name, " +
				DatabaseContents.TABLE_SALE_LINEITEM + ".unit_price ";
		SQLiteDatabase database = this.getWritableDatabase();
	    Cursor cursor = database.rawQuery(selectQuery, null);
	    if (cursor.moveToNext()) {
	        do {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put(COLUMN_ID, cursor.getString(0));
                map.put(COLUMN_NAME, cursor.getString(1));
                map.put(COLUMN_UNITPRICE, cursor.getString(2));
                map.put(COLUMN_QUANTITY, cursor.getString(3));
                wordList.add(map);
            } while (cursor.moveToNext());
        }
        Log.e("Closing Sales ", "" + wordList);
	    database.close();
	    return wordList;
    }

    public ArrayList<HashMap<String, String>> getPengasong() {
	    ArrayList<HashMap<String, String>> wordList;
	    wordList = new ArrayList<HashMap<String, String>>();
	    String selectQuery = "SELECT DISTINCT pengasong, SUM(quantity * unit_price) AS total FROM "
                + DatabaseContents.TABLE_SALE_LINEITEM + " WHERE date(start_time) = '" +
                DateTimeStrategy.getCurrentDate() + "' AND pengasong <> 'NONE' GROUP BY pengasong";
	    SQLiteDatabase database = this.getReadableDatabase();
	    Cursor cursor = database.rawQuery(selectQuery, null);
	    if (cursor.moveToNext()) {
	        do {
	            HashMap<String, String> map = new HashMap<String, String>();
	            map.put(COLUMN_PENGASONG, cursor.getString(0));
	            map.put(COLUMN_TOTAL, cursor.getString(1));
	            wordList.add(map);
            } while (cursor.moveToNext());
        }
        Log.e("Pengasong ", "" + wordList);
	    database.close();
	    return wordList;
    }

   public ArrayList<HashMap<String, String>> getPaymentCash() {
		ArrayList<HashMap<String, String>> wordList;
		wordList = new ArrayList<HashMap<String, String>>();
		String selectQuery = "SELECT DISTINCT payment, SUM(quantity * unit_price) AS total FROM "
				+ DatabaseContents.TABLE_SALE_LINEITEM + " WHERE date(start_time) = '" +
				DateTimeStrategy.getCurrentDate() + "' GROUP BY payment";
		SQLiteDatabase database = this.getReadableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor.moveToNext()) {
			do {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put(COLUMN_PAYMENT, cursor.getString(0));
				map.put(COLUMN_TOTAL, cursor.getString(1));
				wordList.add(map);
			} while (cursor.moveToNext());
		}
		Log.e("Payment Cash ", "" + wordList);
		database.close();
		return wordList;
   }

   public ArrayList<HashMap<String, String>> getPaymentCard() {
	   ArrayList<HashMap<String, String>> wordList;
	   wordList = new ArrayList<HashMap<String, String>>();
	   String selectQuery = "SELECT SUM(quantity * unit_price) AS total FROM "
			   + DatabaseContents.TABLE_SALE_LINEITEM + " WHERE date(start_time) = '" +
			   DateTimeStrategy.getCurrentDate() + "' AND payment = 'CARD'";
	   SQLiteDatabase database = this.getReadableDatabase();
	   Cursor cursor = database.rawQuery(selectQuery, null);
	   if (cursor.moveToNext()) {
		   do {
			   HashMap<String, String> map = new HashMap<String, String>();
			   map.put(COLUMN_PAYMENT, cursor.getString(0));
			   map.put(COLUMN_TOTAL, cursor.getString(1));
			   wordList.add(map);
		   } while (cursor.moveToNext());
	   }
	   Log.e("Payment Card ", "" + wordList);
	   database.close();
	   return wordList;
   }

   public ArrayList<HashMap<String, String>> getPaymentVoucher() {
	   ArrayList<HashMap<String, String>> wordList;
	   wordList = new ArrayList<HashMap<String, String>>();
	   String selectQuery = "SELECT SUM(quantity * unit_price) AS total FROM "
			   + DatabaseContents.TABLE_SALE_LINEITEM + " WHERE date(start_time) = '" +
			   DateTimeStrategy.getCurrentDate() + "' AND payment = 'VOUCHER'";
	   SQLiteDatabase database = this.getReadableDatabase();
	   Cursor cursor = database.rawQuery(selectQuery, null);
	   if (cursor.moveToNext()) {
		   do {
			   HashMap<String, String> map = new HashMap<String, String>();
			   map.put(COLUMN_PAYMENT, cursor.getString(0));
			   map.put(COLUMN_TOTAL, cursor.getString(1));
			   wordList.add(map);
		   } while (cursor.moveToNext());
	   }
	   Log.e("Payment Voucher ", "" + wordList);
	   database.close();
	   return wordList;
   }

	public ArrayList<HashMap<String, String>> getSemuaSales() {
		ArrayList<HashMap<String, String>> wordList;
		wordList = new ArrayList<HashMap<String, String>>();
		String selectQuery = "SELECT * FROM " + DatabaseContents.TABLE_SALE_LINEITEM +
				" INNER JOIN " + DatabaseContents.TABLE_SALE + " ON " + DatabaseContents.TABLE_SALE_LINEITEM +
				".sale_id = " + DatabaseContents.TABLE_SALE + "._id " +
				" WHERE " + DatabaseContents.TABLE_SALE + ".status = 'ENDED'";
		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor.moveToNext()) {
			do {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put(COLUMN_ID, cursor.getString(0));
				map.put(COLUMN_SALEID, cursor.getString(1));
				map.put(COLUMN_NOST, cursor.getString(2));
				map.put(COLUMN_PRODUCTID, cursor.getString(3));
				map.put(COLUMN_QUANTITY, cursor.getString(4));
				map.put(COLUMN_UNITPRICE, cursor.getString(5));
				map.put(COLUMN_PAYMENT, cursor.getString(6));
				map.put(COLUMN_START, cursor.getString(7));
				map.put(COLUMN_CASHIER, cursor.getString(8));
				map.put(COLUMN_OUTLET, cursor.getString(9));
				map.put(COLUMN_SYNC, cursor.getString(10));
				map.put(COLUMN_PENGASONG, cursor.getString(11));
				wordList.add(map);
			} while (cursor.moveToNext());
		}
		Log.e("Detail Sales ", "" + wordList);
		database.close();
		return wordList;
	}



}
