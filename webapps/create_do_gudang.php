	
    <section class="content-header">
          <h1>
            DELIVERY ORDER GUDANG
            <small>Input DO</small>
          </h1>
          <ol class="breadcrumb">
            <li><?echo"<a href=\"".SITE_URL."/main.php?option=Dashboard\">";?><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Delivery Order Gudang</li>
          </ol>
        </section>

        <!-- Main content -->
    <section class="content">
	<div class="box box-default">
		<div class="box-header with-border">
              <h3 class="box-title">Delivery Order Gudang</h3>   
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>           
        </div><!-- /.box-header -->  
        <script language="JavaScript">
			function kirpesan() {
				if(document.formpesan.kode_pr.value=="") {
					alert("Kolom Kode PR belum diisi");
					return false;
				}
				return true;				
			}
		</script>          
        
		<div class="box-body">
			<form role="form" id="form1"  method="POST" name="form1" autocomplete="off" action="main.php?option=simpan-tr-gudang">
              <div class="row">
                <div class="col-md-6">
                    <div class="form-group">             
                        <label>Kode DO: </label>
                        <?php
                        $bln = date('m');
                        $thn = date('Y');
						$sql = query("SELECT max(kode_do) as maxBar FROM xx_do_header");
						$cek = fetch($sql);
                        //$mnt = month();
						if($cek['maxBar'] == NULL) {
							echo "<input type='text' class='form-control' id='kode_tr' name='kode_tr' placeholder='Kode TR' value='DO-JBR/$bln.$thn/001' readonly='readonly'>";
						} else {
                            $bar = $cek["maxBar"];
							$doc = substr($bar,0,7);
                            $dbul = substr($bar,7,2);
                            $dthn = substr($bar,10,4);
                            $nobar = '001';
                            if ($thn = $dthn and $bln = $dbul){
                                $nobar = (int) substr($bar,15,3); 
                                $nobar++;
                            } 
							$newID = $doc . $bln . '.' . $thn . '/' . sprintf("%03s", $nobar);

                            echo "<input type='text' class='form-control' id='kode_tr' placeholder='Kode TR' value='$newID' readonly='readonly' name='kode_tr'>";
						}
					  ?> 
					</div>  	
                   	<div class="form-group"> 
                        <label>Dari : </label>
                        <select  id="area_a" name="area_a" class="form-control" >
                    		<option value="" disabled>Pilih Area</option>
                    		<?
							$area=query("SELECT DISTINCT areaid,areanm FROM m_area order by areanm");
							while($a=fetch($area)){
							?>
								<option value="<? echo $a['areaid']; ?>"><? echo $a['areanm']; ?></option>
							<?
							}
							?>
						</select>
					</div>
					<div class="form-group">
						<label>Nama Pembuat</label>
						<input type="text" name="pembuat" id="pembuat" placeholder="Nama Pembuat" class="form-control">
					</div>			
				</div><!-- /.col -->
            	<div class="col-md-6">
            	<input type="hidden" id="kode_user" name="kode_user" value="<? echo '.$s[kode_user].'; ?>">
            		<div class="form-group">
                    	<label>Tanggal Order :</label>
                    	<div class="input-group">
                      	<div class="input-group-addon">
                        	<i class="fa fa-calendar"></i>
                      	</div>
                      	<input type="text" class="form-control pull-right" name="start_date" id="start_date" value="<? echo date('m/d/Y');?>"  />
                    	</div><!-- /.input group -->
						</div>
          			<div class="form-group">
                    <label>Tanggal Kirim :</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      	<input type="date" class="form-control pull-right" name="tgl_form" id="tgl_form" value="<? echo date('m/d/Y');?>"  />
                    </div><!-- /.input group -->
					
                  	</div><!-- /.form group -->
           			<div class="form-group"> 
                        <label>Kirim Ke : </label>
                        <select  id="area_z" name="area_z" class="form-control" >
                    		<option value="" disabled>Pilih Area</option>
                    		<?
							$areaz=query("SELECT DISTINCT areaid,areanm FROM m_area order by areanm");
							while($z=fetch($areaz)){
							?>
								<option value="<? echo $z['areaid']; ?>"><? echo $z['areanm']; ?></option>
							<?
							}
							?>
						</select>
					</div>
				  </div>
             <div class="col-md-12">
             		<div class="form-group">
                        <label>Keterangan</label>
                        <input type="text" class="form-control" id="keterangan" name="keterangan" placeholder="Keterangan" value="<? echo $s['keterangan']; ?>" />
                    </div>                    		
			</div>
             <div class="box-footer">
             <div class="col-md-11">
             	<!--<input type="button" class="btn btn-primary " id="addrow" value="Tambah Barang" /> -->
			 </div>
			 <div class="col-md-1">	
				<input type="submit" id="btn_submit" name="btn_submit" value="Simpan" class="btn btn-danger"/>
				 </div>
		</div>
              </div><!-- /.row -->	
              </form>		
        </div><!-- /.box-body -->
		<!-- </form> -->
	</div><!-- /.box --> 
	<div class="box">
                <div class="box-header">
                  <h3 class="box-title">Daftar Transaksi</h3>
                </div><!-- /.box-header -->
                <div class="box-body"> 
                	<table id="example2" class="table table-bordered table-hover dataTable-scroll-x">
                    	<thead>
                      		<tr>
                                <th>Tanggal Buat</th>
                                <th>Tanggal Kirim</th>
                        		<th>Kode DO</th>
                                <th>Dari </th>
                                <th>Kirim Ke</th>
                                <th>Nama Pembuat</th>
								<th>Keterangan</th>
		                        <th>Action</th>                     
                	        </tr>
                      	</thead>
                    	<tbody>
                  <?php
				  $query = mysql_query("select * from xx_do_header WHERE status_terima IS NULL AND kode_sr IS NULL");
				  $count = mysql_num_rows($query);
				  if($count > 0){
				  	while($r=fetch($query)) {
						echo "<tr>";
						$oa = fetch(query("SELECT areanm FROM m_area WHERE areaid = '".$r[kode_area_a]."'"));
						$az = fetch(query("SELECT areanm FROM m_area WHERE areaid = '".$r[kode_area_z]."'"));
						//cari barang
						$tgl=date('j M Y H:i',strtotime($r['tgl_buat']));
						$tkr=date('j M Y',strtotime($r['tgl_kirim']));
						echo "<td>".$tgl."</td>";
						echo "<td>".$tkr."</td>";
                        echo "<td>".$r['kode_do']."</td>";
						echo "<td>".$oa['areanm']."</td>";
						echo "<td>".$az['areanm']."</td>";
						echo "<td>".$r['nama_pembuat']."</td>";
						echo "<td>".$r['keterangan']."</td>";
						echo "<td><a href='main.php?option=add-tr-detail&id=".$r['kode_do']."' class='edit'>+ Item</a> || <a href='main.php?option=hapus-tr&id=".$r['kode_do']."' class='hapus'>Hapus</a></td>";
                        
					}
				  }			  
				  ?>

				  	</tbody>
                  </table>
                </div>
              </div><!-- /.box -->
        
	</section>