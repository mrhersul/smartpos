	
    <section class="content-header">
          <h1>
            INPUT PURCHASE REQUEST
            <small>Input Purchase Request</small>
          </h1>
          <ol class="breadcrumb">
            <li><?echo"<a href=\"".SITE_URL."/main.php?option=Dashboard\">";?><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Input Purchase Request</li>
          </ol>
        </section>

        <!-- Main content -->
    <section class="content">
	<div class="box box-default">
		<div class="box-header with-border">
              <h3 class="box-title">Input Purchase Request</h3>   
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>           
        </div><!-- /.box-header -->  
        <script language="JavaScript">
			function kirpesan() {
				if(document.formpesan.kode_pr.value=="") {
					alert("Kolom Kode PR belum diisi");
					return false;
				}
				return true;				
			}
		</script>          
        
		<div class="box-body">
			<form role="form" id="form1"  method="POST" name="form1" autocomplete="off" action="main.php?option=simpan-pr-gudang">
              <div class="row">
                <div class="col-md-6">
                    <div class="form-group">             
                        <label>Kode PR: </label>
                        <?php
                        $bln = date('m');
                        $thn = date('Y');
						$sql = query("SELECT max(kode_pr) as maxBar FROM xx_pr_header where kode_pr like '%/$bln.$thn/%'");
						$cek = fetch($sql);
                        //$mnt = month();
						if($cek['maxBar'] == NULL) {
							echo "<input type='text' class='form-control' id='kode_pr' name='kode_pr' placeholder='Kode PR' value='PR-JBR/$bln.$thn/001' readonly='readonly'>";
						} else {
                            $bar = $cek["maxBar"];
							$doc = substr($bar,0,7);
                            $dbul = substr($bar,7,2);
                            $dthn = substr($bar,10,4);
                            $nobar = '001';
                            if ($thn = $dthn and $bln = $dbul){
                                $nobar = (int) substr($bar,15,3); 
                                $nobar++;
                            } 
							$newID = $doc . $bln . '.' . $thn . '/' . sprintf("%03s", $nobar);

                            echo "<input type='text' class='form-control' id='kode_pr' placeholder='Kode PR' value='$newID' readonly='readonly' name='kode_pr'>";
						}
					  ?> 
					</div>  	
                   	<div class="form-group"> 
                        <label>Lokasi : </label>
                        <select  id="area" name="area" class="form-control selectpicker" data-live-search="true" >
                    		<option value="" disabled>Pilih Area</option>
                    		<?
							$area=query("SELECT DISTINCT areaid,areanm FROM m_area order by areaid");
							while($a=fetch($area)){
							?>
								<option value="<? echo $a['areaid']; ?>"><? echo $a['areanm']; ?></option>
							<?
							}
							?>
						</select>
					</div>			
				</div><!-- /.col -->
            <div class="col-md-6">
                    	<input type="hidden" id="kode_user" name="kode_user" value="<? echo '.$s[kode_user].'; ?>">
                       	<div class="form-group">
                    	<label>Tanggal Kirim :</label>
                    	<div class="input-group">
                      	<div class="input-group-addon">
                        	<i class="fa fa-calendar"></i>
                      	</div>
                      	<?

                      if (isset($_POST['start_date'])) {

					  ?>

                      	<input type="text" class="form-control pull-right" name="start_date" id="start_date" value="<? echo date('d/m/Y',strtotime($_POST['start_date']));?>"  >

                      <? } else { ?>

                      	<input type="text" class="form-control pull-right" name="start_date" id="start_date" value="<? echo date('m/d/Y');?>"  />

                      <? } ?>
                    	</div><!-- /.input group -->
						</div>
                      <div class="form-group"> 
                        <label>Nama Pembuat : </label>
                        <input type="text" id="pembuat" name="pembuat" placeholder="Nama Pembuat" class="form-control">
					</div>
                       			
				</div><!-- /.col -->
             <div class="col-md-12">
             		<div class="form-group">
                        <label>Keterangan</label>
                        <input type="text" class="form-control" id="keterangan" name="keterangan" placeholder="Keterangan" value="<? echo $s['keterangan']; ?>" />
                    </div>                    		
			</div>
             <div class="box-footer">
             <div class="col-md-11">
             	<!--<input type="button" class="btn btn-primary " id="addrow" value="Tambah Barang" /> -->
			 </div>
			 <div class="col-md-1">	
				<input type="submit" id="btn_submit" name="btn_submit" value="Simpan" class="btn btn-danger"/>
				 </div>
		</div>
              </div><!-- /.row -->	
              </form>		
        </div><!-- /.box-body -->
		<!-- </form> -->
	</div><!-- /.box --> 
	<div class="box">
                <div class="box-header">
                  <h3 class="box-title">Daftar Item</h3>
                </div><!-- /.box-header -->
                <div class="box-body"> 
                	<table id="example2" class="table table-bordered table-hover dataTable-scroll-x">
                    	<thead>
                      		<tr>
                                <th>Tanggal Buat</th>
                                <th>Tanggal Kirim</th>
                        		<th>Kode PR</th>
                                <th>Lokasi</th>
                                <th>Keterangan</th>
		                        <th>Action</th>                     
                	        </tr>
                      	</thead>
                    	<tbody>
                  <?php
				  $query = mysql_query("select * from xx_pr_header WHERE kode_sr IS NULL AND status <> '3' order by kode_pr desc");
				  $count = mysql_num_rows($query);
				  if($count > 0){
				  	while($r=fetch($query)) {
						echo "<tr>";
						//cari barang
						$tgl=date('j M Y H:i',strtotime($r['tgl_pr']));
						$kir=date('j M Y', strtotime($r['tgl_kirim']));
						//lokasi
						$lk=fetch(query("SELECT areaid,areanm FROM m_area WHERE areaid = '".$r['kode_area']."'"));
						echo "<td>".$tgl."</td>";
						echo "<td>".$kir."</td>";
                        echo "<td>".$r['kode_pr']."</td>";
						echo "<td>".$lk['areanm']."</td>";
						echo "<td>".$r['keterangan']."</td>";
						echo "<td><a href='main.php?option=create-pr-detail&id=".$r['kode_pr']."' class='edit'>+ Item</a> || <a href='main.php?option=edit-pr&id=".$r['kode_pr']."' class='edit'>Edit</a> || <a href='main.php?option=hapus-pr&id=".$r['kode_pr']."' class='hapus'>Hapus</a></td>";
                        
					}
				  }			  
				  ?>

				  	</tbody>
                  </table>
                </div>
              </div><!-- /.box -->
        
	</section>