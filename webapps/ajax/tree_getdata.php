<?php
	//require('../config/config.php');
?>
	
	<div class="box">
		<div class="row">
            <div class="col-xs-12">
                <div class="box-header">
                  <h3 class="box-title">DPR</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table title="Product" class="table table-bordered table-striped tabel" url="treegrid3_getdata.php"
                  rownumbers="true" idField="id" treeField="name">
                    <thead>
                      <tr>
                        <th field="name" width="250">Name</th>
                        <th field="amount" width="150" align="right" formatter="formatDollar">Amount</th>    
                      </tr>
                    </thead>
                  </table>
				<script>
					function formatDollar(value){
						if (value){
							return 'Rp. '+value;
						} else {
							return '';
						}
					}
				</script>
					
                </div><!-- /.box-body --> 				
			</div><!-- /.col -->
          </div><!-- /.row -->
	</div><!-- /.box -->

