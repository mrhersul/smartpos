<?
	require('../config/config.php');
	require('../config/function.php');
	//require('../header.php');
	$limit = 10;
	$adjacent = 3;
	$page =$_GET['page'];
	if($page==1){
		$start = 0;  
	}
	else{
		$start = ($page-1)*$limit;
	}
	$start_date= date('Ymd',strtotime($_GET['datefrm']));
	$end_date=date('Ymd',strtotime($_GET['dateto']));
	$jenis = intval($_GET['jns']);
	$club = $_GET['club'];
	//$sql_tot=query("EXEC SP_DPR_FOR_WEB '$start_date','$end_date','$jenis','$club'");
	//echo num($sql_tot);
	//$tot = fetch($sql_tot);
?>	
	<div class="box">
		<div class="row">
            <div class="col-xs-12">
                <div class="box-header">
                  <h3 class="box-title">Detail Transactions</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
				<?
				if ($start_date > $end_date){?>
					<div class="error-content">
					<h3 align="center"><i class="fa fa-warning text-yellow"></i> Oops! Daterange period invalid.</h3>					              
					</div><!-- /.error-content -->
				<?}else{?>
                  <table class="table table-bordered table-striped tabel">
                    <thead>
                      <tr>
                        <th>Club</th>
                        <th>Date</th>
                        <th>Descriptions</th>
                        <th>Amount</th>                        
                      </tr>
                    </thead>
					<?									
					$sql=query("EXEC SP_DPR_FOR_WEB '$start_date','$end_date','0','0','$jenis','$club'");
					$rows=num($sql);
					$total=0;
					while($tot=fetch($sql)){
						$total=$total+$tot['AMOUNT'];
					}
					$sql=query("EXEC SP_DPR_FOR_WEB '$start_date','$end_date','$start','$limit','$jenis','$club'");
					?>
					<tbody>
					<?
					
					if(num($sql)>0){
						
						while($dpr=fetch($sql)){
							$tgl=date('j M Y',strtotime($dpr['DATE']));
							$amount=convert_to_rupiah($dpr['AMOUNT']);
							echo "<tr>";
							echo "<td>".$dpr['CLUB']."</td>";
							echo "<td>".$tgl."</td>";
							echo "<td>".$dpr['PROD_SOLD']."</td>";
							echo "<td>".$amount."</td>";
							echo "</tr>";
							//$total=$total+$dpr['AMOUNT'];
							
						}
						
					}else{
						echo "<tr>";
						echo "<td colspan=\"4\" align=\"center\">Data Tidak ditemukan.</td>";
						echo "</tr>";
					}
					
					?>                                   
                    </tbody>                    
                  </table>
                </div><!-- /.box-body --> 
				<div class="box-footer">					
					<?pagination($limit,$adjacent,$rows,$page);?>
					<h2>Total Amount : <?echo convert_to_rupiah($total);
					?></h2>
                </div>				
			</div><!-- /.col -->
          </div><!-- /.row -->
	</div><!-- /.box -->
	<?}
mssql_close($link);	
?>