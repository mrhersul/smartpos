	<?php
	// Handle AJAX request (start)
	if( isset($_POST['btn_submit']) && isset($_POST['name']) ){
		echo $_POST['name'];
 		exit;
	}
	if( isset($_POST['barcode'])) {
		echo $_POST['barcode'];
		exit;
	}
	// Handle AJAX request (end)
	?>
    <section class="content-header">
          <h1>
            APPROVE PAYMENT 
            <small>EVENT</small>
          </h1>
          <ol class="breadcrumb">
            <li><?echo"<a href=\"".SITE_URL."/main.php?option=Dashboard\">";?><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Approve Payment</li>
          </ol>
        </section>

        <!-- Main content -->
    <section class="content">
	<div class="box box-default">      
		
	<div class="box">
                <div class="box-header">
                  <h3 class="box-title">Daftar Event</h3>
                </div><!-- /.box-header -->
                <div class="box-body"> 
                	<table id="example2" class="table table-bordered table-hover dataTable-scroll-x">
                    	<thead>
                      		<tr>
                        		<th>No Inv</th>
                                <th>Tamggal Event</th>
                        		<th>Tanggal Inv</th>
		                        <th>Due Date</th>
        		                <th>No Form</th>
                                <th>Acara</th>  
                                <th>Quantity</th>
                                <th>Harga</th>
                                <th>Total Harga</th>
                                <th>Harga Net</th>
                                <th>Pb1</th>
                                <th>JLA(%)</th>
                                <th>Total Disc</th>
                                <th>Total</th>
                                <th>Action</th>                      
                	        </tr>
                      	</thead>
                    	<tbody>
                  <?
				  $query = query("select * from TABLE_EVENT WHERE lunas = 'T'");
				  $count = num($query);
				  if($count > 0){
				  	while($r=fetch($query)) {
						$tgl_event=date('j M Y',strtotime($r['tgl_event']));
						$tgl_inv=date('j M Y',strtotime($r['tgl_inv']));
						$due_date=date('j M Y',strtotime($r['due_date']));
						$harga = convert_to_rupiah($r['harga']);
						$total_hrg=convert_to_rupiah($r['total_hrg']);
						$harga_net=convert_to_rupiah($r['harga_net']);
						$pb1=convert_to_rupiah($r['pb1']);
						$jla=convert_to_rupiah($r['jla']);
						$total_disc=convert_to_rupiah($r['total_disc']);
						$total_harga=convert_to_rupiah($r['total_harga']);
						echo "<tr>";
						echo "<td>".$r['no_inv']."</td>";
						echo "<td>".$tgl_event."</td>";
						echo "<td>".$tgl_inv."</td>";
						echo "<td>".$due_date."</td>";
						echo "<td>".$r['no_form_jbr']."</td>";
						echo "<td>".$r['acara']."</td>";
						echo "<td>".$r['qty']."</td>";
						echo "<td>".$harga."</td>";
						echo "<td>".$total_hrg."</td>";
						echo "<td>".$harga_net."</td>";
						echo "<td>".$pb1."</td>";
						echo "<td>".$jla."</td>";
						echo "<td>".$total_disc."</td>";
						echo "<td>".$total_harga."</td>";
						echo "<td><a href='main.php?option=approve-event&id=".$r['_id']."' class='trash'>approve</a></td>";
					}
				  }			  
				  ?>

				  	</tbody>
                  </table>
                </div>
              </div><!-- /.box -->
              
              
	</section>