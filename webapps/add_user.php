	<?php
	// Handle AJAX request (start)
	if( isset($_POST['btn_submit']) && isset($_POST['name']) ){
		echo $_POST['name'];
 		exit;
	}
	// Handle AJAX request (end)
	?>
    <section class="content-header">
          <h1>
            User Management
            <small>Daftar User</small>
          </h1>
          <ol class="breadcrumb">
            <li><?echo"<a href=\"".SITE_URL."/main.php?option=Dashboard\">";?><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">User Management</li>
          </ol>
        </section>

        <!-- Main content -->
    <section class="content">
	<div class="box box-default">
		<div class="box-header with-border">
              <h3 class="box-title">Input Man Power</h3>   
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>           
        </div><!-- /.box-header -->  
        <script language="JavaScript">
			function kirpesan() {
				if(document.formpesan.user.value=="") {
					alert("Kolom User Login belum diisi");
					return false;
				}
				if(document.formpesan.nama.value=="") {
					alert("Kolom Nama belum diisi");
					return false;
				}
				if(document.formpesan.level.value=="") {
					alert("Kolom Barcode belum diisi");
					return false;
				}
				return true;				
			}
		</script>          
        
		<div class="box-body">
			<form role="form" id="form_product"  method="POST" name="formpesan" autocomplete="off" onSubmit='return kirpesan();' action>
              <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                    <label>Nama Lengkap: </label>
                    <input type="text" class="form-control" id="nama" placeholder="Nama Lengkap" style="text-transform:uppercase">    
                    <label>User Login: </label>
                    <input type="text" class="form-control" id="user" placeholder="User Login" style="text-transform:uppercase">
                   	<label>Password: </label>
                    <input type="password" class="form-control" id="password" placeholder="Password"  />
                    </div>
                    <div class="form-group">
                    <label>Level: <select id="level" class="form-control">
                    <?
					$lev = query("SELECT * FROM TABLE_LEVEL WHERE status = 'ACTIVE'");
					while ($l=fetch($lev)){
					?>
                    <option value="<? echo $l['_id']; ?>"><? echo $l['nama_level']; ?></option>
                    <? } ?>
                    </select></label>
                    </div>
                    <div class="form-group">         
                    	<input type="submit" id="btn_submit" value="Simpan" class="btn btn-danger"  />
                    </div>					
				</div><!-- /.col -->
              </div><!-- /.row -->			
        </div><!-- /.box-body -->
		</form>
        
        <script>
		$(document).ready(function(){
			$('#btn_submit').click(function(){
					var nama = $('#nama').val();
					var user = $('#user').val();
					var password = $('#password').val();
					var level = $('#level').val();
					var dataStr= 'nama='+nama+'&user='+user+'&password='+password+'&level='+level;
					
						$.ajax({
							type: "POST",
							url: "ajax/simpan_user.php",
							data: dataStr,
							//cache: false,
							success: function(response){
								$('#response').text('name : ' + response);					                                                    
							}									
						});								
				});
		});
		</script>
        
        
	</div><!-- /.box --> 
	<div class="box">
                <div class="box-header">
                  <h3 class="box-title">Daftar Produk</h3>
                </div><!-- /.box-header -->
                <div class="box-body"> 
                	<table id="example2" class="table table-bordered table-hover dataTable-scroll-x">
                    	<thead>
                      		<tr>
                        		<th>Nama Lengkap</th>
                                <th>User Login</th>
                        		<th>Level</th>
                                <th>Status</th>  
                                <th>Action</th>                      
                	        </tr>
                      	</thead>
                    	<tbody>
                  <?
				  $query = query("select * from TABLE_USER WHERE status='Y'");
				  $count = num($query);
				  if($count > 0){
				  	while($r=fetch($query)) {
						echo "<tr>";
						echo "<td>".$r['nama_lengkap']."</td>";
						echo "<td>".$r['nama_user']."</td>";
						//cari nama level
						$v = fetch(query("SELECT nama_level FROM TABLE_LEVEL WHERE _id = '$r[level_id]'"));
						echo "<td>".$v['nama_level']."</td>";
						echo "<td>AKTIF</td>";
						//echo "<td><a href='main.php?option=edit-cashier&id=".$r['_id']."' class='edit'>Edit</a>&nbsp; || &nbsp;";
						echo "<td><a href='main.php?option=hapus-user&id=".$r['_id']."' class='trash'>Hapus</a></td>";						
					}
				  }			  
				  ?>

				  	</tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
              
              
	</section>