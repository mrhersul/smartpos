	
    <section class="content-header">
          <h1>
            INPUT MASTER SUPPLIER
            <small>Input Master Supplier</small>
          </h1>
          <ol class="breadcrumb">
            <li><?echo"<a href=\"".SITE_URL."/main.php?option=Dashboard\">";?><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Input Master Supplier</li>
          </ol>
        </section>

        <!-- Main content -->
    <section class="content">
	<div class="box box-default">
		<div class="box-header with-border">
              <h3 class="box-title">Input Master Supplier</h3>   
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>           
        </div><!-- /.box-header -->  
        <script language="JavaScript">
			function kirpesan() {
				if(document.formpesan.kode_pr.value=="") {
					alert("Kolom Kode PR belum diisi");
					return false;
				}
				return true;				
			}
		</script>          
        
		<div class="box-body">
			<form role="form" id="form1"  method="POST" name="form1" autocomplete="off" action="main.php?option=simpan-supplier">
              <div class="row">
                <div class="col-md-6">
                    <div class="form-group">             
                        <label>Supplier ID: </label>
                        <?
						$sql = query("SELECT max(VENDORID) as maxBar FROM m_vendor WHERE DEPTID='800'");
						$cek = fetch($sql);
                        //$mnt = month();
						if($cek['maxBar'] == NULL) {
							echo "<input type='text' class='form-control' id='vendorid' name='vendorid' value='S-00001' readonly='readonly'>";
						} else {
                            $bar = $cek["maxBar"];
							$pilihan = substr($bar,0,2);
							$nobar = (int) substr($bar,2,5);
							$nobar++;
							$newID = $pilihan . sprintf("%05s", $nobar);

                            echo "<input type='text' class='form-control' id='vendorid' value='$newID' readonly='readonly' name='vendorid'>";
						}
					  ?> 
					</div>  	
                   	<div class="form-group">
                   		<label>Nomor Telepon :</label>
                   		<input type="tel" id="tlp" name="tlp" placeholder="Telepon" class="form-control">
                   	</div>
                   	<div class="form-group"> 
                        <label>Email : </label>
                        <input type="text" id="email" name="email" placeholder="Email" class="form-control">
					</div>
					<div class="form-group">
						<label>Bank :</label>
						<input type="text" id="banknm" name="banknm" placeholder="BANK" class="form-control" style="text-transform: uppercase">
					</div>
					<div class="form-group">
						<label>AN :</label>
						<input type="text" id="an" name="an" placeholder="Atas Nama" class="form-control" style="text-transform: uppercase">
					</div>
					<div class="form-group"> 
                        <label>Account Hutang : </label>
                        <select  id="acgl" name="acgl" class="form-control selectpicker" data-live-search="true" >
                    		<option value="-">-</option>
                    		<?
							$area=query("SELECT acno,acnm FROM m_coa1 WHERE acno like '21%' order by acnm");
							while($a=fetch($area)){
							?>
								<option value="<? echo $a['acno']; ?>"><? echo $a['acno'].' - '.$a['acnm']; ?></option>
							<?
							}
							?>
						</select>
					</div>			
				</div><!-- /.col -->
            <div class="col-md-6">
                    	<div class="form-group">
                    	<label>Nama Supplier :</label>
                    	<input type="text" id="vendornm" name="vendornm" placeholder="Nama Vendor" class="form-control" style="text-transform: uppercase">
						</div>
                      	<div class="form-group">
                      		<label>Contact Person :</label>
                      		<input type="text" id="ctprsn" name="ctprsn" placeholder="Contact Person" class="form-control" style="text-transform:uppercase">
                      	</div>
                      	<div class="form-group">
                      		<label>NPWP :</label>
                      		<input type="text" id="npwp" name="npwp" placeholder="NPWP" class="form-control">
                      	</div>
                      	<div class="form-group">
                      		<label>No Rekening :</label>
                      		<input type="text" id="norek" name="norek" placeholder="NOREK" class="form-control">
                      	</div>
                       	<div class="form-group">
                   		<label>Fax</label>
                   		<input type="tel" id="fax" name="fax" placeholder="FAX" class="form-control">
                   	</div>		
                   	<div class="form-group"> 
                        <label>Account Pembelian : </label>
                        <select  id="acrued" name="acrued" class="form-control selectpicker" data-live-search="true" >
                    		<option value="-">-</option>
                    		<?
							$acno=query("SELECT acno,acnm FROM m_coa1 WHERE acno like '21%' order by acnm");
							while($c=fetch($acno)){
							?>
								<option value="<? echo $c['acno']; ?>"><? echo $c['acno'].' - '.$c['acnm']; ?></option>
							<?
							}
							?>
						</select>
					</div>	
				</div><!-- /.col -->
             <div class="col-md-12">
             		<div class="form-group">
                        <label>Alamat</label>
                        <textarea class="form-control" rows="4" cols="50" id="addr" name="addr"></textarea>
                    </div>                    		
			</div>
             <div class="box-footer">
             <div class="col-md-11">
             	<!--<input type="button" class="btn btn-primary " id="addrow" value="Tambah Barang" /> -->
			 </div>
			 <div class="col-md-1">	
				<input type="submit" id="btn_submit" name="btn_submit" value="Simpan" class="btn btn-danger"/>
				 </div>
		</div>
              </div><!-- /.row -->	
              </form>		
        </div><!-- /.box-body -->
		<!-- </form> -->
	</div><!-- /.box --> 
	<div class="box">
                <div class="box-header">
                  <h3 class="box-title">Daftar Supplier</h3>
                </div><!-- /.box-header -->
                <div class="box-body"> 
                	<table id="example2" class="table table-bordered table-hover dataTable-scroll-x">
                    	<thead>
                      		<tr>
                                <th>Kode Supplier</th>
                                <th>Nama Supllier</th>
                        		<th>Telepon</th>
                                <th>Email</th>
                                <th>No Rek</th>
                                <th>Nama Bank</th>
                                <th>Atas Nama</th>
                                <th>Contact Person</th>
		                        <th>Action</th>                     
                	        </tr>
                      	</thead>
                    	<tbody>
                  <?php
				  $query = query("select * from m_vendor WHERE jenis = '2' order by vendornm");
				  $count = num($query);
				  if($count > 0){
				  	while($r=fetch($query)) {
						echo "<tr>";
						echo "<td>".$r['VENDORID']."</td>";
						echo "<td>".$r['VENDORNM']."</td>";
                        echo "<td>".$r['TLP']."</td>";
						echo "<td>".$r['EMAIL']."</td>";
						echo "<td>".$r['NOREK']."</td>";
						echo "<td>".$r['BANKNM']."</td>";
						echo "<td>".$r['AN']."</td>";
						echo "<td>".$r['CTPRSN']."</td>";
						echo "<td><a href='main.php?option=view-vendor&id=".$r['VENDORID']."' class='edit'>View</a></td>";
					}
				  }			  
				  ?>

				  	</tbody>
                  </table>
                </div>
              </div><!-- /.box -->
        
	</section>