-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Waktu pembuatan: 29. Juli 2018 jam 10:44
-- Versi Server: 5.5.54
-- Versi PHP: 5.3.10-1ubuntu3.26

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sulesoft_pos`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `LANGUAGE`
--

CREATE TABLE IF NOT EXISTS `LANGUAGE` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(5) NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `pendidikan` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data untuk tabel `menu`
--

INSERT INTO `menu` (`id`, `pendidikan`) VALUES
(1, 'TK'),
(2, 'SD'),
(3, 'SMP'),
(4, 'SMA/SMK'),
(5, 'Diploma'),
(6, 'S1'),
(7, 'S2'),
(8, 'S3');

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_cashier`
--

CREATE TABLE IF NOT EXISTS `table_cashier` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `outlet_id` int(11) NOT NULL,
  `cashier` varchar(100) NOT NULL,
  `password` varchar(10) NOT NULL,
  `level` varchar(1) NOT NULL,
  `status` varchar(10) NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `table_cashier`
--

INSERT INTO `table_cashier` (`_id`, `outlet_id`, `cashier`, `password`, `level`, `status`) VALUES
(1, 1, 'herman', '12345', '5', 'ACTIVE');

-- --------------------------------------------------------

--
-- Struktur dari tabel `TABLE_CUSTOMER`
--

CREATE TABLE IF NOT EXISTS `TABLE_CUSTOMER` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer` varchar(100) NOT NULL,
  `notelpon` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `TABLE_HIST_PRICE`
--

CREATE TABLE IF NOT EXISTS `TABLE_HIST_PRICE` (
  `TANGGAL` date NOT NULL DEFAULT '0000-00-00',
  `BARCODE` varchar(100) NOT NULL,
  `HARGA` int(11) NOT NULL,
  `HPP` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `TABLE_HIST_PRICE`
--

INSERT INTO `TABLE_HIST_PRICE` (`TANGGAL`, `BARCODE`, `HARGA`, `HPP`) VALUES
('2018-07-18', 'M0013', 12000, 5500),
('2018-07-18', 'M0001', 17000, 8000),
('2018-07-18', 'M0002', 17000, 8000),
('2018-07-18', 'M0003', 25000, 10000),
('2018-07-18', 'M0001', 15000, 8000),
('2018-07-18', 'M0001', 17000, 8000),
('2018-07-18', 'M0001', 15000, 8000),
('2018-07-18', 'M0001', 17000, 8000),
('2018-07-18', 'M0004', 17000, 8000),
('2018-07-18', 'M0005', 17000, 8000),
('2018-07-18', 'M0007', 23000, 11000),
('2018-07-18', 'M0008', 11000, 5500),
('2018-07-18', 'M0009', 11000, 5500),
('2018-07-18', 'M0010', 11000, 5500),
('2018-07-18', 'M0012', 15000, 7500),
('2018-07-18', 'M0013', 15000, 5500),
('2018-07-18', 'M0012', 15000, 8000),
('2018-07-18', 'M0001', 15000, 8000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `TABLE_LEVEL`
--

CREATE TABLE IF NOT EXISTS `TABLE_LEVEL` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(50) NOT NULL,
  `status` varchar(10) NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `TABLE_LEVEL`
--

INSERT INTO `TABLE_LEVEL` (`_id`, `nama_level`, `status`) VALUES
(1, 'ADMIN', 'ACTIVE'),
(2, 'MANAGER', 'ACTIVE'),
(3, 'SUPERVISOR', 'ACTIVE'),
(4, 'STAFF', 'ACTIVE'),
(5, 'KASIR', 'ACTIVE'),
(6, 'HEAD KASIR', 'ACTIVE');

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_outlet`
--

CREATE TABLE IF NOT EXISTS `table_outlet` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `outlet` varchar(100) NOT NULL,
  `status` varchar(10) NOT NULL,
  `keterangan` varchar(254) NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `table_outlet`
--

INSERT INTO `table_outlet` (`_id`, `outlet`, `status`, `keterangan`) VALUES
(1, 'CARNIVALIA', 'ACTIVE', 'FOOD AND BEVERAGE'),
(2, 'MYSTERIA', 'ACTIVE', 'BEVERAGE AND FOOD'),
(3, 'FOOD YARD', 'ACTIVE', 'FOOD AND BEVERAGE'),
(4, 'KANTIN', 'ACTIVE', 'FOOD AND BEVERAGE');

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_product_catalog`
--

CREATE TABLE IF NOT EXISTS `table_product_catalog` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `outlet_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `gambar` varchar(250) NOT NULL,
  `barcode` varchar(100) NOT NULL,
  `unit_price` double NOT NULL,
  `hpp` double NOT NULL,
  `status` varchar(10) NOT NULL,
  `status_kirim` int(11) NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data untuk tabel `table_product_catalog`
--

INSERT INTO `table_product_catalog` (`_id`, `outlet_id`, `name`, `gambar`, `barcode`, `unit_price`, `hpp`, `status`, `status_kirim`) VALUES
(1, 1, 'Es Kelapa', '', 'M0001', 15000, 8000, 'ACTIVE', 0),
(2, 1, 'Juice Belimbing', '', 'M0002', 17000, 8000, 'ACTIVE', 0),
(3, 1, 'Es Kelapa Orange', '', 'M0003', 25000, 10000, 'ACTIVE', 0),
(4, 1, 'Hot Lemon Tea', '', 'M0004', 17000, 8000, 'ACTIVE', 0),
(5, 1, 'Ice Lemon Tea', '', 'M0005', 17000, 8000, 'ACTIVE', 0),
(6, 1, 'Hot Mineral', '', 'M0006', 5000, 1500, 'ACTIVE', 0),
(7, 1, 'Orange', '', 'M0007', 23000, 11000, 'ACTIVE', 0),
(8, 1, 'Hot Tea', '', 'M0008', 11000, 5500, 'ACTIVE', 0),
(9, 1, 'Hot Coffee', '', 'M0009', 11000, 5500, 'ACTIVE', 0),
(10, 1, 'Ice Tea', '', 'M0010', 11000, 5500, 'ACTIVE', 0),
(11, 1, 'Promo Ice Tea', '', 'M0011', 6364, 5500, 'ACTIVE', 0),
(12, 1, 'El Alpukat', '', 'M0012', 15000, 8000, 'ACTIVE', 0),
(13, 1, 'El Anggur', '', 'M0013', 15000, 5500, 'ACTIVE', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `TABLE_SALE`
--

CREATE TABLE IF NOT EXISTS `TABLE_SALE` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `outlet_id` int(11) NOT NULL,
  `status` varchar(40) NOT NULL,
  `payment` varchar(50) NOT NULL,
  `total` double NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `order` int(11) NOT NULL,
  `cashier_id` int(11) NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_sale_lineitem`
--

CREATE TABLE IF NOT EXISTS `table_sale_lineitem` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `sale_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit_price` double NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `TABLE_STOCK`
--

CREATE TABLE IF NOT EXISTS `TABLE_STOCK` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `outlet_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `hpp` double NOT NULL,
  `cost` double NOT NULL,
  `date_added` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `TABLE_STOCK_SUM`
--

CREATE TABLE IF NOT EXISTS `TABLE_STOCK_SUM` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `TABLE_SUPPLIER`
--

CREATE TABLE IF NOT EXISTS `TABLE_SUPPLIER` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier` varchar(100) NOT NULL,
  `status` varchar(10) NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `TABLE_TIPE_BAYAR`
--

CREATE TABLE IF NOT EXISTS `TABLE_TIPE_BAYAR` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `tipe_bayar` varchar(50) NOT NULL,
  `status` varchar(10) NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `TABLE_TIPE_BAYAR`
--

INSERT INTO `TABLE_TIPE_BAYAR` (`_id`, `tipe_bayar`, `status`) VALUES
(1, 'CASH', 'ACTIVE'),
(2, 'BNI', 'ACTIVE'),
(3, 'BCA', 'ACTIVE');

-- --------------------------------------------------------

--
-- Struktur dari tabel `TABLE_TRANSFER_TO_OUTLET`
--

CREATE TABLE IF NOT EXISTS `TABLE_TRANSFER_TO_OUTLET` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `notrans` varchar(10) NOT NULL,
  `id_outlet` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `jumlah` double NOT NULL,
  `pengirim` varchar(100) NOT NULL,
  `status_kirim` varchar(10) NOT NULL,
  `status_sync` int(11) NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `TABLE_USER`
--

CREATE TABLE IF NOT EXISTS `TABLE_USER` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `level_id` int(11) NOT NULL,
  `nama_user` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `status` enum('Y','N') NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `TABLE_USER`
--

INSERT INTO `TABLE_USER` (`_id`, `level_id`, `nama_user`, `password`, `nama_lengkap`, `status`) VALUES
(1, 1, 'admin', 'cb18fae562143d4d02d9e1fbef06bd9a', 'Administrator', 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `vo_users`
--

CREATE TABLE IF NOT EXISTS `vo_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_id` varchar(23) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `encrypted_password` varchar(80) NOT NULL,
  `salt` varchar(10) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id` (`unique_id`,`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `vo_users`
--

INSERT INTO `vo_users` (`id`, `unique_id`, `name`, `email`, `encrypted_password`, `salt`, `created_at`, `updated_at`) VALUES
(1, '5b5b95595f43e8.87962575', 'Sule', 'sule@auto.id', '123', '123', '2018-07-28 04:57:45', '0000-00-00 00:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
