<?php
include "config/koneksi.php";
include "config/library.php";
include "config/fungsi_indotgl.php";
include "config/fungsi_combobox.php";
include "config/class_paging.php";
include "config/fungsi_thumb.php";
include "config/fungsi_enc.php";
include "config/fungsi_browser.php";

// Bagian Home
if ($_GET['module']=='home'){

  if ($_SESSION['leveluser']){

	$jam=date("H:i:s");
	$tgl=tgl_indo(date("Y m d")); 	
  	echo "<br /><p align=center>Hai <b>$_SESSION[namalengkap]</b>, selamat datang di halaman HUMAN RESOURCES INFORMATION SYSTEM - PT DUNKINDO LESTARI. 
          Silahkan klik menu pilihan yang berada di bagian header atau pada bagian Control Panel di bawah ini. <br /> <b>$hari_ini, $tgl, $jam </b>WIB</p><br />";	
		
  	echo "<table class='list'><thead>
		<td class='center' colspan=5><center>ADMINISTRATOR Control Panel</center></td></thead>
		<tr>
		  <td width=120 align=center><a href=media.php?module=user><img src=images/user.jpg border=none><br /><b>Manajemen User Admin</b></a></td>
		  <td width=120 align=center><a href=media.php?module=modul><img src=images/modul.png border=none><br /><b>Manajemen Modul</b></a></td>
		  <td width=120 align=center><a href=media.php?module=menuutama><img src=images/berita.png border=none><br /><b>Menu</b></a></td>
		  <td width=120 align=center><a href=media.php?module=submenu><img src=images/komentar.png border=none><br /><b>Sub Menu</b></a></td>
		  <td width=120 align=center><a href=media.php?module=listcuti><img src=images/poling.png border=none><br><b>Administrative Task</b></a></td>
		  
    	</tr>
    	</table>";
  }
 else {
 // Cek User LDAP
 $user = $_SESSION['namauser'];
 $nip = $_SESSION['nip'];
 $cuser = mssql_query("SELECT * FROM karyawan WHERE nip = '$nip'");
 $c = mssql_fetch_array($cuser);
 
 $jam=date("H:i:s");
	$tgl=tgl_indo(date("Y m d")); 	
  	echo "<br /><p align=center>Hai <b>$_SESSION[namalengkap]</b>, selamat datang di halaman HUMAN RESOURCES INFORMATION SYSTEM - PT DUNKINDO LESTARI. 
          Silahkan klik menu pilihan yang berada di bagian header atau pada bagian Control Panel di bawah ini. <br /> <b>$hari_ini, $tgl, $jam </b>WIB</p><br />";
 
 }
}

// Bagian Modul
elseif ($_GET['module']=='modul'){
  if ($_SESSION['leveluser']==1){
    include "modul/mod_modul/modul.php";
  } else {
	echo "Otorisasi anda tidak diijinkan untuk mengakses halaman ini!
      <br><input type=button value=Back onclick=self.history.back()>";
  }
  
  echo "<table class='list'><thead>
		<td class='center' colspan=5><center>ADMINISTRATOR Control Panel</center></td></thead>
		<tr>
		  <td width=120 align=center><a href=media.php?module=user><img src=images/user.jpg border=none><br /><b>Manajemen User Admin</b></a></td>
		  <td width=120 align=center><a href=media.php?module=modul><img src=images/modul.png border=none><br /><b>Manajemen Modul</b></a></td>
		  <td width=120 align=center><a href=media.php?module=menuutama><img src=images/berita.png border=none><br /><b>Menu</b></a></td>
		  <td width=120 align=center><a href=media.php?module=submenu><img src=images/komentar.png border=none><br /><b>Sub Menu</b></a></td>
		  <td width=120 align=center><a href=media.php?module=listcuti><img src=images/poling.png border=none><br><b>Administrative Task</b></a></td>
		  
    	</tr>
    	</table>";
}

// Bagian User
elseif ($_GET['module']=='user'){
  if ($_SESSION['leveluser']==1){
    include "modul/mod_users/user.php";
  } else {
	echo "Otorisasi anda tidak diijinkan untuk mengakses halaman ini!
      <br><input type=button value=Back onclick=self.history.back()>";
  }
  
  echo "<table class='list'><thead>
		<td class='center' colspan=5><center>ADMINISTRATOR Control Panel</center></td></thead>
		<tr>
		  <td width=120 align=center><a href=media.php?module=user><img src=images/user.jpg border=none><br /><b>Manajemen User Admin</b></a></td>
		  <td width=120 align=center><a href=media.php?module=modul><img src=images/modul.png border=none><br /><b>Manajemen Modul</b></a></td>
		  <td width=120 align=center><a href=media.php?module=menuutama><img src=images/berita.png border=none><br /><b>Menu</b></a></td>
		  <td width=120 align=center><a href=media.php?module=submenu><img src=images/komentar.png border=none><br /><b>Sub Menu</b></a></td>
		  <td width=120 align=center><a href=media.php?module=listcuti><img src=images/poling.png border=none><br><b>Administrative Task</b></a></td>
		  
    	</tr>
    	</table>";
}

// Bagian Menu Utama
elseif ($_GET['module']=='menuutama'){
  if ($_SESSION['leveluser']==1){
    include "modul/mod_menuutama/menuutama.php";
  } else {
	echo "Otorisasi anda tidak diijinkan untuk mengakses halaman ini!
      <br><input type=button value=Batal onclick=self.history.back()>";
  }
  
  echo "<table class='list'><thead>
		<td class='center' colspan=5><center>ADMINISTRATOR Control Panel</center></td></thead>
		<tr>
		  <td width=120 align=center><a href=media.php?module=user><img src=images/user.jpg border=none><br /><b>Manajemen User Admin</b></a></td>
		  <td width=120 align=center><a href=media.php?module=modul><img src=images/modul.png border=none><br /><b>Manajemen Modul</b></a></td>
		  <td width=120 align=center><a href=media.php?module=menuutama><img src=images/berita.png border=none><br /><b>Menu</b></a></td>
		  <td width=120 align=center><a href=media.php?module=submenu><img src=images/komentar.png border=none><br /><b>Sub Menu</b></a></td>
		  <td width=120 align=center><a href=media.php?module=listcuti><img src=images/poling.png border=none><br><b>Administrative Task</b></a></td>
		  
    	</tr>
    	</table>";
}

// Bagian Sub Menu
elseif ($_GET['module']=='submenu'){
  if ($_SESSION['leveluser']==1){
    include "modul/mod_submenu/submenu.php";
  } else {
	echo "Otorisasi anda tidak diijinkan untuk mengakses halaman ini!
      <br><input type=button value=Batal onclick=self.history.back()>";
  }
  
  echo "<table class='list'><thead>
		<td class='center' colspan=5><center>ADMINISTRATOR Control Panel</center></td></thead>
		<tr>
		  <td width=120 align=center><a href=media.php?module=user><img src=images/user.jpg border=none><br /><b>Manajemen User Admin</b></a></td>
		  <td width=120 align=center><a href=media.php?module=modul><img src=images/modul.png border=none><br /><b>Manajemen Modul</b></a></td>
		  <td width=120 align=center><a href=media.php?module=menuutama><img src=images/berita.png border=none><br /><b>Menu</b></a></td>
		  <td width=120 align=center><a href=media.php?module=submenu><img src=images/komentar.png border=none><br /><b>Sub Menu</b></a></td>
		  <td width=120 align=center><a href=media.php?module=listcuti><img src=images/poling.png border=none><br><b>Administrative Task</b></a></td>
		  
    	</tr>
    	</table>";
}

// Bagian Daftar Cuti untuk Admin
elseif ($_GET['module']=='listcuti'){
	if ($_SESSION['leveluser']==1 OR $_SESSION['leveluser']==2){
		include "modul/mod_admin/listcuti.php";
	} else {
		echo "Otorisasi anda tidak diijinkan untuk mengakses halaman ini!
		<br><input type=button value=Batal onclick=self.history.back()>";
	}
	
	echo "<table class='list'><thead>
		<td class='center' colspan=5><center>ADMINISTRATOR Control Panel</center></td></thead>
		<tr>
		  <td width=120 align=center><a href=media.php?module=user><img src=images/user.jpg border=none><br /><b>Manajemen User Admin</b></a></td>
		  <td width=120 align=center><a href=media.php?module=modul><img src=images/modul.png border=none><br /><b>Manajemen Modul</b></a></td>
		  <td width=120 align=center><a href=media.php?module=menuutama><img src=images/berita.png border=none><br /><b>Menu</b></a></td>
		  <td width=120 align=center><a href=media.php?module=submenu><img src=images/komentar.png border=none><br /><b>Sub Menu</b></a></td>
		  <td width=120 align=center><a href=media.php?module=listcuti><img src=images/poling.png border=none><br><b>Administrative Task</b></a></td>
		  
    	</tr>
    	</table>";
}

elseif ($_GET['module']=='carilist'){
	if ($_SESSION['leveluser']==1 OR $_SESSION['leveluser']==2){
		include "modul/mod_admin/carilist.php";
	} else {
		echo "Otorisasi anda tidak diijinkan untuk mengakses halaman ini!
		<br><input type=button value=Batal onclick=self.history.back()>";
	}
	echo "<table class='list'><thead>
		<td class='center' colspan=5><center>ADMINISTRATOR Control Panel</center></td></thead>
		<tr>
		  <td width=120 align=center><a href=media.php?module=user><img src=images/user.jpg border=none><br /><b>Manajemen User Admin</b></a></td>
		  <td width=120 align=center><a href=media.php?module=modul><img src=images/modul.png border=none><br /><b>Manajemen Modul</b></a></td>
		  <td width=120 align=center><a href=media.php?module=menuutama><img src=images/berita.png border=none><br /><b>Menu</b></a></td>
		  <td width=120 align=center><a href=media.php?module=submenu><img src=images/komentar.png border=none><br /><b>Sub Menu</b></a></td>
		  <td width=120 align=center><a href=media.php?module=listcuti><img src=images/poling.png border=none><br><b>Administrative Task</b></a></td>
		  
    	</tr>
    	</table>";
}

// Bagian Cuti atau Ijin
elseif ($_GET['module']=='cutiijin'){
	include "modul/mod_cuti/cuti.php";
}

// Bagian View Cuti atau Ijin
elseif ($_GET['module']=='viewijin'){
	include "modul/mod_cuti/view.php";
}

// Bagian IDT
elseif ($_GET['module']=='idt'){
	include "modul/mod_idt/idt.php";
}

// Bagian Administrative Task
elseif ($_GET['module']=='task'){
	include "modul/mod_cuti/task.php";
}

// Bagian Batal Cuti
elseif ($_GET['module']=='batal'){
	include "modul/mod_cuti/btlcuti.php";
}

// Apabila modul tidak ditemukan
else{
  //echo "<p><b>MODUL BELUM ADA ATAU BELUM LENGKAP</b></p>";
  header('location:media.php?module=home');
  
}
?>
