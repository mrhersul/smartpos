<script language="javascript">
function printdiv(printpage)
{
var headstr = "<html><head><title></title></head><body>";
var footstr = "</body>";
var newstr = document.all.item(printpage).innerHTML;
var oldstr = document.body.innerHTML;
document.body.innerHTML = headstr+newstr+footstr;
window.print();
document.body.innerHTML = oldstr;
return false;
}
</script>

    <section class="content-header"> 
          <h1>
            b'Raya System 
            <small>Jasa Boga Raya</small>
          </h1>
          <ol class="breadcrumb">
            <li><?echo"<a href=\"".SITE_URL."/main.php?option=Dashboard\">";?><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">b'Raya System</li>
          </ol>
        </section> 
<section class="content">
<div id="div_print" >
<div class="box">
	
                <div class="box-header">
                  <h3 class="box-title">COGS Resep</h3>
                </div><!-- /.box-header -->
                <div class="box-body"> 
					

            
					
					
<div class="box">
	
                <div class="box-header">
                  <h3 class="box-title">Daftar Resep</h3>
                </div><!-- /.box-header -->
                <div class="box-body"> 
                	<table id="example2" class="table table-bordered table-hover dataTable-scroll-x">
                    	<thead>
                      		<tr>
                        		<th>No</th>
								<th>Kode Resep</th>
                                <th>Nama Menu</th>
								<th>Outlet</th>
								<th>Resep</th>
								<th>COGS</th>
                        		<th>Jual</th>

                                <th></th> 
                                
                                                  
                	        </tr>
                      	</thead>
                    	<tbody>
                  <?
				  $query = query("select * from table_product_catalog where status ='ACTIVE' and kode_resep is NOT NULL order by _id desc"); 
				  $count = num($query);
				  if($count > 0){
					  $i=0;
				  	while($r=fetch($query)) {
						$i=$i+1;
								 
						echo "<tr>";
						echo "<td>".$i."</td>";
						echo "<td>".$r[kode_resep]."</td>"; 
						echo "<td>".$r[name]."</td>"; 
						echo "<td>".cari("outlet", "table_outlet","_id='".$r[outlet_id]."'")."</td>"; 
						echo "<td>";
						$cogs = 0;
						$total_cogs = 0;
						 $query_resep = query("select * from m_resep_detail where kode_resep ='".$r[kode_resep]."'"); 
							while($s=fetch($query_resep)) {
								
								$cogs = carihpp($s[kode_barang],$s[qty],$s[kode_satuan],$s[satuan]);
								$total_cogs =$total_cogs+$cogs;
								
									if($cogs==0) { echo "<span style='color:red'>";}
									echo $s[kode_barang]."-(".$s[qty]." ".$s[satuan].") ".$s[nama_barang]." Rp. ".round($cogs,2)."<br>";
									if($cogs==0) { echo "</span>";}
								if($_GET[update]==1) {
								//update table m_resep_detail
								//update kategori
								$kategori= cari("kode_kategori", "barang","kode_barang='".$s[kode_barang]."' and kode_unit='".$s[kode_satuan]."'");
								updatedata($s[id],"m_resep_detail","kategori",$kategori);
								//isi harga satuan
								$satuan = cari("harga", "barang","kode_barang='".$s[kode_barang]."' and kode_unit='".$s[kode_satuan]."'");
								updatedata($s[id],"m_resep_detail","harga_satuan",$satuan);
								//update harga cogs
								updatedata($s[id],"m_resep_detail","harga_cogs",$cogs);
								}
							}
						echo "</td>";
						echo "<td>".round($total_cogs,2)."</td>";
						echo "<td>".round($r[unit_price],2)."</td>";
						echo "<td></td>";
						echo "</tr>";
						
						
					}
				  }			  
				  ?>

				  	</tbody>
                  </table>
				
                </div>
              </div><!-- /.box -->
	</div>
	
</section>
