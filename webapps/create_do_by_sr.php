	<!--<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script> -->
	
	<?php
	// Handle AJAX request (start)
	if( isset($_POST['btn_submit']) && isset($_POST['name']) ){
		echo $_POST['name'];
 		exit;
	}
	
	?>
    <section class="content-header">
          <h1>
            DELIVERY ORDER
            <small>Input Delivery Order</small>
          </h1>
          <ol class="breadcrumb">
            <li><?echo"<a href=\"".SITE_URL."/main.php?option=Dashboard\">";?><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Input Delivery Order</li>
          </ol>
        </section>

        <!-- Main content -->
    <section class="content">
	<div class="box">
                <div class="box-header">
                  <h3 class="box-title">Daftar DO By SR</h3>
                </div><!-- /.box-header -->
                <div class="box-body"> 
                	<table id="example2" class="table table-bordered table-hover dataTable-scroll-x">
                    	<thead>
                      		<tr>
                                <th>Tanggal Order</th>
                                <th>Tanggal Kirim</th>
                        		<th>Kode SR</th>
                        		<th>Area</th>
		                        <th>Nama Pembuat</th>
								<th>Keterangan</th>
		                        <th>Status</th> 
                                <th>Action</th>                      
                	        </tr>
                      	</thead>
                    	<tbody>
                  <?
				  $query = query("select * from xx_sr_header WHERE kode_sr IN (select kode_sr from xx_sr_detail WHERE request = '2' AND status IS NULL)");
				  $count = num($query);
				  $total = 0;	
				  if($count > 0){
				  	while($r=fetch($query)) {
						$tgl=date('j M Y H:i',strtotime($r['tgl_sr']));
						$kir=date('j M Y',strtotime($r['tgl_kirim']));
						echo "<tr>";
						//cari outlet
						$o = fetch(query("SELECT areanm FROM m_area WHERE areaid = '$r[kode_area]'"));
						echo "<td>".$tgl."</td>";
						echo "<td>".$kir."</td>";
                        echo "<td>".$r['kode_sr']."</td>";
						echo "<td>".$o['areanm']."</td>";
						echo "<td>".$r['nama_pembuat']."</td>";	
						echo "<td>".$r['keterangan']."</td>";
						echo "<td>OPEN</td>";
						echo "<td><a href='main.php?option=create-do&id=".$r['kode_sr']."' class='edit'>Proses</a></td>";
					}
				  }			  
				  ?>

				  	</tbody>
                  </table>
                </div>
              </div><!-- /.box -->
	</section>