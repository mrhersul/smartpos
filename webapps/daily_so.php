	<?php
	// Handle AJAX request (start)
	if( isset($_POST['btn_submit']) && isset($_POST['name']) ){
		echo $_POST['name'];
 		exit;
	}
	
	?>
    <section class="content-header">
          <h1>
            DAILY STOCK OPNAME
            <small>Daftar Stock Opname</small>
          </h1>
          <ol class="breadcrumb">
            <li><?echo"<a href=\"".SITE_URL."/main.php?option=Dashboard\">";?><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Daftar Stock Opname</li>
          </ol>
        </section>

        <!-- Main content -->
    <section class="content">
	<div class="box box-default">
		<div class="box-header with-border">
              <h3 class="box-title">Input Produk</h3>   
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>           
        </div><!-- /.box-header -->  
        <script language="JavaScript">
			function kirpesan() {
				if(document.formpesan.name.value=="") {
					alert("Kolom Nama Outlet belum diisi");
					return false;
				}
				if(document.formpesan.keterangan.value=="") {
					alert("Kolom Keterangan belum diisi");
					return false;
				}
				return true;				
			}
		</script>          
        
		<div class="box-body">
			<form role="form" id="form_product"  method="POST" name="formpesan" autocomplete="off" onSubmit='return kirpesan();' action>
              <div class="row">
                <div class="col-md-6">
                    <div class="form-group">                    	
                        <label>Nama Outlet: </label>
                        <input type="text" class="form-control" id="name" placeholder="Nama Outlet">
                        <label>Keterangan</label>
                        <input type="text" class="form-control" id="keterangan" placeholder="Keterangan" />
                    </div>
                    <div class="form-group">         
                    	<input type="submit" id="btn_submit" value="Simpan" class="btn btn-danger"  />
                    </div>					
				</div><!-- /.col -->
              </div><!-- /.row -->			
        </div><!-- /.box-body -->
		</form>
        
        <script>
		$(document).ready(function(){
			$('#btn_submit').click(function(){
					var name = $('#name').val();
					var keterangan = $('#keterangan').val();
					var dataStr= 'name='+name+'&keterangan='+keterangan;					
						$.ajax({
							type: "POST",
							url: "ajax/simpan_outlet.php",
							data: dataStr,
							//cache: false,
							success: function(response){
								$('#response').text('name : ' + response);					                                                    
							}									
						});								
				});
		});
		</script>
        
        
	</div><!-- /.box --> 
	<div class="box">
                <div class="box-header">
                  <h3 class="box-title">Daftar Outlet</h3>
                </div><!-- /.box-header -->
                <div class="box-body"> 
                	<table id="example2" class="table table-bordered table-hover dataTable-scroll-x">
                    	<thead>
                      		<tr>
                        		<th>Outlet</th>
                                <th>Keterangan</th>
                        		<th>Status</th>  
                                <th>Action</th>                      
                	        </tr>
                      	</thead>
                    	<tbody>
                  <?
				  $query = query("select * from TABLE_OUTLET WHERE status='ACTIVE'");
				  $count = num($query);
				  $total = 0;	
				  if($count > 0){
				  	while($r=fetch($query)) {
						echo "<tr>";
						echo "<td>".$r['outlet']."</td>";
						echo "<td>".$r['keterangan']."</td>";
						echo "<td>".$r['status']."</td>";
						echo "<td><a href='main.php?option=edit-outlet&id=".$r['_id']."' class='edit'>Edit</a>&nbsp; || &nbsp;";
						echo "<a href='main.php?option=hapus-outlet&id=".$r['_id']."' class='trash'>Hapus</a></td>";
					}
				  }			  
				  ?>

				  	</tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
	</section>