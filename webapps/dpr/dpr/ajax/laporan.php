<?
	require('../config.php');
	//require('../function.php');
	//require('../header.php');
?>
	
	<div class="box">
		<div class="row">
            <div class="col-xs-12">
                <div class="box-header">
                  <h3 class="box-title">DPR</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered table-striped tabel">
                    <thead>
                      <tr>
                        <th>Club</th>
                        <th>Date</th>
                        <th>Descriptions</th>
                        <th>Amount</th>                        
                      </tr>
                    </thead>
					<?
					$jenis = intval($_GET['jns']);
					$club = $_GET['club'];
					$start_date= date('Ymd',strtotime($_GET['datefrm']));
					$end_date=date('Ymd',strtotime($_GET['dateto']));
					$sql=query("EXEC SP_DPR_FOR_WEB '$start_date','$end_date','$jenis','$club'");?>
					<tbody>
					<?
					if(num($sql)>0){
						while($dpr=fetch($sql)){
							$tgl=date('j M Y',strtotime($dpr['DATE']));
							echo "<tr>";
							echo "<td>".$dpr['CLUB']."</td>";
							echo "<td>".$tgl."</td>";
							echo "<td>".$dpr['DESCRIPTION']."</td>";
							echo "<td>".$dpr['AMOUNT']."</td>";
							echo "</tr>";						
						}
						
					}else{
						echo "<tr>";
						echo "<td colspan=\"4\" align=\"center\">Data Tidak ditemukan.</td>";
						echo "</tr>";
					}
					?>                                   
                    </tbody>                    
                  </table>
                </div><!-- /.box-body --> 
				<div class="box-footer">
				
                </div>				
			</div><!-- /.col -->
          </div><!-- /.row -->
	</div><!-- /.box -->