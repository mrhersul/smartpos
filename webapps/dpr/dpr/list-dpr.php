	<section class="content-header">
          <h1>
            Laporan Kegiatan            
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
          </ol>
    </section>
	<!-- Main content -->
    <section class="content">
	<div class="box box-default">
		<div class="box-header with-border">
              <h3 class="box-title">Daily Report Production</h3>              
        </div><!-- /.box-header -->
		<div class="box-body">
			<form role="form" method="POST" autocomplete="off">
              <div class="row">
                <div class="col-md-6">
					<div class="form-group">
                      <label>Find DPR</label>
                      <select id="cmbDpr" class="form-control">
						<option value="0">All</option>
						<option value="1">Dues Sales</option>
						<option value="2">Terms Sales</option>
						<option value="3">PT</option>
						<option value="4">Manual Payment</option>
						<option value="5">Others</option>
                      </select>
                    </div>					
					<div class="form-group">
                      <label>Clubs:</label>
                      <select id="cmbClub" class="form-control">
						<option value="0">All</option>
						<?
						$sql=query("select club_id,nama_club from master_club");
						while($opt=fetch($sql)){
							echo "<option value=\"".$opt['club_id']."\">".$opt['nama_club']."</option>";
						}
						?>						
                      </select>
                    </div>
				</div><!-- /.col -->
				<div class="col-md-6">					
					<!-- Date range -->
					<div class="form-group">
                    <label>Start Date</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right reservation" id="start_date">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
				  <div class="form-group">
                    <label>End Date</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right reservation" id="end_date" onchange="showUser(this.value)">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
				</div><!-- /.col -->
              </div><!-- /.row -->
			
        </div><!-- /.box-body -->		
		</form>
	</div><!-- /.box -->
	<script>
	function showUser(str) {
		var jenis_dpr = $("#cmbDpr").val();
		var club_id = $("#cmbClub").val();
		var start_date = $("#start_date").val();
		if (str == "") {
			document.getElementById("txtHint").innerHTML = "";
			return;
		} else {
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
		
		xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET","ajax/laporan.php?jns="+jenis_dpr+"&club="+club_id+"&datefrm="+start_date+"&dateto="+str,true);
        xmlhttp.send();
		}
	}
	</script>
	<script type="text/javascript">
		$(function() {
			 $("#cmbRPTP").change(function(){				 
				  var id_rptp = $(this).val();
		 
				  $.ajax({
					 type: "POST",
					 dataType: "html",
					 url: "function.php",
					 data: "actionfunction=get_ropp&id_rptp="+id_rptp,
					 success: function(msg){
						 if(msg == ''){
								 $("select#cmbROPP").html('<option value="">--Pilih ROPP--</option>');								 
						 }else{
								   $("select#cmbROPP").html(msg);                                                       
						 }		                                                        
					 }
				  });                    
			 });
			
			 		   
		});
	</script>
	<div id="txtHint"></div>
	
	
	</section>