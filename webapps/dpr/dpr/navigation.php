	<header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini">B</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>BEST</b>Fitness</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              <!-- Notifications: style can be found in dropdown.less -->
              <!-- Tasks: style can be found in dropdown.less -->
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                  <span class="hidden-xs"><?echo $_SESSION['name'];?></span>
                </a>
                <ul class="dropdown-menu">
					<li><!-- Task item -->
							<?php echo"<a href=\"".SITE_URL."/main.php?option=sign-out\">";?>
							<h5>Sign Out</h5>                       
							</a>
					</li><!-- end task item -->					
				</ul>
              </li>
              <!-- Control Sidebar Toggle Button -->              
            </ul>
          </div>
        </nav>
      </header>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?echo $_SESSION['name'];?></p>
				<p>Hak Akses : <?echo $_SESSION['akses'];?></p>
            </div>
          </div>
          <!-- search form -->
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">Main Menu</li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Human Resource</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><?echo"<a href=\"".SITE_URL."/main.php?option=form-cuti\">";?><i class="fa fa-circle-o"></i> Form Cuti</a></li>
                <li><?echo"<a href=\"".SITE_URL."/main.php?option=form-izin\">";?><i class="fa fa-circle-o"></i> Form Izin</a></li>
				<li><a href="#"><i class="fa fa-circle-o"></i> Cek Absen</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Cek Masa Cuti</a></li>
				<li><a href="#"><i class="fa fa-circle-o"></i> Request Slip Gaji</a></li>                
              </ul>
            </li>            
			<li><?echo"<a href=\"".SITE_URL."/main.php?option=daftar-dpr\">";?><i class="fa fa-list"></i><span>DPR</span></a></li> 
                     
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- =============================================== -->
