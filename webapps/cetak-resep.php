<script language="javascript">
function printdiv(printpage)
{
var headstr = "<html><head><title></title></head><body>";
var footstr = "</body>";
var newstr = document.all.item(printpage).innerHTML;
var oldstr = document.body.innerHTML;
document.body.innerHTML = headstr+newstr+footstr;
window.print();
document.body.innerHTML = oldstr;
return false;
}
</script>
<style>
</style>
    <section class="content-header"> 
          <h1>
            b'Raya System 
            <small>Jasa Boga Raya</small>
          </h1>
          <ol class="breadcrumb">
            <li><?echo"<a href=\"".SITE_URL."/main.php?option=Dashboard\">";?><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">b'Raya System</li>
          </ol>
        </section> 
<section class="content">
<div id="div_print" >

	<div class="box">
                <div class="box-header">
                  <h3 class="box-title">Daftar Resep</h3>
                </div><!-- /.box-header -->
                <div class="box-body"> 
                	<table id="example2" class="table table-bordered table-hover dataTable-scroll-x" data-pagination="false">
                    	<thead>
                      		<tr>
                        		<th>No</th>
                                <th>Kode Resep</th>
								<th>Nama Resep</th>
								<th>Nama Outlet</th>
                        		<th>Detail Resep</th>
								<th>Harga COGS</th>
								<th>Harga Jual</th>
		                        <th>Modified</th>
                	        </tr>
                      	</thead>
                    	<tbody>
                  <?
				  $query = query("select * from m_resep where status != 9 order by id desc"); 
				  $count = num($query);
				  if($count > 0){
					  $i=0;
				  	while($r=fetch($query)) {
						$i=$i+1;
						$tgl_event=date('j M Y',strtotime($r['waktu_pelayanan_awal'])); 
						$tgl_inv=date('j M Y',strtotime($r['tgl_inv']));
					 
						echo "<tr>";
						echo "<td>".$i."</td>"; 
						echo "<td>".$r[kode_resep]."</td>";  
						echo "<td>".$r[nama_resep]."</td>";
						echo "<td>".$r[outlet]."</td>";
						echo "<td>";
						echo "<table border='1'>
						<tr>
							<th>Kode Barang</th>
							<th>Nama Barang</th>
							<th>Jumlah</th>
							<th>Satuan</th>
							<th>Harga Satuan</th>
							<th>Harga COGS</th>
						</tr>";
						
						$cogs = 0;
						$total_cogs = 0;
						 $query_resep = query("select * from m_resep_detail where kode_resep ='".$r[kode_resep]."' and status !=9"); 
							while($s=fetch($query_resep)) {
								
								$cogs = carihpp($s[kode_barang],$s[qty],$s[kode_satuan],$s[satuan]);
								$total_cogs =$total_cogs+$cogs;
								echo "<tr>";
									if($cogs==0) { echo "<span style='color:red'>";}
									echo "<td>".$s[kode_barang]."</td>";
									echo "<td>".$s[nama_barang]."</td>";
									echo "<td>".$s[qty]."</td>";
									echo "<td>".$s[satuan]."</td>";
									echo "<td>Rp. ".round($s[harga_satuan],2)."</td>";
									echo "<td>Rp. ".round($cogs,2)."</td>";
									if($cogs==0) { echo "</span>";}
								
							}
						
						echo "</table>";
						echo "</td>";
						echo "<td>Rp. ".round($total_cogs,2)."</td>";
						echo "<td>Rp.".round(cari("unit_price", "table_product_catalog","kode_resep='".$r[kode_resep]."'"),2)."</td>";
						echo "<td>".$r[modified]."</td>";
						echo "</tr>";
					}
				  }			  
				  ?>

				  	</tbody>
                  </table>
                </div>
              </div><!-- /.box -->
	
</div>
<input name="b_print" type="button" class="ipt"   onClick="printdiv('div_print');" value=" CETAK ">
              
</section>