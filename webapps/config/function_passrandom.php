<?php
function generatePassword ($length = 8)
{
// mulai dengan password kosong
$password = "";

// definisikan karakter-karakter yang diperbolehkan
$possible = "0123456789bcdfghjkmnpqrstvwxyz";

// set up sebuah counter
$i = 0;

// tambahkan karakter acak ke $password sampai $length tercapai
while ($i < $length) {
// ambil sebuah karakter acak dari beberapa
// kemungkinan yang sudah ditentukan tadi
$char = substr($possible, mt_rand(0, strlen($possible)-1), 1);

// kami tidak ingin karakter ini jika sudah ada pada password
if (!strstr($password, $char)) {
$password .= $char;
$i++;
}
}
return $password;
}
?>