<?php
session_start();
#require('config.php');
require('config/config.php');
if(isset($_SESSION['namauser'])){
?>
<!DOCTYPE html>
<html>
  <head>
  <?php require("header.php");?>
  </head>
  <body class="hold-transition skin-red sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
	<!--------Menu Bar---------->
	<? require("navigation.php");?>
	<!-------//Menu Bar--------->
	<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
		<?
		if($_SESSION[level] == 1){
			if($option == "Dashboard"){require('home.php');}
			elseif($option == "product-update"){require('product-update.php');}
			elseif($option == "sign-out"){require('logout.php');}
			elseif($option == "change-pwd"){require('pwd.php');}			
			elseif($option == "daftar-dpr"){require('list-dpr2.php');}
			elseif($option == "daftar-sum"){require('dpr-rep.php');}
			elseif($option == "try-dpr"){require('list-dpr2.php');}
			elseif($option == "daftar-today"){require('dpr-reptoday.php');}
			elseif($option == "daftar-month"){require('dpr-repmonth.php');}	
			elseif($option == "daftar-today-det"){require('dpr-reptodaydet.php');}
			elseif($option == "daftar-month-det"){require('dpr-repmonthdet.php');}
			elseif($option == "register-member"){require('reg-member.php');}
			elseif($option == "register-result"){require('reg-result.php');}
			elseif($option == "register-success"){require('reg-success.php');}
			elseif($option == "test-email"){require('test-email.php');}
			elseif($option == "mgm-club"){require('mgm-club.php');}
			
			else{require('404.php');}
		}
		elseif($_SESSION[level] == 2){
			if($option == "Dashboard"){require('home.php');}
			elseif($option == "sign-out"){require('logout.php');}
			elseif($option == "change-pwd"){require('pwd.php');}
			elseif($option == "daftar-dpr"){require('list-dpr2.php');}	
			elseif($option == "daftar-sum"){require('dpr-rep.php');}
			elseif($option == "daftar-today"){require('dpr-reptoday.php');}		
			elseif($option == "daftar-month"){require('dpr-repmonth.php');}
			elseif($option == "daftar-today-det"){require('dpr-reptodaydet.php');}
			elseif($option == "daftar-month-det"){require('dpr-repmonthdet.php');}
			elseif($option == "mgm-club"){require('mgm-club.php');}
			else{require('404.php');}
		}
		elseif($_SESSION[level] == 3){
			if($option == "Dashboard"){require('home.php');}
			elseif($option == "sign-out"){require('logout.php');}
			elseif($option == "register-member"){require('reg-member.php');}
			elseif($option == "register-result"){require('reg-result.php');}
			elseif($option == "register-success"){require('reg-success.php');}
			else{require('404.php');}
		}else{
			require('404.php');
		}
		
		?>
        <!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<footer class="main-footer">
        <strong>Copyright &copy; 2015 Best Fitness | PT Always Fit. All rights reserved. Powered by IT Team Palughada.</strong>
    </footer>

      <!-- Control Sidebar -->
	  <!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->
</body>
</html>
<?}
else{
	header('location:index.php');
	exit;
}

?>