<?php

	require("config/config.php");

	$id = $_GET['id'];

	echo 'idnya : '. $id;

	$del = "UPDATE table_product_catalog SET status = 'NON' WHERE _id = '$barcode'";

	$res = query($del);

	if(isset($res)){

		echo "Yes";

	} else {

		echo "No";

	}

	

//	header("Location:\"".SITE_URL."/main.php?option=Dashboard\"");

?>

	<section class="content-header">

          <h1>

            DASHBOARD

            <small><?echo $_SESSION[namalengkap];?></small>

          </h1>

          <ol class="breadcrumb">

            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

          </ol>

        </section>



        <!-- Main content -->

        <section class="content">



          <!-- Default box -->

          <div class="box">

            <div class="box-header with-border">

              <h3 class="box-title">Sales Bulan Ini</h3>

              <div class="box-tools pull-right">

                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>

                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>

              </div>

            </div>

            <div class="box-body">

              <canvas id="doughnut-chart" width="800" height="450"></canvas>

              <script>

		new Chart(document.getElementById("doughnut-chart"), {

    	type: 'doughnut',

    	data: {

      		labels: ["Outlet 1", "Outlet 2", "Outlet 3", "Outlet 4", "Outlet 5"],

      		datasets: [

        	{

          		label: "Penjualan (Rp.)",

          		backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],

          		data: [20000000,75000000,150000000,100000000,65000000]

        	}

      		]

    	},

    	options: {

      		title: {

        		display: true,

        		text: 'Total Sales Bulan Ini.'

      		}

    	}

	});

	</script>

            </div><!-- /.box-body -->

            <div class="box-footer">             

            </div><!-- /.box-footer-->

          </div><!-- /.box -->

        </section>